﻿using UnityEngine;

namespace STDS
{
    public class CampignSetNextActive : MonoBehaviour
    {
       // [SerializeField]
       // Campign campign = null;

        [SerializeField]
        int nextMission =0;

        [SerializeField]
        private bool abilitesSave = false;


        public int GetNextMission()
        {
            return nextMission;
        }

        public void SuccesCall()
        {
            if (StoreSTDSGameData.instance != null)
            {
                if (nextMission > StoreSTDSGameData.instance.stdsData.CampignLevel)
                {
                    StoreSTDSGameData.instance.stdsData.CampignLevel = nextMission;

                    if (abilitesSave)
                        StoreSTDSGameData.instance.stdsData.endlessOpen = nextMission;
                   // StoreSTDSGameData.instance.Save();
                }
                


                //SaveJson();
            }
            // campign.ChangeCurrentActiveCampign(nextMission);
        }
    }
}
