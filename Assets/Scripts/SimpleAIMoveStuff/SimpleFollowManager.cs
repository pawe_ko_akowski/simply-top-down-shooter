﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class SimpleFollowManager : MonoBehaviour
    {
        [SerializeField]
        int archivePositionStore = 10;

        public static SimpleFollowManager instance = null;

        Vector2 destination;
        List<SimpleFollow> simpleFollowList = new List<SimpleFollow>();
        Vector2[] archivePlayerPosition;
        int lastArchiveIndex = 0;
        int helpInt;


        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            archivePlayerPosition = new Vector2[archivePositionStore];
        }


        public void UpdatePlayerPosition(Vector2 pos)
        {
            destination = pos;
            archivePlayerPosition[lastArchiveIndex] = pos;
            lastArchiveIndex++;
            if (lastArchiveIndex >= archivePositionStore)
                lastArchiveIndex = 0;

            GetInfoAllFollowers();
        }

        public Vector2 GetPlayerPosition(int minus = 1)
        {
            //Debug.Log("Last " + lastArchiveIndex + "   " + "before  " + GetBefore(3));

            return archivePlayerPosition[GetBefore(minus)];
        }

        public Vector2 GetMoveVectorFromTwoLast()
        {
           // Debug.Log(lastArchiveIndex + "    " + GetBefore());

            return (archivePlayerPosition[lastArchiveIndex] - archivePlayerPosition[GetBefore()]).normalized;
        }

        //public Vector2 GetPlayerPosFromLastTwo()
      //  {
            // Debug.Log(lastArchiveIndex + "    " + GetBefore());

           // return destination + (archivePlayerPosition[lastArchiveIndex] - archivePlayerPosition[GetBefore()]).normalized;
       // }


        int GetBefore(int minus = 1)
        {
            helpInt = lastArchiveIndex - minus;
            if (helpInt < 0)
                return archivePositionStore + (helpInt);
            return helpInt;         
        }


        public void RegisterSimpleFollow(SimpleFollow simpFollow)
        {
            simpleFollowList.Add(simpFollow);
            simpFollow.SetupDestination(destination);
        }


        public void UnregisterSimpleFollow(SimpleFollow simpFollow)
        {
            simpleFollowList.Remove(simpFollow);
        }


        void GetInfoAllFollowers()
        {
            for (int i = 0; i < simpleFollowList.Count; i++)
            {
                simpleFollowList[i].SetupDestination(destination);
            }
        }
    }
}
