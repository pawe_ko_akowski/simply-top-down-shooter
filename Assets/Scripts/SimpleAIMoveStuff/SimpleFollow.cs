﻿using UnityEngine;
using STDS.Steer2D;

namespace STDS
{
    public enum MOVE_STRATEGY
    {
        SIMPLY_MOVE,
        RANDOM_SIMPLY_MOVE,
        PREDICTED_MOVE,
        RANDOM_PREDICTED_MOVE
    }


    interface FollowPlaceStrategy
    {
        Vector2 FollowTo(Vector2 playerPos);
    }


    class SimpleFollowPlaceStrategy : FollowPlaceStrategy
    {
        public Vector2 FollowTo(Vector2 playerPos)
        {
            return playerPos;
        }
    }


    class CrazyFollowPlaceStrategy : FollowPlaceStrategy
    {
        public Vector2 FollowTo(Vector2 playerPos)
        {
                return playerPos + new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * Random.Range(1, 10f);
        }
    }


    class PredictedMoveStrategy : FollowPlaceStrategy
    {
        private SimpleFollowManager sfm;
        private Rigidbody2D rig2d;
        private float predictValue = 15f;
        private float squarePredictValu;


        public PredictedMoveStrategy(Rigidbody2D rig)
        {
            sfm = SimpleFollowManager.instance;
            rig2d = rig;
            squarePredictValu = predictValue * predictValue;
        }


        protected bool SmallDistance()
        {
            if (Vector2.SqrMagnitude(rig2d.position - sfm.GetPlayerPosition()) < squarePredictValu)
                return true;
            return false;
        }   


        public Vector2 FollowTo(Vector2 playerPos)
        {
            if (SmallDistance())
                return playerPos;
            return playerPos - sfm.GetMoveVectorFromTwoLast() * predictValue;
        }
    }


    class RandomPredictedMoveStrategy : PredictedMoveStrategy
    {
        public RandomPredictedMoveStrategy(Rigidbody2D rig):base(rig)
        {           
        }

        public new Vector2 FollowTo(Vector2 playerPos)
        {
            if (SmallDistance())
                return playerPos;
            return base.FollowTo(playerPos) + new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * Random.Range(1, 10f);
        }
    }


    [RequireComponent(typeof(Rigidbody2D))]
    public class SimpleFollow : MonoBehaviour
    {
        public float speed = 5, stopDist = 0.1f, timeForChange = 0.5f, stopFollowDistance = 0;
        public MOVE_STRATEGY moveStrategy = MOVE_STRATEGY.SIMPLY_MOVE;
        public Rigidbody2D rig2D;
        public SteeringAgent steerAgent;

       // private Transform rigTransform;
        private Vector2 destination;
        private Vector2 currentDestination;
        private Vector2 playerPosition;
        private bool currentDestinationSetup = false;
        private float activeSpeed;
        private bool go = true;
        private bool init = false;
        private float timer;
        private bool iSee = false;
        private float helpFloat;
        private Vector2 followPlace;       
        private FollowPlaceStrategy followToPlace;
        private SimpleFollowManager sf;


        private void Awake()
        {
            init = true;
           // rigTransform = rig2D.transform;
            stopFollowDistance *= stopFollowDistance;
            stopDist *= stopDist;
            speed = speed + Random.Range(-speed * 0.1f, speed * 0.1f);         
            SetMoveStrategy();
            sf = SimpleFollowManager.instance;
        }


        private void OnEnable()
        {
            if (init)
                sf.RegisterSimpleFollow(this);             
        }


        private void OnDisable()
        {
            activeSpeed = speed;
            timer = 0;
            sf.UnregisterSimpleFollow(this);
        }


        private void FixedUpdate()
        {
           if (go)
           {
                followPlace = steerAgent.GetMoveVector();
                rig2D.AddForce(followPlace * speed);
                rig2D.rotation = Mathf.Atan2(followPlace.y, followPlace.x) * Mathf.Rad2Deg;
                //helpFloat = Mathf.Atan2(followPlace.y, followPlace.x) * Mathf.Rad2Deg;
                //rig2D.rotation = helpFloat;
               // rigTransform.rotation = Quaternion.Lerp(rigTransform.rotation, Quaternion.Euler(0, 0, helpFloat), Time.deltaTime*10f);
            }
        }


        private void SetMoveStrategy()
        {
            switch (moveStrategy)
            {
                case MOVE_STRATEGY.SIMPLY_MOVE:
                    {
                        followToPlace = new SimpleFollowPlaceStrategy();
                        break;
                    }
                case MOVE_STRATEGY.RANDOM_SIMPLY_MOVE:
                    {
                        followToPlace = new CrazyFollowPlaceStrategy();
                        break;
                    }
                case MOVE_STRATEGY.PREDICTED_MOVE:
                    {
                        followToPlace = new PredictedMoveStrategy(rig2D);
                        break;
                    }
                case MOVE_STRATEGY.RANDOM_PREDICTED_MOVE:
                    {
                        followToPlace = new RandomPredictedMoveStrategy(rig2D);
                        break;
                    }
                default:
                    {
                        Debug.LogError("This shouldynt happen - no that strategy or null!!!");
                        break;
                    }
            }
        }


        private int side(float x1, float y1, float x2, float y2, float x3, float y3)
        {
            float dot = (y2 - y1) * (x2 - x3) + (x1 - x2) * (y2 - y3);
            if (dot == 0) return 0;
            if (dot < 0) return -1;
            return 1;
        }


        private int side(Vector2 one, Vector2 two, Vector2 three)
        {
            float dot = (two.y - one.y) * (two.x - three.x) + (one.x - two.x) * (two.y - three.y);
            if (dot == 0) return 0;
            if (dot < 0) return -1;
            return 1;
        }


        public float GetSpeed()
        {
            return speed;
        }


        public float GetActiveSpeed()
        {
            return activeSpeed;
        }


        public void ProgressSpeed(float speedAdd)
        {
            activeSpeed += speedAdd;
        }


        public void SetupDestination(Vector2 pos)
        {
             playerPosition = pos;

             destination = pos;
        }


        public void StopFollow()
        {
            go = false;
        }


        public void RestartFollow()
        {
            go = true;
        }


        public Vector2 GetDirection()
        {
            return (currentDestination - rig2D.position).normalized;
        }


        public Vector2 GetDestination()
        {
            return destination;
        }


        public bool ISeePlayerOnFront()
        {
            return !currentDestinationSetup;
        }


        public void DirectionChange(Vector2 correctDirection)
        {
            currentDestination = correctDirection;
            timer = 0;
        }


        public void DirectionChange()
        {
            if (timer > timeForChange)
            {
                timer = 0;
                currentDestination = followToPlace.FollowTo(playerPosition);
            }  
        }


        public Vector2 GetPlayerDirectionNormalized()
        {
            return (followToPlace.FollowTo(playerPosition) - rig2D.position).normalized;
        }


        public Vector2 GetPlayerDirection()
        {
            return (playerPosition - rig2D.position);
        }


        public bool ClearViewCheck()
        {
            if (Physics2D.Raycast(rig2D.position, GetPlayerDirectionNormalized(), Vector2.Distance(rig2D.position, GetDestination()), 1 << 0))
                iSee = false;
            else
                iSee = true;
            return iSee;
        }
    }
}
