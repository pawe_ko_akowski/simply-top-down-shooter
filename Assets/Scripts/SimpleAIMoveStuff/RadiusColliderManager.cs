﻿using UnityEngine;

namespace STDS
{
    public class RadiusColliderManager : MonoBehaviour
    {
        class ColliderRadius
        {
            public int id;
            public float radius;

            public ColliderRadius(int identyfier, float rad)
            {
                id = identyfier;
                radius = rad;
            }
        }

        static public RadiusColliderManager instance = null;

        [SerializeField]
        string circleCollidersTag = "circleCollider";

        ColliderRadius[] colliders = null;
        int helpInt;


        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        void Start()
        {
            GameObject[] gobs = GameObject.FindGameObjectsWithTag(circleCollidersTag);

            colliders = new ColliderRadius[gobs.Length];

            for (int i = 0; i < gobs.Length; i++)
            {
                CircleCollider2D circle = gobs[i].GetComponent<CircleCollider2D>();
                colliders[i] = new ColliderRadius(circle.GetInstanceID(), circle.radius * circle.gameObject.transform.localScale.x);
                //Debug.Log(circle.GetInstanceID() + "   " + circle.radius* circle.gameObject.transform.localScale.x);
            }
        }


        int IHaveIt(int id)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                if (id == colliders[i].id)
                    return i;
            }
            return -1;
        }


        float ReturnRadius(int id)
        {
            helpInt = IHaveIt(id);

            if (helpInt < 0)
            {
                Debug.LogError("Nie ma czegoś takiego w bazie");
                return 0;
            }
            return colliders[helpInt].radius;
        }


        public float GetCircleColliderRadius(int id)
        {
            return ReturnRadius(id);
        }
    }
}
