﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

namespace STDS
{
    [RequireComponent(typeof(CoinsAndXPCalculator))]
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance = null;

        [SerializeField]
        int sceneToLoadAfterDead = 0;
        //[SerializeField]
       // int cashAndXPMultipierIfSucces = 2;
        [SerializeField]
        float endingMenuTime = 1.6f;
        [SerializeField]
        AudioMixer masterMixer = null;
        bool turnOn = true;

        public bool runCampignLevelTimer = true;


        GameObject failureText = null, succesText = null;
        GameObject rewardMenuGob = null, breakGameMenuGob = null;
        GameObject player = null;
        bool gameLive = true;
        CoinsAndXPCalculator coinsAndXPCalculator;
        CampignSetNextActive campignNext;
        AudioSource audiosource;
        AudioClip winAudioClip, loseAudioclip, btnAudioClip;
        //WaitForSeconds wait;
        ScorePointsCalculator sPC;
        LevelMonsterSpawner lMS;
        CampignLevelTimer cLT = null;
        SomethingBeforeEnd sBE = null;
        private float timeScale = 0;
        private const float timeSclaeAd = 0.3f;

        // ScorePointsCalculator sPC;

        private void OnDestroy()
        {
            if(cLT!=null)
            cLT.StopTimer();
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            timeScale = 1f;

            if (StoreSTDSGameData.instance != null)
                timeScale += (StoreSTDSGameData.instance.stdsData.timeScale * timeSclaeAd);
            SetTimeToCurrentTime();

            coinsAndXPCalculator = GetComponent<CoinsAndXPCalculator>();
            campignNext = GetComponent<CampignSetNextActive>();

            InGameFiller inGameFiller = GameObject.FindObjectOfType<InGameFiller>();

            failureText = inGameFiller.failureTextGob;
            succesText = inGameFiller.succesTextGob;
            rewardMenuGob = inGameFiller.rewardMenuGob;
            breakGameMenuGob = inGameFiller.breakMenuGob;

            failureText.SetActive(false);
            succesText.SetActive(false);
            rewardMenuGob.SetActive(false);
            breakGameMenuGob.SetActive(false);

            winAudioClip = inGameFiller.winAudioClip;
            loseAudioclip = inGameFiller.loseAudioclip;
            btnAudioClip = inGameFiller.btnAudioClip;

            inGameFiller.resumeBtn.onClick.AddListener(ResumeMatch);
            inGameFiller.endMatchBtn.onClick.AddListener(ToRewardMenu);
            inGameFiller.restartMatchBtn.onClick.AddListener(RestartMatch);
            inGameFiller.toMainMenuBtn.onClick.AddListener(GoToMainMenu);

            audiosource = GetComponent<AudioSource>();
            player = GetComponent<PlayerTransformManager>().PlayerTransform.gameObject;

            //wait = new WaitForSeconds(endingMenuTime);
            sPC = FindObjectOfType<ScorePointsCalculator>();

            lMS = GetComponent<LevelMonsterSpawner>();

            if(runCampignLevelTimer)
            cLT = gameObject.AddComponent<CampignLevelTimer>();
            sBE = FindObjectOfType<SomethingBeforeEnd>();
        }


        private void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Escape))
            if(turnOn)
            if (gameLive)
            {
                if (Input.GetButtonDown("Cancel"))
                    ToogleBreak();
            }
            else
            {
                endingMenuTime -= Time.deltaTime;
                if (endingMenuTime < 0)
                {
                    RealyEndGAme();
                    turnOn = false;
                }
            }

            //dupa
            //if (Input.GetButtonDown("Fire1"))
            //{
            //    if (gameLive)
            //    {
            //        SuccesEndGame();
            //    }
            //}
        }


        void SetTimeToZero()
        {
            Time.timeScale = 0;
        }

        void SetTimeToCurrentTime()
        {
            Time.timeScale = timeScale;
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus == false)
            {
                if (gameLive)
                {
                    breakGameMenuGob.SetActive(true);
                    player.SetActive(false);
                    SetTimeToZero();
                }
                   masterMixer.SetFloat("masterVolume", -80);
                    //audioMixer.SetFloat("musicVolume", musicSlider.value);
            }
                else
                {
                    masterMixer.SetFloat("masterVolume", 0);
                }
        }

        //private IEnumerator EndGame()
        //{
        //    Time.timeScale = 1;
        //    yield return wait;
        //    CalculateAllCoinAndXpStuf();
        //    rewardMenuGob.SetActive(true);
        //    player.SetActive(false);
        //    CursorShow();
        //    EndIfScoreAreCalc();
        //  //  TimerCalc();
        //}

        private void RealyEndGAme()
        {
            //Time.timeScale = 1;
                SetTimeToCurrentTime();
                CalculateAllCoinAndXpStuf();
                rewardMenuGob.SetActive(true);
                player.SetActive(false);
                CursorShow();
                EndIfScoreAreCalc();           
        }


        private void CursorShow()
        {
            Cursor.visible = true;
        }

        private void ToogleBreak()
        {
            if (Time.timeScale > 0)
            {
                breakGameMenuGob.SetActive(true);
                player.SetActive(false);
                SetTimeToZero();
                CursorShow();


            }
            else
            {
                TurnOnGame();
            }
        }


        private void TurnOnGame()
        {
            breakGameMenuGob.SetActive(false);
            player.SetActive(true);
            SetTimeToCurrentTime();
        }


        public void RestartMatch()
        {
            //audiosource.PlayOneShot(btnAudioClip);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }


        public void GoToMainMenu()
        {
           // audiosource.PlayOneShot(btnAudioClip);
            SceneManager.LoadScene(sceneToLoadAfterDead);
        }


        public void ResumeMatch()
        {
            audiosource.PlayOneShot(btnAudioClip);
            TurnOnGame();
        }


        public void ToRewardMenu()
        {
            if (gameLive)
            {
                turnOn = false;
                audiosource.PlayOneShot(btnAudioClip);
                gameLive = false;
                //Time.timeScale = 1;
                SetTimeToCurrentTime();
                breakGameMenuGob.SetActive(false);
                rewardMenuGob.SetActive(true);
                player.SetActive(false);
                CalculateAllCoinAndXpStuf();
                CursorShow();
                EndIfScoreAreCalc();
            }
           // TimerCalc();
            //coinsAndXPCalculator.CalculateCoins(1);
            //coinsAndXPCalculator.CalculateXP(1);

        }


        private void EndIfScoreAreCalc()
        {
            if (sPC != null)
            {
                sPC.GiveIncreaseInformation(false);
                sPC.SaveScore();

            }
        }

        //private void TimerCalc()
        //{
        //    if (cLT != null)
        //    {
        //        cLT.StopTimer();
        //    }
            
        //}

        public void EndGameFinaly()
        {
            if (gameLive)
            {
                gameLive = false;
                
                if(!lMS.EndlessMode)
                failureText.SetActive(true);
                audiosource.PlayOneShot(loseAudioclip);
                
                //coinsAndXPCalculator.CalculateCoins(1);
                //coinsAndXPCalculator.CalculateXP(1);
                //dupa do usuniecia linijka poniżej

               // StartCoroutine(EndGame());
            }
        }


        public void SuccesEndGame()
        {
            if (gameLive)
            {

                if (sBE != null)
                    if (!sBE.MakeJob())
                        return;
                //coinsAndXPCalculator.CalculateCoins(cashAndXPMultipierIfSucces);
                //coinsAndXPCalculator.CalculateXP(cashAndXPMultipierIfSucces);
                // coinsAndXPCalculator.CalculateCoins(1);
                // coinsAndXPCalculator.CalculateXP(1);
               // CalculateAllCoinAndXpStuf();
                 gameLive = false;
                if (!lMS.EndlessMode)
                    succesText.SetActive(true);
                audiosource.PlayOneShot(winAudioClip);



                if (campignNext!=null)
                campignNext.SuccesCall();
               // StartCoroutine(EndGame());
            }
        }

        void CalculateAllCoinAndXpStuf()
        {
            coinsAndXPCalculator.CalculateCoins();
            
            coinsAndXPCalculator.CalculateXP();
            coinsAndXPCalculator.DecreaseAdRewardMultiplier();
        }
    }
}
