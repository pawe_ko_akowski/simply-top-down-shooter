﻿using UnityEngine;

namespace STDS
{  
    public class BoomAudioPlay : MonoBehaviour
    {
        private AudioClip audioClip = null;
        private AudioSource audioSour;

        private void Awake()
        {
            audioSour = GetComponent<AudioSource>();
            audioClip = audioSour.clip;
        }


        public void PlayAudiosource(float volume)
        {
            audioSour.volume = volume;
            audioSour.PlayOneShot(audioClip);
        }
    }
}
