﻿using UnityEngine;

namespace STDS
{
    public class MonsterSmoothSet : MonoBehaviour
    {
        public MonoBehaviour[] toStayOrDestroyMonobehviour;
        public Transform parentToAsignTr;
        private void Awake()
        {
            if (parentToAsignTr == null)
                parentToAsignTr = transform.parent;

            if (StoreSTDSGameData.instance != null)
            {
                if (StoreSTDSGameData.instance.stdsData.smooth <= 0)
                    SetSmooth(false);
                else
                    SetSmooth(true);
            }
            else
                SetSmooth(false);
        }

        public virtual void SetSmooth(bool what)
        {
            if (what)
            {
                for (int i = 0; i < toStayOrDestroyMonobehviour.Length; i++)
                {
                    toStayOrDestroyMonobehviour[i].enabled = true;
                }
            }
            else
            {
                for (int i = 0; i < toStayOrDestroyMonobehviour.Length; i++)
                {
                    parentToAsignTr.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
                    Destroy(toStayOrDestroyMonobehviour[i]);
                }
            }
            Destroy(this);
        }
    }
}
