﻿using UnityEngine;
namespace STDS
{
    public class CampignLevelTimer : MonoBehaviour
    {
        float startTime, timer;
        int startLevel = 0;
        //CampignSetNextActive cSNA;
        STDSdata data;

        void Start()
        {
            //cSNA = GetComponent<CampignSetNextActive>();
            if (StoreSTDSGameData.instance != null)
            {
                data = StoreSTDSGameData.instance.stdsData;
                startLevel = data.CampignLevel;
            }
            startTime = Time.time;
            timer = 0;
        }

        
        public void StopTimer()
        {
            timer = Time.time - startTime;
            timer *= 100;
            timer = Mathf.RoundToInt(timer);
            timer /= 100;

           // if (cSNA.GetNextMission() > data.CampignLevel)
           // {
                if(data != null)
                if (data.CampignLevel < 10 ||(data.CampignLevel - startLevel)==1)
                   data.actualCampignTime += timer;
           // }
   
        }


    }
}
