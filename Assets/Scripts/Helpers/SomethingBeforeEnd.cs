﻿using UnityEngine;
using UnityEngine.UI;
namespace STDS
{
    public class SomethingBeforeEnd : MonoBehaviour
    {
        bool run = false;
        bool done = false;
        public GameObject filmGob, gameGob;
        public float finishTime = 10f;
      //  PlayableDirector pD = null;
        public Camera playerCamera;
        public AudioListener charAudiolistner;
        public float waitTime = 1f;

        [TextArea(0, 12)]
        public string englishDescription;
        [TextArea(0, 12)]
        public string polishDescription;
        public Text descriptionText;

        private void Start()
        {
            if(StoreSTDSGameData.instance != null)
            if (StoreSTDSGameData.instance.stdsData.language == 0)
            {
                descriptionText.text = polishDescription;
            }
            else
            {
                descriptionText.text = englishDescription;
            }
            //pD = GetComponent<PlayableDirector>();
            gameGob.SetActive(true);
            filmGob.SetActive(false);
            charAudiolistner.enabled = true;
            playerCamera.enabled = true;

            if (StoreSTDSGameData.instance != null)
                if (StoreSTDSGameData.instance.stdsData.introOutroWatch >= 2)
                {
                    Destroy(filmGob);
                    Destroy(gameObject);
                }
        }


        public bool MakeJob()
        {
            if (StoreSTDSGameData.instance != null)
                if (StoreSTDSGameData.instance.stdsData.introOutroWatch >= 2)
                    return true;

            run = true;
            return done;

        }


        private void End()
        {
            GameManager.instance.SuccesEndGame();
        }


        void Update()
        {
            if (run)
            {
                waitTime -= Time.deltaTime;
                if (waitTime < 0)
                    if (!done)
                    {
                        gameGob.SetActive(false);
                        charAudiolistner.enabled = false;
                        playerCamera.enabled = false;
                        filmGob.SetActive(true);
                        done = true;
                    }
                    else
                    {
                        finishTime -= Time.deltaTime;
                        if (finishTime < 0)
                        {
                            gameGob.SetActive(true);
                            if (StoreSTDSGameData.instance != null)
                            {
                                StoreSTDSGameData.instance.stdsData.introOutroWatch = 2;
                                StoreSTDSGameData.instance.Save();
                            }
                            filmGob.SetActive(false);
                            charAudiolistner.enabled = true;
                            playerCamera.enabled = true;
                            End();
                        }
                    }
            }
        }
    }
}
