﻿using UnityEngine;

namespace STDS
{
    public class PlayerDamageManager : MonoBehaviour
    {
        public static PlayerDamageManager instance = null;
        private int playerDamage;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        public int GetPlayerDamage
        {
            get
            {
                return playerDamage;
            }
        }


        public void SetPlayerDamage(int to)
        {
            playerDamage = to;
        }
    }
}
