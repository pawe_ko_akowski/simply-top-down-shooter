﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class SomePlayerObiectsGetter : MonoBehaviour
    {
        [SerializeField]
        Text coinText = null;
        [SerializeField]
        Transform toFollow = null;


        public Text GetText
        {
            get
            {
                return coinText;
            }
        }

        public Transform FollowTransform
        {
            get
            {
                return toFollow;
            }
        }
    }
}
