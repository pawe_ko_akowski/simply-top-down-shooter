﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class OpenURL : MonoBehaviour {

        public string loadPage = "http://wolfingame.com/";
        public Button loadBtn;

        private void Start()
        {
            if (loadBtn != null)
                loadBtn.onClick.AddListener(LoadURL);
        }

        private void LoadURL()
        {
            Application.OpenURL(loadPage);
        }

        public void LoadSite()
        {
            LoadURL();
        }

    }
}
