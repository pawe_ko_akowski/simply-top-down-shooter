﻿using UnityEngine;

namespace STDS
{
    public class EnemyParticleDamager : MonoBehaviour
    {
        [SerializeField]
        private int bulletDamage = 10;

        private Helth helth;
        private GameObject playerGob;


        private void Start()
        {
            helth = GameObject.Find("Player").GetComponentInChildren<PlayerHealth>(true);
            playerGob = helth.gameObject;
        }

        protected void OnParticleCollision(GameObject other)
        {
            if(other == playerGob)
            helth.Damage(bulletDamage);
        }

        public void ChangeBuletDamage(int howMany)
        {
            bulletDamage += howMany;
        }
    }
}
