﻿using UnityEngine;

namespace STDS
{
    public class CutOffIfIfMaxLevel : MonoBehaviour
    {
        public GameObject xpRewardPanel;

        void Start()
        {
            if (StoreSTDSGameData.instance != null)
            {
                if (StoreSTDSGameData.instance.stdsData.level >= 100)
                    // Destroy(xpRewardPanel, .1f);
                    xpRewardPanel.SetActive(false);
            }

            Destroy(this);
        }

    }
}
