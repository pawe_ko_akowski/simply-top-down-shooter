﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class MysteryFinder : MonoBehaviour
    {
        [SerializeField]
        private bool hideAfterStart = false;

        [SerializeField]
        private int elementToFind = 0;

        [SerializeField]
        private GameObject textGob = null;

        STDSdata gameData;
        AudioSource audiosource;
        Text text;
        Collider2D col;
        


        private void OnTriggerEnter2D(Collider2D collision)
        {
          //  if (collision.CompareTag("Player"))
            {
                if (gameData != null)
                {
                    gameData.mysteryFind[elementToFind] = 1;
                    MysteryFullCheck();
                }
                col.enabled = false;
                audiosource.Play();
                textGob.SetActive(false);
                //Destroy(this.gameObject, 3f);
            }
        }

        private void MysteryFullCheck()
        {
            for (int i = 0; i < gameData.mysteryFind.Length; i++)
            {
                if (gameData.mysteryFind[i] == 0)
                    return;
            }
            gameData.mysteryFull = 1;
        }


        void Awake()
        {
           // Debug.Log("Awake");
            col = GetComponent<Collider2D>();
            text = GetComponentInChildren<Text>();

            if (StoreSTDSGameData.instance != null)
            {
                gameData = StoreSTDSGameData.instance.stdsData;

              //  Debug.Log(gameData.mysteryFull + "   " + gameData.mysteryFind[elementToFind]);
                if (gameData.mysteryFull >= 1 || gameData.mysteryFind[elementToFind] >= 1)
                {
                   // Debug.Log("Dupa");
                    text.text = "";
                    col.enabled = false;
                    //Destroy(this.gameObject);
                    return;
                }
            }

            if (hideAfterStart)
                gameObject.SetActive(false);

            audiosource = GetComponent<AudioSource>();
        }

    }
}
