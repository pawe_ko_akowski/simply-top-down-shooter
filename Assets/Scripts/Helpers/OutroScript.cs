﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class OutroScript : MonoBehaviour
    {
        [TextArea(0, 12)]
        public string englishDescription;
        [TextArea(0, 12)]
        public string polishDescription;
        public Text descriptionText;

        void Start()
        {
            if (StoreSTDSGameData.instance.stdsData.language == 0)
            {
                descriptionText.text = polishDescription;
            }
            else
            {
                descriptionText.text = englishDescription;
            }

            Destroy(this);
        }
    }
}
