﻿using UnityEngine;

namespace STDS
{
    public class SplashParticleEmiter : MonoBehaviour
    {
        [SerializeField]
        private int partSysNumber = 0;

        private ParticleSystem.EmitParams emiPar = new ParticleSystem.EmitParams();
        private ParticleSystem parSys;
        private Rigidbody2D rig2D;
 

        private void Start()
        {
            parSys = ParticleSpllatterManager.instance.GetParticleSystemSplater(partSysNumber);
            rig2D = GetComponent<Rigidbody2D>();
        }


        public void EmitParticle()
        {
            emiPar.position = transform.position;
            emiPar.rotation = rig2D.rotation;
            // emiPar.velocity = pos * bulletSpeed;
            parSys.Emit(emiPar, 1);
        }
    }
}
