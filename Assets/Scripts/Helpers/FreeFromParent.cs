﻿using UnityEngine;

namespace STDS
{
    public class FreeFromParent : MonoBehaviour
    {
        private void Start()
        {
            transform.SetParent(null);
        }
    }
}
