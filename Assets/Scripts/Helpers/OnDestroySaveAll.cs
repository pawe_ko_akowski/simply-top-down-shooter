﻿using UnityEngine;

namespace STDS
{
    public class OnDestroySaveAll : MonoBehaviour
    {
        private void OnDestroy()
        {
            if(StoreSTDSGameData.instance != null)
            StoreSTDSGameData.instance.Save();
        }


    }
}
