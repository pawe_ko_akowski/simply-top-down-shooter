﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class OnPlayerBullet : MonoBehaviour
    {
        ScorePointsCalculator sPC;

        private void Start()
        {
            sPC = FindObjectOfType<ScorePointsCalculator>();
            if (sPC == null)
                Destroy(this);
        }

        protected void OnParticleCollision(GameObject other)
        {
            if (other.layer == 0)
                sPC.GiveIncreaseInformation(false);
            else
                sPC.GiveIncreaseInformation(true);
        }
    }
}
