﻿using UnityEngine;

namespace STDS
{
    public class PlayerSmoothSet : MonsterSmoothSet
    {
        public override void SetSmooth(bool what)
        {
            if (!what)
                this.transform.SetParent(parentToAsignTr);
            base.SetSmooth(what);
        }
    }
}
