﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class DemoScript : MonoBehaviour
    {
        public string demoText = " demo";
        public GameObject hidePanelGob;
        public bool demo = false;
        public Text gameNameText;

        // Use this for initialization
        void Start()
        {
            if (!demo)
                Destroy(hidePanelGob);
            else
            {
                gameNameText.text += demoText;
            }
            Destroy(this);
        }
    }
}

