﻿
using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class CanvasManager : MonoBehaviour
    {
        public static CanvasManager instance = null;

        [SerializeField]
        AudioClip coinClipBeep = null;
 
        Text coinsText = null;
        Animator coinTextAnimator = null;
        int coins = 0;
        int xpPoints = 0;
        int helpMultiplier = 0;
        int coinAdd = 0;
        AudioSource audioSource = null;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            // coinsText.text = coins.ToString();
            coinsText = PlayerTransformManager.instance.PlayerTransform.GetComponent<SomePlayerObiectsGetter>().GetText;
            coinsText.text = coins.ToString();
            coinTextAnimator = coinsText.transform.parent.GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }


        private void MultiplyCoins(int multiplier = 2)
        {
            helpMultiplier = coins * multiplier - coins;
            UpdateCoinsWithdd(helpMultiplier);
        }


        private void MultiplyXP(int multiplier = 2)
        {
            helpMultiplier = xpPoints * multiplier - xpPoints;
            UpdateXP(helpMultiplier);
        }


        public void SetupCoinAdd(int amount)
        {
            coinAdd = amount;
        }


        public int GetCoins
        {
            get
            {
                return coins;
            }
        }


        public int GetXP
        {
            get
            {
                return xpPoints;
            }
        }


        public void UpdateCoins(int amount)
        {
            coins += amount + coinAdd;
            coinsText.text = coins.ToString();
            coinTextAnimator.SetTrigger("Coin");
            audioSource.PlayOneShot(coinClipBeep);
        }


        public void UpdateCoinsWithdd(int amount)
        {
            coins += (amount);
            coinsText.text = coins.ToString();
        }


        public void UpdateXP(int xp)
        {
            xpPoints += xp;
        }
    }
}
