﻿using UnityEngine;

namespace STDS
{
    public class PickableItem : MonoBehaviour
    {
        //[SerializeField]
       // Collider2D playerCollider = null;


        //private void Start()
       // {
          //  playerCollider = PlayerTransformManager.instance.PlayerTransform.GetComponentInChildren<Collider2D>(true);
       // }


        private void OnTriggerEnter2D(Collider2D other)
        {
            // if (other == playerCollider)
            // {
            if (!other.isTrigger)
            {
                gameObject.SetActive(false);
                DoSomething();
            }
           // }
        }


        protected virtual void DoSomething()
        {

        }
    }
}
