﻿using UnityEngine;

namespace STDS
{
    public class TurnOnObiectAfter : MonoBehaviour
    {
        [SerializeField]
        float time;
        [SerializeField]
        GameObject toEnabled = null;


        private void Update()
        {
            time -= Time.deltaTime;
            if (time < 0)
            {
                toEnabled.SetActive(true);
                Destroy(this);
            }
        }
    }
}
