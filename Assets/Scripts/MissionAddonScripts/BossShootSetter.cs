﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace STDS
{
    public class BossShootSetter : MonoBehaviour {
        [Serializable]
        class ScriptSetter
        {
            public MonoBehaviour script = null;
            public int enabledWhen = 800;

            public void Info(int data)
            {
                if (data < enabledWhen)
                {
                    script.enabled = true;
                }
                else
                {
                    script.enabled = false;
                }
            }
        }

        [SerializeField]
        ScriptSetter[] scriptSetter = null;

        public void SetDataInfo(int data)
        {
           // Debug.Log(data);

            for (int i = 0; i < scriptSetter.Length; i++)
            {
                scriptSetter[i].Info(data);
            }
        }
    }
}
