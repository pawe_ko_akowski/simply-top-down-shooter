﻿using UnityEngine;

namespace STDS
{
    public class XandYScaler : MonoBehaviour
    {

        [SerializeField]
        float startScale = 0.2f;
        [SerializeField]
        float endScale = 20f;
        [SerializeField]
        float scaleTime = 100f;

        float timer = 0;
        float helpScale = 0;


        void Start()
        {
            transform.localScale = new Vector3(startScale, startScale, 1);
        }


        void Update()
        {
            timer += Time.deltaTime;
            helpScale = startScale + (timer / scaleTime) * (endScale - startScale);
            transform.localScale = new Vector3(helpScale, helpScale, 1);
            if (helpScale > endScale)
            {
                transform.localScale = new Vector3(endScale, endScale, 1);
                Destroy(this);
            }
        }
    }
}
