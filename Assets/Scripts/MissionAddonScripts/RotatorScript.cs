﻿using System.Collections;
using UnityEngine;

namespace STDS
{
    public class RotatorScript : MonoBehaviour
    {
        [SerializeField]
        float rotToSet;
        [SerializeField]
        float rotateSpeed = 5f;

        Quaternion rot;
        bool ready = false;


        void Start()
        {
            rot = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, rotToSet);
            Init();
        }


        protected virtual void Init()
        {

        }


        public virtual bool RotateObject()
        {
            ready = true;
            //StartCoroutine(Rotate());
            return true;
        }


        public virtual void RotateObjectTo(float rotZ)
        {
            rotToSet = rotZ;
            rot = Quaternion.Euler(transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, rotToSet);
            ready = true;
           // StartCoroutine(Rotate());
        }


        public virtual bool RotateObjectTo(TriggerLabirynth trigLab)
        {
            return true;
        }

        private void Update()
        {
            if (ready)
                if (transform.localRotation.eulerAngles.z != rotToSet)
                {
                    transform.localRotation = Quaternion.RotateTowards(transform.localRotation, rot, Time.deltaTime * rotateSpeed);
                }
                else
                {
                    ready = false;
                }
        }

        //IEnumerator Rotate()
        //{
        //    while (transform.localRotation.eulerAngles.z != rotToSet)
        //    {
        //        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, rot, Time.deltaTime * rotateSpeed);
        //        yield return null;
        //    }
        //    yield return null;
        //}
    }
}
