﻿using UnityEngine;

namespace STDS
{
    public class ParticleSystemTurnOff : MonoBehaviour
    {

         ParticleSystem pS;


        private void Start()
        {
            pS = GetComponent<ParticleSystem>();
            transform.SetParent(null);
        }

        private void OnDisable()
        {
            ParticleSystem.MainModule module;
            ParticleSystem.EmissionModule moduleEmis;
            module = pS.main;
            moduleEmis = pS.emission;
            module.loop = false;
            moduleEmis.enabled = false;
        }
    }
}
