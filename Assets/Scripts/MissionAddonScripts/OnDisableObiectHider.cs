﻿using UnityEngine;

namespace STDS
{
    public class OnDisableObiectHider : MonoBehaviour
    {

        [SerializeField]
        GameObject[] toHide = null;

        private void OnDisable()
        {
            for (int i = 0; i < toHide.Length; i++)
            {
                toHide[i].SetActive(false);
            }
        }
    }
}
