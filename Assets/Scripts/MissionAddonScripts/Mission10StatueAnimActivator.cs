﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class Mission10StatueAnimActivator : MonoBehaviour
    {
        int statues;
        public static Mission10StatueAnimActivator instance = null;

        [SerializeField]
        bool activeForTest = false;

        [SerializeField]
        Mission10RotorStatue[] statueRotor = null;

        [SerializeField]
        GameObject mysteryToFind = null;

        float timer;
        bool ready = false;


        private void Start()
        {
            instance = this;
            statues = statueRotor.Length;

            if (activeForTest)
                ActivateStatues();
        }

        public void ActivateStatues()
        {
            if (mysteryToFind != null)
                for (int i = 0; i < statueRotor.Length; i++)
                    statueRotor[i].ActiveStatue();
            else
                Destroy(this);
        }

        private void FixedUpdate()
        {
            if (ready)
            {
                if (Time.time > timer)
                {
                    mysteryToFind.SetActive(true);
                    Destroy(this);
                }
            }
        }

        public void UnlockMystery()
        {
            statues--;
            if (statues <= 0)
            {
                timer = Time.time + 1.5f;
                ready = true;
               // StartCoroutine(UnhideMystery());
            }
        }

        

        //IEnumerator UnhideMystery()
        //{
        //    yield return new WaitForSeconds(1.5f);
        //    mysteryToFind.SetActive(true);
        //    Destroy(this);
        //}
    }
}
