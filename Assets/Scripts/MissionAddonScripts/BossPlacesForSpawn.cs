﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace STDS
{
    public class BossPlacesForSpawn : MonoBehaviour
    {
       // BossMonsterSpawner bossMonsterSpawner = null;

        [SerializeField]
        Transform[] placesForSpawn = null;

        int actualIndex = 0;

      //  private void Start()
      //  {
           // bossMonsterSpawner = GetComponent<BossMonsterSpawner>();
           
       // }


        public Transform[] GetSpawnPlaces
        {
            get
            {
                return placesForSpawn;
            }
        }

        public Transform GetNextSpawnPlace
        {
            get
            {
                if (actualIndex < placesForSpawn.Length)
                {
                    return placesForSpawn[actualIndex++]; //actualIndex++;                    
                }
                else
                {
                    actualIndex = 0;
                    return placesForSpawn[actualIndex];
                }
                
                
            }
        }
        //IEnumerator SpawningTime()
        //{
        //    while (true)
        //    {
        //        yield return new WaitForSeconds(2f);
        //        SpawnAround();
        //    }
        //}


        //public void SpawnAround()
        //{
        //    for (int i = 0; i < placesForSpawn.Length; i++)
        //    {
        //        bossMonsterSpawner.Spawn(placesForSpawn[i].position);
        //    }
        //}

        //public void SpawnAround()
        //{
        //    for (int i = 0; i < placesForSpawn.Length; i++)
        //    {
        //        bossMonsterSpawner.Spawn(placesForSpawn[i].position);
        //    }
        //}

        //public void SpawnNext()
        //{
        //    if (actualIndex < placesForSpawn.Length)
        //    {
        //        bossMonsterSpawner.Spawn(placesForSpawn[actualIndex].position);
        //        actualIndex++;
        //    }
        //    else
        //    {
        //        actualIndex = 0;
        //    }
        //}
    }
}
