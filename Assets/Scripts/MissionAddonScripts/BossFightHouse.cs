﻿using UnityEngine;

namespace STDS
{
    public class BossFightHouse : MonoBehaviour
    {
        void Start()
        {
            BossFightHouseCounter.instance.Register();
        }

        private void OnDisable()
        {
            BossFightHouseCounter.instance.Unregister();
        }
    }
}
