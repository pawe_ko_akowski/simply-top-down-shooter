﻿using System.Collections;
using UnityEngine;

namespace STDS
{
    public class MonsterAreaDamage : MonoBehaviour
    {
        [SerializeField]
        private int damage = 10000;
        [SerializeField]
        private float waitTime = 3f;
        [SerializeField]
        AudioClip dethClipToPlay = null;


        //WaitForSeconds wait;
        bool inRange = false;
        PlayerLiveManager plMan;
        AudioSource audiosource;
        float timer = 0;


        void Start()
        {
            //wait = new WaitForSeconds(waitTime);
            plMan = PlayerLiveManager.instance;
            audiosource = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            inRange = true;
            timer = Time.time + waitTime;
            //StartCoroutine(WaitAndBait());
        }


        private void FixedUpdate()
        {
            if (inRange)
            {
                if (Time.time > timer)
                {
                    plMan.DamagePlayer(damage);
                    audiosource.clip = dethClipToPlay;
                    audiosource.Play();
                    inRange = false;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
           // StopAllCoroutines();
            inRange = false;
        }


        //IEnumerator WaitAndBait()
        //{
        //    yield return wait;
        //    //if (inRange)
             
        //}
    }
}
