﻿using UnityEngine;

namespace STDS
{
    public class EndPositionUpdate : MonoBehaviour
    {
        [SerializeField]
        string playerName = "PlayerAll";

        void Start()
        {
            Destroy(GameObject.Find(playerName).GetComponentInChildren<PlayerPosition2dUpdate>(true));
            SimpleFollowManager.instance.UpdatePlayerPosition(transform.position);
        }
    }
}
