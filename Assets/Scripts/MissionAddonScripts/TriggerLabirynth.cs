﻿using UnityEngine;

namespace STDS
{
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class TriggerLabirynth : MonoBehaviour
    {
        [SerializeField]
        Color spriteColorChange = Color.red;
        [SerializeField]
        protected RotatorScript rotScript = null;
       // [SerializeField]
       // AudioClip audioClip = null;

        AudioSource audiosource;
        SpriteRenderer spRend;
        bool used = false;
        Color startColor;


        void Start()
        {
            audiosource = GetComponent<AudioSource>();
            spRend = GetComponent<SpriteRenderer>();

            startColor = spRend.color;
        }


        void OnTriggerEnter2D(Collider2D collision)
        {
            if (PlayerCheck(collision))
            {
                SuccesAction();

                rotScript.RotateObject();
            }
        }


        protected bool PlayerCheck(Collider2D collision)
        {
            return (!used && collision.CompareTag("Player"));
        }


        protected virtual void SuccesAction()
        {
            audiosource.Play();
            used = true;
            spRend.color = spriteColorChange;
        }


        protected virtual void FailAction()
        {

        }


        public void ResetTrigger()
        {
            used = false;
            spRend.color = startColor;
        }
    }
}
