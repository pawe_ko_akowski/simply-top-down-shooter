﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class ScorePointsCalculator : MonoBehaviour
    {
        public bool basicScore = false;
        public bool smallArena = true;

        public Image sliderPointImage;
        public Text waveText, scoreText, scoreMultiplierText;
        public Animator scoreTextAnimator;
        public string animationName = "score";
        int animationIntName;

        public int scoreStaticMultiplier = 10;
        public AudioClip huraAudioclip;
        public float volumeBase = 1f;
        public float volumeIncrease = 0.1f;
        public int[] levelRankPoints; // 5, 10, 20, 40, 80, 160, 320, 640 1280 2560

        int actualLevel = 1;
        int monsterKil = 0;
        int score;
        int extraScore = 0;
        int allScore = 0;

        public float maxFill = 4;
        float actualFill = 0;
        public float addFill = 2f;

        public int staticRankAdd = 1;
        int fillLevel = 0;
        int fillLevelRnak = 0;
        int fillMultiplier = 0;

        AudioManager audioMan;
    

        // Use this for initialization
        void Start()
        {
            waveText.text = "1";
            scoreText.text = scoreMultiplierText.text = "0";
            sliderPointImage.fillAmount = 0;
            audioMan = AudioManager.instance;
            animationIntName = Animator.StringToHash(animationName);
        }

        public void GiveLevelInformation()
        {
            actualLevel++;
            waveText.text = actualLevel.ToString();
        }

        public void GiveKilInformation()
        {
            monsterKil++;
            if (basicScore)
            {
                CalculateBasicScore();
                CalculateAllScore();
                ShowAllScore();
            }
        }

        public void GiveIncreaseInformation(bool succes = true)
        {
            if (succes)
            {
                FillRank();
                SliderUp();
            }
            else
            {
                EndExtraScoreCalculation();
            }
            ShowFillAmount();
        }


        public void SaveScore()
        {
            if (StoreSTDSGameData.instance != null)
                if (smallArena)
                {
                    if(allScore > StoreSTDSGameData.instance.stdsData.smallArenaHighscore)
                    StoreSTDSGameData.instance.stdsData.smallArenaHighscore = allScore;
                    if(actualLevel > StoreSTDSGameData.instance.stdsData.smallArenaWaveHigscore)
                    StoreSTDSGameData.instance.stdsData.smallArenaWaveHigscore = actualLevel;
                }
                else
                {
                    if (allScore > StoreSTDSGameData.instance.stdsData.bigArenaHighscore)
                        StoreSTDSGameData.instance.stdsData.bigArenaHighscore = allScore;
                    if (actualLevel > StoreSTDSGameData.instance.stdsData.bigArenaWaveHighscore)
                        StoreSTDSGameData.instance.stdsData.bigArenaWaveHighscore = actualLevel;
                }
        }

        private void FillRank()
        {
            fillLevel++;

            if (fillLevel >= levelRankPoints[fillLevelRnak])
            {
                fillLevelRnak++;
                fillMultiplier = fillLevelRnak + staticRankAdd;
                ShowMultiplier();
            }
        }

        private void SliderUp()
        {
            actualFill += addFill;
            if (actualFill > maxFill)
                actualFill = maxFill;
        }

        private void CalculateBasicScore()
        {
            score += scoreStaticMultiplier;
        }


        private void CalculateExtraScore()
        {
            extraScore += fillMultiplier * scoreStaticMultiplier * fillLevel;
        }


        private void CalculateAllScore()
        {
            allScore = score + extraScore;
        }


        private void ShowAllScore()
        {
            ShowScore();
            ShowMultiplier();
        }

        private void ShowMultiplier()
        {
            if (fillMultiplier > 0)
                scoreMultiplierText.text = fillMultiplier.ToString();
            else
                scoreMultiplierText.text = "";
        }

        private void ShowScore()
        {
            scoreText.text = allScore.ToString();
        }

        private void ResetFill()
        {
                actualFill = 0;
                fillLevel = 0;
                fillMultiplier = 0;
                fillLevelRnak = 0;           
        }


        private void EndExtraScoreCalculation()
        {
            Play2DAudioGoodSound();
            CalculateExtraScore();
            CalculateAllScore();
            ResetFill();
            ShowAllScore();
        }


        private void ShowFillAmount()
        {
            sliderPointImage.fillAmount = actualFill / maxFill;
        }


        private void Play2DAudioGoodSound()
        {
            if (fillLevelRnak > 0)
            {
                audioMan.Play2DAudio(huraAudioclip, volumeBase + fillLevelRnak * volumeIncrease, Random.Range(0.9f, 1.1f));
                scoreTextAnimator.SetTrigger(animationIntName);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (actualFill > 0)
            {
                actualFill -= Time.deltaTime;
                if (actualFill < 0)
                {
                    
                    EndExtraScoreCalculation();
                }
                ShowFillAmount();
            }
        }
    }
}
