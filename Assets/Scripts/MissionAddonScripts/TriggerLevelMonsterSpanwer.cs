﻿using UnityEngine;
using System.Collections;

namespace STDS
{
    public class TriggerLevelMonsterSpanwer : MonoBehaviour
    {
        [SerializeField]
        GameObject[] hardRock = null, lightRock = null;
        [SerializeField]
        int waveForThis = 0;
        [SerializeField]
        private AudioClip doorOpenAudioClip = null;

        Collider2D thisCollider;
        LevelMonsterSpawner lMS;
        Collider2D[] colliderInLightRock;
        Animator[] animatorInLIghtRock;
        AudioSource[] audiosurceInLightRock;
       // WaitForSeconds wait = new WaitForSeconds(.45f);
        int allGates;
        int gatesCounter = 0;
        bool ready = false;
        float timer = 0;
        // PlaceManager pM ;


        void Start()
        {
           // Debug.Log("Start");
            colliderInLightRock = new Collider2D[lightRock.Length];
            animatorInLIghtRock = new Animator[lightRock.Length];
            audiosurceInLightRock = new AudioSource[lightRock.Length];
            lMS = FindObjectOfType<LevelMonsterSpawner>();
           // pM = FindObjectOfType<PlaceManager>();
            for (int i = 0; i < hardRock.Length; i++)
            {
                hardRock[i].SetActive(false);
            }
            for (int j = 0; j < lightRock.Length; j++)
            {
                colliderInLightRock[j] = lightRock[j].GetComponent<Collider2D>();
                animatorInLIghtRock[j] = lightRock[j].GetComponentInChildren<Animator>();
                audiosurceInLightRock[j] = lightRock[j].GetComponentInChildren<AudioSource>();
                lightRock[j].SetActive(false);
            }
            thisCollider = GetComponent<Collider2D>();
            allGates = hardRock.Length + lightRock.Length;
        }


        private void FixedUpdate()
        {
            //if (Input.GetButtonDown("Fire1"))
            //{
            //    hardRock[0].SetActive(true);
            //}

            if (ready)
                if (gatesCounter < allGates)
                {
                    if (Time.time > timer)
                    {
                        if (gatesCounter < hardRock.Length)
                        {
                            hardRock[gatesCounter].SetActive(true);
                            //Debug.Log("Dupa " + gatesCounter);
                        }
                        else
                        {
                            lightRock[gatesCounter - hardRock.Length].SetActive(true);
                            colliderInLightRock[gatesCounter - hardRock.Length].enabled = (true);
                            //Debug.Log("Dupa " + gatesCounter);
                        }
                        gatesCounter++;
                        timer = Time.time + 0.45f;
                    }


                    // yield return wait;
                }
                else
                {
                    ready = false;
                    Destroy(this);
                }
        }


        //private IEnumerator CloseObstacles()
        //{
        //    while (gatesCounter < allGates)
        //    {
        //        if (gatesCounter < hardRock.Length)
        //        {                  
        //            hardRock[gatesCounter].SetActive(true);
        //        }
        //        else
        //        {
        //            lightRock[gatesCounter - hardRock.Length].SetActive(true);
        //            colliderInLightRock[gatesCounter - hardRock.Length].enabled = (true);
        //        }
        //        gatesCounter++;

        //        yield return wait;
        //    }
        //}


        private void OpenLightObstacle(int element)
        {
          //  if (colliderInLightRock.Length > 0)
           // {
                colliderInLightRock[element].enabled = false;
                animatorInLIghtRock[element].SetTrigger("open");
                audiosurceInLightRock[element].PlayOneShot(doorOpenAudioClip);
            //}
        }


        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                lMS.StartNextWave(waveForThis);
                //for (int i = 0; i < hardRock.Length; i++)
                //{
                //    hardRock[i].SetActive(true);
                //}
                //for (int j = 0; j < lightRock.Length; j++)
                //{
                //    lightRock[j].SetActive(true);
                //}
                thisCollider.enabled = false;
                ready = true;
               // StartCoroutine(CloseObstacles());
            }
        }
    

        public int NextWave()
        {
            for (int j = 0; j < lightRock.Length; j++)
            {
                //lightRock[j].SetActive(false);
                OpenLightObstacle(j);
            }
            return waveForThis;
        }
    }
}
