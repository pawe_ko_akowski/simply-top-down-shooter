﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class ArenasParticlesRemover : MonoBehaviour
    {

        public ParticleSystem[] ps;
        public float distToRecognize = 2f;
        public Color[] color;
        public int particleForRow = 100;
        ParticleSystem.Particle[][] p;
        float timer;
        public float startSize = 2.8f;


        void Start()
        {
            p = new ParticleSystem.Particle[ps.Length][];
            for (int i = 0; i < p.Length; i++)
            {
                var w = ps[i].main;
                for (int s = 0; s < particleForRow; s++)
                {
                    w.startColor = color[Random.Range(0, color.Length)];
                    float add = Random.Range(0, .3f) - 0.1f;
                    w.startSize = startSize + add;
                    ps[i].Emit(1);
                }
                p[i] = new ParticleSystem.Particle[ps[i].main.maxParticles];
            }

            for (int i = 0; i < p.Length; i++)
            {
                int maxPart = ps[i].GetParticles(p[i]);
                bool[] toDelete = new bool[maxPart];
                for (int l = 0; l < toDelete.Length; l++)
                    toDelete[l] = false;

                for (int j = 0; j < maxPart; j++)
                {
                    for (int k = 0; k < maxPart; k++)
                    {
                        if (toDelete[j] != true)
                        {
                            if (j != k)
                            {
                                float dist = Vector3.Distance(p[i][j].position, p[i][k].position);

                                if (dist < distToRecognize)
                                {
                                    toDelete[k] = true;
                                    continue;
                                }
                            }
                        }
                    }
                }

                List<ParticleSystem.Particle> particles = new List<ParticleSystem.Particle>();
                for (int m = 0; m < toDelete.Length; m++)
                {
                    if (toDelete[m])
                    { }
                    else
                    {
                        particles.Add(p[i][m]);
                    }
                }
                ps[i].SetParticles(particles.ToArray(), toDelete.Length);
            }
        }


        void FixedUpdate()
        {
            timer += Time.deltaTime;
            if (timer > .3f)
            {
                for (int i = 0; i < ps.Length; i++)
                {
                    ParticleSystem.CollisionModule cm = ps[i].collision;
                    cm.enabled = false;
                }
                Destroy(this);
            }
        }
    }
}

