﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class Timer : MonoBehaviour
    {
        [SerializeField]
        Text timerText = null;
        float timer = 0;


        void FixedUpdate()
        {
            timer += Time.deltaTime;
            timerText.text = timer.ToString("F2");
        }
    }
}
