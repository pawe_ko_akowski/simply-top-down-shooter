﻿using UnityEngine;

namespace STDS
{
    public class BossFightHouseDamageActivator : MonoBehaviour
    {
        public static BossFightHouseDamageActivator instance = null;
        bool active = false;
        Collider2D bossCollider = null;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            Activate(false);
        }

        public void Activate(bool what)
        {
            // instance.enabled = what;
            active = what;
        }

        public void SetBossCollider(Collider2D col)
        {
            bossCollider = col;
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (active && collision == bossCollider)
            {
                AllyHouseHelthManager.instance.DamageHouses(true);
            }
        }


        public void OnTriggerExit2D(Collider2D collision)
        {
            if (active && collision == bossCollider)
            {
                AllyHouseHelthManager.instance.DamageHouses(false);
                active = false;
            }
        }
    }
}
