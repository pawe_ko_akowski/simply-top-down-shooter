﻿using UnityEngine;

namespace STDS
{
    public class BossFightImmortalityHouses : MonoBehaviour
    {
        [SerializeField]
        bool useIt = false;

        AllyHouseHelthManager allHousesMan;


        private void Start()
        {
            if (!useIt)
            {
                Destroy(this);
                return;
            }
            allHousesMan = AllyHouseHelthManager.instance;
            ImoortalNow();
        }


        public void ImoortalNow()
        {
            allHousesMan.DamageHouses(false);
        }
    }
}
