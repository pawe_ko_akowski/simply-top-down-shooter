﻿using UnityEngine;

namespace STDS
{
    public class EndSuccesOnTrigger : MonoBehaviour
    {
        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                GameManager.instance.SuccesEndGame();
            }
        }
    }
}
