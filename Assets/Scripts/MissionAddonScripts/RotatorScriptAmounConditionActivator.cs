﻿using UnityEngine;

namespace STDS
{
public class RotatorScriptAmounConditionActivator : RotatorScripsAmountCondition
    {
            [SerializeField]
            GameObject obToActivate = null;

        [SerializeField]
        GameObject[] barierObiect = null;


        public void Start()
        {
            obToActivate.SetActive(false);
        }

        public override bool RotateObject()
            {
                if(amount < barierObiect.Length)
                barierObiect[amount].SetActive(false);

                amount++;
                if (amount >= triggerLab.Length)
                {
                    obToActivate.SetActive(true);
                    Mission10StatueAnimActivator.instance.ActivateStatues();
                    return true;
                }
                return false;
            }
    }
}
