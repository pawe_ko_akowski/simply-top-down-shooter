﻿
using UnityEngine;

namespace STDS
{
    public class BossFightHouseCounter : MonoBehaviour
    {

        public static BossFightHouseCounter instance = null;
        int amount;
        bool stilLive = true;

        [SerializeField]
        GameObject[] gobToTurnOff = null;

        [SerializeField]
        GameObject[] gobToTurnOn = null;

        public bool StilLive
        {
            get
            {
                return stilLive;
            }
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        public void Register()
        {
            amount++;
        }


        public void Unregister()
        {
            amount--;

            if (amount <= 0)
            {
                for (int i = 0; i < gobToTurnOff.Length; i++)
                {
                    gobToTurnOff[i].SetActive(false);
                    stilLive = false;
                }

                for (int i = 0; i < gobToTurnOn.Length; i++)
                {
                    if (gobToTurnOn[i] != null)
                        gobToTurnOn[i].SetActive(true);
                }
            }
        }
    }
}
