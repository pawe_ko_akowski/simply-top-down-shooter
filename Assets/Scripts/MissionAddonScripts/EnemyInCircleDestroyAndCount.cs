﻿using UnityEngine;

namespace STDS
{
    public class EnemyInCircleDestroyAndCount : MonoBehaviour {

        [SerializeField]
        int endGameCounter = 10;

        int counter = 0;


        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (!collision.isTrigger)
            {
                collision.gameObject.SetActive(false);
                counter++;
                CounterCheck();
            }
        }


        void CounterCheck()
        {
            if (counter >= endGameCounter)
            {
                GameManager.instance.EndGameFinaly();
            }
        }
    }
}
