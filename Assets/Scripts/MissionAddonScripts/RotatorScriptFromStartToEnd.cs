﻿using UnityEngine;

namespace STDS
{
    public class RotatorScriptFromStartToEnd : RotatorScripsAmountCondition
    {
        [SerializeField]
        AudioClip  failureSound = null;
        AudioSource audiosource;
        int[] fromStartToEnd;
        [SerializeField]
        GameObject mysteryToFindGob = null;


        void ResetAllTriggers()
        {
            audiosource.PlayOneShot(failureSound);
            amount = 0;
            for (int i = 0; i < triggerLab.Length; i++)
            {
                triggerLab[i].ResetTrigger();
            }
        }


        bool CorrectChose(TriggerLabirynth tL)
        {
            int what = 0;
            for (int i = 0; i < triggerLab.Length; i++)
            {
                if (tL == triggerLab[i])
                {
                    what = i;
                    break;
                }
            }
            if (fromStartToEnd[what] == amount)
                return true;
            return false;
        }


        protected override void Init()
        {
            audiosource = GetComponent<AudioSource>();
            fromStartToEnd = new int[triggerLab.Length];
            if (StoreSTDSGameData.instance != null && StoreSTDSGameData.instance.stdsData.campignEndOnce > 0)
                fromStartToEnd[0] = 0;
            else
                fromStartToEnd[0] = Random.Range(0, triggerLab.Length);
            int last = fromStartToEnd[0];
            for (int i = 1; i < fromStartToEnd.Length; i++)
            {
                fromStartToEnd[i] = last + 1;
                if (fromStartToEnd[i] >= fromStartToEnd.Length)
                    fromStartToEnd[i] = 0;
                last = fromStartToEnd[i];
            }
            base.Init();
        }


        public override bool RotateObjectTo(TriggerLabirynth trigLab)
        {
            if (CorrectChose(trigLab))
            {
                RotateObject();
                if (mysteryToFindGob != null)
                    mysteryToFindGob.SetActive(true);
               // audiosource.PlayOneShot(succesSound);
                return true;
            }


            ResetAllTriggers();
            return false;
        }
    }
}
