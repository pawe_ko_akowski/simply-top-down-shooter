﻿using UnityEngine;
using System;
using STDS.Steer2D;

namespace STDS
{
    public class BossMonsterSpawner : MonoBehaviour {

        [Serializable]
        class EnemySpawner
        {
            public GameObject enemyPrefab = null;
            public int enemyAmount = 0;
        }

        [SerializeField]
        EnemySpawner[] enemySpawner = null;
        [SerializeField]
        int dataInfoToRun = 900;
        [SerializeField]
        float whatTimeToSpawn = 2f;
        [SerializeField]
        bool spawnInAllPlaces = false;

        GameObject[][] enemies;
        int[] countForEvery;
        GameObject helpGob = null;
        BossPlacesForSpawn bossPlacesForSpawn;
        float lastTime = 0;
        bool spawn = false;

        int actualWave = 0;

        private void FixedUpdate()
        {
            if (spawn)
            {
                float difrence = (Time.time - lastTime);

                if (difrence > whatTimeToSpawn)
                {
                    lastTime = Time.time -(difrence -whatTimeToSpawn);
                    if (spawnInAllPlaces)
                    {
                        for (int i = 0; i < bossPlacesForSpawn.GetSpawnPlaces.Length; i++)
                        {
                            Spawn(bossPlacesForSpawn.GetNextSpawnPlace.position);
                        }
                    }
                    else
                    {
                        Spawn(bossPlacesForSpawn.GetNextSpawnPlace.position);
                    }

                }
            }
        }

        void HideAllMonsters()
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                for (int j = 0; j < enemies[i].Length; j++)
                {
                    enemies[i][j].SetActive(false);
                }
            }
        }

        public void DataInfo(int data)
        {
            if (data <= 0)
            {
                HideAllMonsters();
                spawn = false;
                return;
            }

            if (data < dataInfoToRun)
            {
                if (!spawn)
                {
                    spawn = true;
                    lastTime = Time.time - whatTimeToSpawn;
                }
            }
            else
            {
                spawn = false;
            }
        }

        private void Start()
        {
            bossPlacesForSpawn = GetComponent<BossPlacesForSpawn>();
            enemies = new GameObject[enemySpawner.Length][];
            countForEvery = new int[enemySpawner.Length];

            for (int i = 0; i < enemySpawner.Length; i++)
            {
                enemies[i] = new GameObject[enemySpawner[i].enemyAmount];
                for (int j = 0; j < enemies[i].Length; j++)
                {
                    enemies[i][j] = Instantiate(enemySpawner[i].enemyPrefab);
                    enemies[i][j].GetComponent<EnemyHealth>().DontCountThisEnemy();
                    MeleAttack ma = enemies[i][j].GetComponent<MeleAttack>();
                    if (ma != null)
                        ma.SetNoHouseAttack();
                    EnemyBossEvade ebe = enemies[i][j].AddComponent<EnemyBossEvade>();
                    ebe.SetBossRigidbody(GetComponent<Rigidbody2D>());
                    ebe.SetRadius(8.4f);
                    ebe.Weight = 5f;
                    enemies[i][j].SetActive(false);
                }

            }
        }


        public void Spawn(Vector3 pos)
        {
            if (countForEvery[actualWave] < enemySpawner[actualWave].enemyAmount)
            {
                helpGob = enemies[actualWave][countForEvery[actualWave]];
                if (!helpGob.activeSelf)
                {
                    helpGob.transform.position = pos;
                    helpGob.SetActive(true);
                }
                countForEvery[actualWave]++;
            }
            else
            {
                actualWave++;
                if (actualWave >= enemies.Length)
                {
                    actualWave = 0;
                    for (int i = 0; i < countForEvery.Length; i++)
                        countForEvery[i] = 0;

                }
            }
        }
    }
}
