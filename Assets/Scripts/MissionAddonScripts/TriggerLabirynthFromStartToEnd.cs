﻿using UnityEngine;

namespace STDS
{
    public class TriggerLabirynthFromStartToEnd : TriggerLabirynth
    {
        void OnTriggerEnter2D(Collider2D collision)
        {
            if (PlayerCheck(collision))
            {
                if (rotScript.RotateObjectTo(this))
                {
                    SuccesAction();
                }  
            }
        }
    }
}
