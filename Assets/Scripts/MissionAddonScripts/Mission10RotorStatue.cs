﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class Mission10RotorStatue : MonoBehaviour
    {
        [SerializeField]
        Animator anim = null;
        bool activeStatue = false;

        // Use this for initialization
        //void Start()
        //{
        //    anim = GetComponent<Animator>();
        //}

        public void ActiveStatue()
        {
            activeStatue = true;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            ActiveAndRotateStatue();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            ActiveAndRotateStatue();
        }

        void ActiveAndRotateStatue()
        {
            if (activeStatue)
            {
                activeStatue = false;
                anim.enabled = true;
                Mission10StatueAnimActivator.instance.UnlockMystery();
                Destroy(this);
            }
        }
    }
}
