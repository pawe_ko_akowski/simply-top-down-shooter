﻿using UnityEngine;

namespace STDS
{
    public class TriggerLabirynthRotSet : TriggerLabirynth
    {
        [SerializeField]
        float rotat = 0;


        void OnTriggerEnter2D(Collider2D collision)
        {
            if (PlayerCheck(collision))
            {
                SuccesAction();
                rotScript.RotateObjectTo(rotat);
            }
        }
    }
}
