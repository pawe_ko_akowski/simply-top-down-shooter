﻿using UnityEngine;

namespace STDS
{
    public class PlaceManager : MonoBehaviour {

        [SerializeField]
        PlaceToSpawn[] placesToSpawn = null;
        [SerializeField]
        TriggerLevelMonsterSpanwer[] triggerLevMonSpawn = null;

        int actualWave = 0;
        //int lastWave = 0;
        LevelMonsterSpawner lMonSpawner;


        void Start()
        {
            lMonSpawner = GetComponent<LevelMonsterSpawner>();
            lMonSpawner.AddDelegateToNextWaveDelegate(NextWave);
           // lMonSpawner.AsignPlaceFinder(placesToSpawn[actualWave]);
        }


        void NextWave()
        {
            //triggerLevMonSpawn[actualWave].NextWave();
            //actualWave++;
            triggerLevMonSpawn[actualWave].NextWave();
           // actualWave = lMonSpawner.CurrentWave;
         ///   lMonSpawner.AsignPlaceFinder(placesToSpawn[actualWave]);
            // actualWave = lMonSpawner.CurrentWave;

            //if (actualWave >= placesToSpawn.Length)
            //    actualWave = placesToSpawn.Length - 1;

            //lMonSpawner.AsignPlaceFinder(placesToSpawn[actualWave]);
        }

        public void AsignNext()
        {
            actualWave = lMonSpawner.CurrentWave;
            if (actualWave >= placesToSpawn.Length)
                actualWave = placesToSpawn.Length - 1;
            // Debug.Log(actualWave);
            lMonSpawner.AsignPlaceFinder(placesToSpawn[actualWave]);
        }


        //public void SendActualPlace(int numb)
        //{
        //    actualWave = numb;
        //}
    }

}