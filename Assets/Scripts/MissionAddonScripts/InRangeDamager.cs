﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class InRangeDamager : MonoBehaviour
    {
        [SerializeField]
        int damage = 10;
        [SerializeField]
        float attackBreak = 0.4f;

        List<Collider2D> enemyCollider = new List<Collider2D>();
        float timer;


        public void FixedUpdate()
        {
            timer -= Time.fixedDeltaTime;
            if (timer <= 0 && enemyCollider.Count > 0)
            {
                //Debug.Log("mam");
                for(int i =0; i<enemyCollider.Count; i++)
                    EnemyManager.instance.DamageInstanceId(enemyCollider[i].transform.GetInstanceID(), damage);
                timer = attackBreak;
            }
        }


        public void OnTriggerEnter2D(Collider2D collision)
        {
            if(!collision.isTrigger)
            enemyCollider.Add(collision);
        }


        public void OnTriggerExit2D(Collider2D collision)
        {
            if(!collision.isTrigger)
            enemyCollider.Remove(collision);
        }
    }
}
