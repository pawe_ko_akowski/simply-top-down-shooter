﻿using UnityEngine;

namespace STDS
{
    public class SixCampignKeyActivator : MonoBehaviour
    {
        //[SerializeField]
        //int doorActivator = 0;
        SixCampignKeyManager keyManager;
        public AudioClip takeAudiocClip;


        void Start()
        {
            keyManager = GameObject.FindObjectOfType<SixCampignKeyManager>();
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            keyManager.KeyFind();
            AudioManager.instance.Play2DAudio(takeAudiocClip);
            Destroy(gameObject);
        }
    }
}
