﻿using UnityEngine;

namespace STDS
{
    public class RotatorScripsAmountCondition : RotatorScript
    {
        [SerializeField]
        protected TriggerLabirynth[] triggerLab;

        protected int amount = 0;


        public override bool RotateObject()
        {
            amount++;
            if(amount >= triggerLab.Length)
               return base.RotateObject();
            return false;
        }
    }
}

