﻿using UnityEngine;

namespace STDS
{
    public class EnemyEvolutionProgress : MonoBehaviour
    {
        public MeleAttack meleAttack;
        public Helth health;
        public SimpleFollow follow;


        public void ProgresMonster(LevelEvolution e)
        {
            if (meleAttack != null)
                meleAttack.ProgressDamage(e.damageAdd);
            health.ProgressHelth(e.liveAdd);
            health.ProgressArmor(e.armorAdd);
            follow.ProgressSpeed(e.speedAdd);
        }
    }
}
