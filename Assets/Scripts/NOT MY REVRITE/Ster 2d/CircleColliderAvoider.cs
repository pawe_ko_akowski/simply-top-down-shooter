﻿using UnityEngine;

namespace STDS.Steer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CircleColliderAvoider : SteeringBehaviour
    {
        public Rigidbody2D thisRig;
        public SimpleFollow simFol;
        public CircleCollider2D circleCol;
        public string colliderTag = "wall";

        private bool wallCircle = false;
        private int colliderId = 0;
        private float colliderRadius = 0, thisRadius = 0;
        private Vector2 collPos;
        private Vector2 dir;
        private RadiusColliderManager radiusManager = null;
        private Vector2 helpOne, helpTwo, helpThree;


        protected override void Init()
        {
            thisRig = GetComponent<Rigidbody2D>();
            simFol = GetComponent<SimpleFollow>();
            radiusManager = RadiusColliderManager.instance;
            thisRadius = circleCol.radius * transform.localScale.x;
        }


        private void OnTriggerStay2D(Collider2D collision)
        {
            if(!wallCircle)
            if(collision.CompareTag(colliderTag))
            {
                colliderId = collision.GetInstanceID();
                collPos = collision.transform.position;
                colliderRadius = radiusManager.GetCircleColliderRadius(colliderId);
                colliderRadius += thisRadius;
                wallCircle = true;
            }
        }


        public override Vector2 GetVelocity()
        {
            if (wallCircle)
            {
                helpOne = simFol.GetPlayerDirectionNormalized();
                helpTwo = collPos - thisRig.position;
                dir = (collPos - thisRig.position).normalized;
                helpThree.x = -dir.y;
                helpThree.y = dir.x;
                if ((helpOne.x * helpTwo.y - helpOne.y * helpTwo.x) < 0)
                {
                    dir = helpThree;// * (radius + thisRadius));
                }
                else
                {
                    dir =-helpThree;// * (radius + thisRadius));
                }
                wallCircle = false;
                return dir.normalized;
            }
            else
            return Vector2.zero;
        }
    }
}
