﻿using UnityEngine;

namespace STDS.Steer2D
{
    public class BossRigidBodySeek : RigidBodySeek
    {
       // Helth helth;

        bool moreThanHalf = true;
        [SerializeField]
        Vector2 placeToGo = Vector2.zero;
        Vector2 dir;


        public void Rescue(bool decision)
        {
            moreThanHalf = !decision;
           // Debug.Log(moreThanHalf);
        }
        //private void Awake()
        //{
        //    helth = GetComponent<Helth>();
        //}

        public Vector2 GetDestinationDirection()
        {
            return dir;
        }

        public override Vector2 GetVelocity()
        {
            if (!BossFightHouseCounter.instance.StilLive)
            {
                dir = base.GetVelocity();
                return dir;
            }

            if (moreThanHalf)
            {
                //  Debug.Log(base.GetVelocity());
                dir = base.GetVelocity();
                return dir;
            }
            else
            {
                //Debug.Log("TO " + (placeToGo - thisRig.position).normalized);
                dir = (placeToGo - thisRig.position).normalized;
                return dir;

            }


        }

        //void OnDrawGizmos()
        //{
        //    if (DrawGizmos)
        //    {
        //        Gizmos.color = Color.blue;
        //        Gizmos.DrawWireSphere(transform.position, 3);
        //    }
        //}
    }
}
