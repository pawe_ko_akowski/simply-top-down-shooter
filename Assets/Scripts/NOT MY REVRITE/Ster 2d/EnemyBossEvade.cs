﻿using UnityEngine;

namespace STDS.Steer2D
{
    public class EnemyBossEvade : SteeringBehaviour
    {
        Rigidbody2D bossRig = null;
        //Transform bossTransform = null;
        Rigidbody2D thisRig = null;
        float radius;

        

        public void SetBossRigidbody(Rigidbody2D rig)
        {
            bossRig = rig;
           // bossTransform = bossRig.transform;
        }

        public void SetRadius(float radiuss)
        {
            radius = radiuss;
        }


        public override Vector2 GetVelocity()
        {
            float distance = Vector2.Distance(thisRig.position, bossRig.position);//, bossTransform.position);

            if (distance < radius)
            {
                return (thisRig.position - bossRig.position).normalized;
            }
            else
                return Vector2.zero;
        }


        private void Start()
        {
            thisRig = GetComponent<Rigidbody2D>();
        }
    }
}
