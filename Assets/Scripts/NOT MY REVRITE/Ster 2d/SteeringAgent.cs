﻿using UnityEngine;
using System.Collections.Generic;

namespace STDS.Steer2D
{
    [RequireComponent(typeof(SimpleFollow))]
    public class SteeringAgent : MonoBehaviour
    {
        public float MaxVelocity
        {
            get
            {
               return simFol.GetActiveSpeed();
            }
        }

        public SimpleFollow simFol;
        [HideInInspector]
        public Vector2 CurrentVelocity;
        public static List<SteeringAgent> AgentList = new List<SteeringAgent>();
        public Rigidbody2D rig;

        private Vector2 returnVector;
        private List<SteeringBehaviour> behaviours = new List<SteeringBehaviour>();


        public void AddToAgentList()
        {
            AgentList.Add(this);
        }


        public void RemoveFromAgentList()
        {
            AgentList.Remove(this);
        }


        public void RegisterSteeringBehaviour(SteeringBehaviour behaviour)
        {
            behaviours.Add(behaviour);
        }


        public void DeregisterSteeringBehaviour(SteeringBehaviour behaviour)
        {
            behaviours.Remove(behaviour);
        }


        private void OnEnable()
        {
            AgentList.Add(this);
        }


        private void OnDisable()
        {
            AgentList.Remove(this);
        }


        public Vector2 GetMoveVector()
        {
            returnVector = Vector2.zero;
            for(int i =0;i<behaviours.Count; i++)
            //foreach (SteeringBehaviour behaviour in behaviours)
            {
                if (behaviours[i].enabled)
                    returnVector += behaviours[i].GetVelocity() * behaviours[i].Weight;
            }
            CurrentVelocity = returnVector.normalized;
            return CurrentVelocity;
        }
    }
}