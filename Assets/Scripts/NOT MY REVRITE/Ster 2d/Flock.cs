﻿using UnityEngine;
using System.Collections.Generic;

namespace STDS.Steer2D
{
    public class Flock : SteeringBehaviour
    {
        public float NeighbourRadius = 1f;
        public float AlignmentWeight = .7f;
        public float CohesionWeigth = .5f;
        public float SeperationWeight = .2f;

        private List<SteeringAgent> neighbouringAgents = new List<SteeringAgent>();
        private Vector2 currentPosition;
        private Vector2 helpDir;


        public override Vector2 GetVelocity()
        {
            currentPosition = (Vector2)transform.position;
            UpdateNeighbouringAgents();
            return alignment() * AlignmentWeight + cohesion() * CohesionWeigth + seperation() * SeperationWeight;
        }


        Vector2 alignment()
        {
            helpDir = Vector2.zero;
            if (neighbouringAgents.Count == 0)
                return helpDir;
            for (int i=0;i<neighbouringAgents.Count; i++)// agent in neighbouringAgents)
                helpDir += neighbouringAgents[i].CurrentVelocity;
            helpDir /= neighbouringAgents.Count;
            return helpDir.normalized;
        }


        Vector2 cohesion()
        {
            helpDir = Vector2.zero;
            for (int i = 0; i < neighbouringAgents.Count; i++)// agent in neighbouringAgents)
                helpDir += neighbouringAgents[i].rig.position;
            helpDir /= neighbouringAgents.Count;
            return (helpDir - currentPosition).normalized;
        }

        Vector2 seperation()
        {
           helpDir = Vector2.zero;
            for (int i = 0; i < neighbouringAgents.Count; i++)
                helpDir += neighbouringAgents[i].rig.position - currentPosition;
            return (helpDir * -1);
        }


        void UpdateNeighbouringAgents()
        {
            neighbouringAgents.Clear();
            list = SteeringAgent.AgentList;
            for (int i =0;i<list.Count; i++)
            {
                if (Vector2.Distance(list[i].rig.position, currentPosition) < NeighbourRadius)
                    neighbouringAgents.Add(list[i]);
            }
        }
    }
}
