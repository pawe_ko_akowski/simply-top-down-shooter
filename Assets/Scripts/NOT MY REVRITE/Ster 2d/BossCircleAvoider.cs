﻿using System;
using UnityEngine;

namespace STDS.Steer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    //[RequireComponent(typeof(SimpleFollow))]
    public class BossCircleAvoider : SteeringBehaviour
    {
        // Rigidbody2D targetRig;
        [SerializeField]
        BossRigidBodySeek boosRigSeek = null;

        Rigidbody2D thisRig = null;
        // public float FleeRadius = 1.0f;
        //public bool DrawGizmos = false;
        //SimpleFollow simFol;

        bool wallCircle = false;
        int colliderId = 0;
        float colliderRadius = 0, thisRadius = 0;
        Vector2 collPos;
        Vector2 dir;

        RadiusColliderManager radiusManager = null;

        [SerializeField]
        string colliderTag = "wall";

        private void Start()
        {
            thisRig = GetComponent<Rigidbody2D>();
           // simFol = GetComponent<SimpleFollow>();
            radiusManager = RadiusColliderManager.instance;
            thisRadius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!wallCircle)
                if (collision.CompareTag(colliderTag))
                {
                    colliderId = collision.GetInstanceID();
                    collPos = collision.transform.position;
                    colliderRadius = radiusManager.GetCircleColliderRadius(colliderId);
                    colliderRadius += thisRadius;
                    wallCircle = true;


                    Vector2 one = boosRigSeek.GetDestinationDirection();
                    Vector2 two = collPos - thisRig.position;
                    dir = (collPos - thisRig.position).normalized;

                    Vector2 dirNegative = new Vector2(-dir.y, dir.x);

                    if ((one.x * two.y - one.y * two.x) < 0)
                    {

                        dir += dirNegative;// * (radius + thisRadius));

                        //dir = new Vector2(hit.collider.transform.position.x, hit.collider.transform.position.y)
                        //    +
                        //     new Vector2(-one.y, one.x) * (radius + thisRadius);



                    }
                    else
                    {

                        dir -= dirNegative;// * (radius + thisRadius));

                    }

                    // Debug.Log("1");
                }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (wallCircle)
            {
                if (collision.GetInstanceID() == colliderId)
                {
                    wallCircle = false;
                    // Debug.Log("2");
                }
            }
        }

        public override Vector2 GetVelocity()
        {
            if (wallCircle)
            {


                return dir.normalized;
            }
            else
                return Vector2.zero;
            // return simFol.GetPlayerDirectionNormalized();// (targetRig.position - thisRig.position);
            // float distance = Vector3.Distance(transform.position, TargetAgent.transform.position);

            // if (distance < FleeRadius)
            // {
            // float t = distance / TargetAgent.MaxVelocity;
            // Vector2 targetPoint = (Vector2)TargetAgent.transform.position + TargetAgent.CurrentVelocity * t;

            // return -(((targetPoint - (Vector2)transform.position).normalized * agent.MaxVelocity) - agent.CurrentVelocity);
            // }
            //else
            // return Vector2.zero;
        }

        //void OnDrawGizmos()
        //{
        //    if (DrawGizmos)
        //    {
        //        Gizmos.color = Color.blue;
        //        Gizmos.DrawWireSphere(transform.position, 3);
        //    }
        //}
    }
}
