﻿using UnityEngine;

namespace STDS.Steer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SimpleFollow))]
    public class RigidBodySeek : SteeringBehaviour
    {
        public Rigidbody2D thisRig;
        public SimpleFollow simFol;

        public override Vector2 GetVelocity()
        {
            return simFol.GetPlayerDirectionNormalized();
        }
    }
}
