﻿using UnityEngine;
using System.Collections.Generic;

namespace STDS.Steer2D
{

        [RequireComponent(typeof(SteeringAgent))]
        public abstract class SteeringBehaviour : MonoBehaviour
        {
            private interface Remover
            {
                void Remove();
                void Add();

            }

        private class NoneRemover : Remover
        {
            public void Add()
            {

            }

            public void Remove()
            {

            }
        }

        private class AgentRemover : Remover
        {
            SteeringAgent agent;
            SteeringBehaviour steerBeh;

            public void Remove()
            {
                agent.DeregisterSteeringBehaviour(steerBeh);
            }

            public void Add()
            {
                agent.RegisterSteeringBehaviour(steerBeh);
            }

            public AgentRemover(SteeringAgent ag, SteeringBehaviour sb)
            {
                agent = ag;
                steerBeh = sb;
            }
        }


        public float Weight = 1;

            protected SteeringAgent agent;
            protected List<SteeringAgent> list;
            Remover remover;
            public abstract Vector2 GetVelocity();

            void Awake()
            {
            agent = GetComponent<SteeringAgent>();
            //SteeringAgent agent = GetComponent<SteeringAgent>();
            if (agent != null)
                remover = new AgentRemover(agent, this);
            else
                remover = new NoneRemover();

                list = SteeringAgent.AgentList;
                Init();
            }

        protected virtual void Init()
        {

        }

        private void OnEnable()
        {
            remover.Add();
        }

        private void OnDisable()
        {
            remover.Remove();
        }

        //void OnDestroy()
        //    {
                
        //    }
        }
    }