﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{   
    public class SkillFiller : MonoBehaviour
    {
        //live images
        [SerializeField]
        private Image liveAmountImage = null, liveHealImage = null, liveArmor = null;

        //cash images
        [SerializeField]
        private Image cashFrequencyImage = null, cashAdImage = null, cashRangeImage = null;

        //grenade images
        [SerializeField]
        private Image grenadeRangeImage = null, grenadeDamageImage = null;

        //mine images
        [SerializeField]
        private Image mineRangeImage = null, mineDamageImage = null;
        

        private void Start()
        {
            if (StoreSTDSGameData.instance != null)
            {
                STDSdata sd = StoreSTDSGameData.instance.stdsData;
                Fill(ref liveAmountImage, sd.liveLevel);
                Fill(ref liveHealImage, sd.liveRegenerationLevel);
                Fill(ref liveArmor, sd.armorLevel);

                Fill(ref cashFrequencyImage, sd.goldFrequencyLevel);
                Fill(ref cashAdImage, sd.goldCoinAddLevel);
                Fill(ref cashRangeImage, sd.goldTakeRangeLevel);

                Fill(ref grenadeDamageImage, sd.grenadeDamageLevel);
                Fill(ref grenadeRangeImage, sd.grenadeRangeLevel);

                Fill(ref mineRangeImage, sd.mineRangeLevel);
                Fill(ref mineDamageImage, sd.mineDamageLevel);
            }

            Destroy(this);         
        }


        private void Fill(ref Image img, float scale)
        {
            img.color = new Color(1 - scale / 10, 1 - scale / 10, 1 - scale / 10);
        }
    }
}
