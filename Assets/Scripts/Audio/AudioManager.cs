﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager instance = null;

        public Transform parentForAudiosource;
        public AudioSource audioSourcePrefab;
        public AudioSource audioSource2DPrefab;
        public int howManyAudiosources = 100;
        public int howMany2DAudiosources = 10;

       // AudioSource audiosource2D;
        List<AudioSource> audiosource;
        List<AudioSource> aduiosource2DList;
        //public AudioSource[] audiosource3d;
        int lastIndex = 0;
        int lastIndex2D = 0;
       // int endIndex = 0;
        

        void Start()
        {
            if (instance == null)
            {
                instance = this;
                audiosource = new List<AudioSource>();
                aduiosource2DList = new List<AudioSource>();
                // audiosource2D = GetComponent<AudioSource>();
                // endIndex = audiosource3d.Length;

                for (int i = 0; i < howManyAudiosources; i++)
                {
                    AudioSource aud = Instantiate(audioSourcePrefab, parentForAudiosource);
                    audiosource.Add(aud);
                }
                for (int i = 0; i < howMany2DAudiosources; i++)
                {
                    AudioSource aud = Instantiate(audioSource2DPrefab, parentForAudiosource);
                    aduiosource2DList.Add(aud);
                }
                DontDestroyOnLoad(parentForAudiosource);
            }
            else
                Destroy(gameObject);



            // Debug.Log("Stworzono " + audiosource.Count);
        }


        public void Play3DAudio(AudioClip what, Vector3 place, float volume = 1, float pitch = 1)
        {
            audiosource[lastIndex].transform.position = place;
            audiosource[lastIndex].volume = volume;
            audiosource[lastIndex].pitch = pitch;
            //audiosource[lastIndex].PlayOneShot(what);
            audiosource[lastIndex].clip = what;
            audiosource[lastIndex].Play();
            lastIndex++;
            if (lastIndex >= howManyAudiosources)
                lastIndex = 0;
        }


        //public void Play3DAudio(AudioClip what, Vector3 place, float volume = 1, float pitch = 1)
        //{
        //    Debug.Log(what.name);
        //    Debug.Log(audiosource3d[lastIndex].name);
        //    Debug.Log(place);
        //    audiosource3d[lastIndex].transform.position = place;
        //    audiosource3d[lastIndex].volume = volume;
        //    audiosource3d[lastIndex].pitch = pitch;
        //    //audiosource[lastIndex].PlayOneShot(what);
        //    audiosource3d[lastIndex].clip = what;
        //    audiosource3d[lastIndex].Play();
        //    lastIndex++;
        //    if (lastIndex >= endIndex)
        //        lastIndex = 0;
        //}


        public void Play2DAudio(AudioClip what, float volume = 1, float pitch =1)
        {
            aduiosource2DList[lastIndex2D].volume = volume;
            aduiosource2DList[lastIndex2D].pitch = pitch;
            aduiosource2DList[lastIndex2D].clip = what;

            aduiosource2DList[lastIndex2D].Play();
            lastIndex2D++;
            if (lastIndex2D >= howMany2DAudiosources)
                lastIndex2D = 0;
        }
    }
}
