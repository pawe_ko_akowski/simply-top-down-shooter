﻿using UnityEngine;

namespace STDS
{
    public class ParticleSpllatterManager : MonoBehaviour
    {

        public static ParticleSpllatterManager instance = null;

        [SerializeField]
        private ParticleSystem[] particleSystems = null;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        public ParticleSystem GetParticleSystemSplater(int index)
        {
            return particleSystems[index];
        }
    }
}
