﻿using UnityEngine;

namespace STDS
{
    public class PlayerTransformManager : MonoBehaviour
    {
        public static PlayerTransformManager instance = null;

        [SerializeField]
        string playerName = "Player";

        Transform playerTransform;
        Rigidbody2D playerRig;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            playerTransform = GameObject.Find(playerName).transform;
            playerRig = playerTransform.GetComponentInChildren<Rigidbody2D>(true);
        }


        public Transform PlayerTransform
        {
            get
            {
                return playerTransform;
            }
        }


        public Vector2 GetPlayerPosition
        {
            get
            {
                return playerRig.position;
            }
        }
    }
}
