﻿using UnityEngine;

namespace STDS
{
    public class BulletParticleSystemManager : MonoBehaviour
    {
        public static BulletParticleSystemManager instance = null;

        [SerializeField]
        ParticleSystem[] particleSystems = null;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        public ParticleSystem GetParticleSystem(int numb)
        {
            return particleSystems[numb];
        }
    }
}
