﻿using UnityEngine;

namespace STDS
{
    public class EnemyAimBullet : EnemyBullet
    {
        [SerializeField]
        float aimingSpeed = 10f;

        PlayerTransformManager playerTrMan = null;


        private void FixedUpdate()
        {
            DestroyBullet();
            rig2D.AddForce((playerTrMan.GetPlayerPosition - rig2D.position).normalized * aimingSpeed);
            rig2D.rotation = Mathf.Atan2(rig2D.velocity.y, rig2D.velocity.x) * Mathf.Rad2Deg + 90f;
        }


        protected override void BulletInit()
        {
            base.BulletInit();
            playerTrMan = FindObjectOfType<PlayerTransformManager>();
        }
    }  
}
