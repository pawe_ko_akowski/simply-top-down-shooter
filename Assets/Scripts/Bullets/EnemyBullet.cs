﻿using UnityEngine;

namespace STDS
{
    public class EnemyBullet : Bullet
    {
        PlayerLiveManager plMan;
        AllyHouseHelthManager allyHouseMan = null;
        

        private void OnDisable()
        {
            liveTime = 0;
            toDestroy = false;
        }


        private void OnTriggerEnter2D(Collider2D other)
        {
            bullet.FillStdsBullet(other.GetInstanceID());

            if (plMan.ThisPlayer(other))
            {
                plMan.DamagePlayer(damage);
            }
            else
            {
                if (allyHouseMan != null)
                    allyHouseMan.DamageHouse(other);
            }
            gameObject.SetActive(false);
        }


        protected override void BulletInit()
        {
            plMan = PlayerLiveManager.instance;

            if (AllyHouseHelthManager.instance != null)
                allyHouseMan = AllyHouseHelthManager.instance.GetInstance;
        }
    }
}
