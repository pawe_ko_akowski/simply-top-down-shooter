﻿using System.Collections;
using UnityEngine;

namespace STDS
{
    public class PlayerGrenadeAttack : MonoBehaviour, BoomSetup
    {
       // [SerializeField]
        protected int damage = 10;
       // [SerializeField]
        protected float range = 1f;
        //[SerializeField]
        float waitToBoom = 1f;
        [SerializeField]
        LayerMask layerToDestroy =1;
        [SerializeField]
        protected float bumSize;

        protected float damageScale;
        protected float squareRange;
        Collider2D[] hits;
        WaitForSeconds wait;
        float timer;

        private DamagerSetter damSetter;



        private void OnEnable()
        {
            timer = Time.time + waitToBoom;
            //StartCoroutine(Boom());
        }


        private void FixedUpdate()
        {
            if (Time.time >= timer)
            {
                BoomAction();
            }
        }



        private void BoomAction()
        {
            hits = Physics2D.OverlapCircleAll(transform.position, range, layerToDestroy);
            for (int i = 0; i < hits.Length; i++)
            {

                if (!hits[i].isTrigger)
                {
                    //damageScale = Vector2.SqrMagnitude(transform.position - hits[i].transform.position);
                    //damageScale = 1 - (damageScale / squareRange);
                    //if (damageScale > 1)
                    //    damageScale = 1;
                    //EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), (int)(damage * damageScale));

                    damageScale = Vector2.SqrMagnitude(transform.position - hits[i].transform.position);
                    EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), damSetter.DamageChose(damageScale));
                }
            }
            ExplosionManager.instance.Explode(transform.position, bumSize);
            gameObject.SetActive(false);
        }


        //private IEnumerator Boom()
        //{
        //    yield return wait;
        //    hits = Physics2D.OverlapCircleAll(transform.position, range, layerToDestroy);
        //    for (int i = 0; i < hits.Length; i++)
        //    {
        //        damageScale = Vector2.SqrMagnitude(transform.position - hits[i].transform.position);
        //        damageScale = 1 - (damageScale / squareRange);
        //        if (damageScale > 1)
        //            damageScale = 1;
        //        EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), (int)(damage * damageScale));
        //    }
        //    ExplosionManager.instance.Explode(transform.position, bumSize);
        //    gameObject.SetActive(false);
        //}

        public void SetupBoomSize(int damagee, float rangee)
        {
            damage = damagee;
            range = rangee;
          //  squareRange = rangee * rangee;
            bumSize = rangee*0.5f;
            //Debug.Log( "Granaty "+range);
            if (StoreSTDSGameData.instance != null)
                waitToBoom = StoreSTDSGameData.instance.stdsData.grenadeBoomTime * 0.5f;
            //wait = new WaitForSeconds(waitToBoom);

            damSetter = new DamagerSetter(damagee, rangee*rangee);
            

           // damSetter.SetupDamage(damagee, rangee);
        }
    }
}
