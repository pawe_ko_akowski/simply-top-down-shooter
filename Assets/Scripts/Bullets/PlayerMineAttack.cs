﻿using System.Collections;
using UnityEngine;


namespace STDS
{

    public interface BoomSetup
    {
        void SetupBoomSize(int damage,float range );
    }

    public class PlayerMineAttack : MonoBehaviour, BoomSetup
    {
      //  [SerializeField]
        protected int damage = 10;
       // [SerializeField]
        protected float range = 1f;
       // [SerializeField]
        protected float bumSize;
        [SerializeField]
        LayerMask layerToDestroy = 0;
       // [SerializeField]
        float waitToBoom = 1f;

        protected AllyHouseHelthManager allyHouseMan = null;
        protected Collider2D col;
        protected float damageScale;
        protected float squareRange;
        protected bool boomHere = false;
        protected float boomTimer = 0;
        protected AudioSource audiosource;
        WaitForSeconds wait;
        Collider2D[] hits;
        bool boom = true;
        float timer;

        private DamagerSetter damSetter;

        //private void Start()
        //{

        //}

        private void OnEnable()
        {
            boom = false;
        }

        //private IEnumerator Boom()
        //{
        //    yield return wait;
        //    hits = Physics2D.OverlapCircleAll(transform.position, range, layerToDestroy);
        //    for (int i = 0; i < hits.Length; i++)
        //    {
        //        damageScale = Vector2.SqrMagnitude(transform.position - hits[i].transform.position);
        //        //damageScale = 1 - (damageScale / squareRange);
        //        //if (damageScale > 1)
        //        //    damageScale = 1;
        //        //EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), (int)(damage * damageScale));
        //        EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), damSetter.DamageChose(damageScale));
        //    }
        //    ExplosionManager.instance.Explode(transform.position, bumSize);
        //    gameObject.SetActive(false);          
        //}



        private void BoomAction()
        {
        
            hits = Physics2D.OverlapCircleAll(transform.position, range, layerToDestroy);
            //Debug.Log(hits.Length);
            for (int i = 0; i < hits.Length; i++)
            {
                if (!hits[i].isTrigger)
                {
                    damageScale = Vector2.SqrMagnitude(transform.position - hits[i].transform.position);
                    //damageScale = 1 - (damageScale / squareRange);
                    //if (damageScale > 1)
                    //    damageScale = 1;
                    //EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), (int)(damage * damageScale));
                    EnemyManager.instance.DamageInstanceId(hits[i].transform.GetInstanceID(), damSetter.DamageChose(damageScale));
                }
            }
            ExplosionManager.instance.Explode(transform.position, bumSize);
            gameObject.SetActive(false);
        }


        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.isTrigger &&!boom)
            {
                boom = true;
                timer = Time.time + waitToBoom;
                //StartCoroutine(Boom());
            }
        }

        private void FixedUpdate()
        {
            if (boom)
            {
                if (Time.time >= timer)
                {
                    BoomAction();
                    boom = false;
                }
            }
        }

        public void SetupBoomSize(int damagee, float rangee)
        {
            damage = damagee;
            range = rangee;
            squareRange = rangee * rangee;
            bumSize = range*0.5f;
           // Debug.Log("Miny " + range);
            if (StoreSTDSGameData.instance != null)
                waitToBoom = StoreSTDSGameData.instance.stdsData.mineBoomTime * 0.5f;
           // Debug.Log(waitToBoom);
           /// wait = new WaitForSeconds(waitToBoom);
            damSetter = new DamagerSetter(damagee, squareRange);

            //damSetter.SetupDamage(damage, range);
           // throw new System.NotImplementedException();
        }
    }
}
