﻿using UnityEngine;

namespace STDS
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        float speed = 2f;
        [SerializeField]
        AudioSource audioSource = null;
        [SerializeField]
        AudioClip wallImpactClip = null;
        [SerializeField]
        float maxBulletLive = 10f;
        [SerializeField]
        protected Rigidbody2D rig2D;
        //[SerializeField]
        protected int damage = 1;

        protected StdsBullet bullet;


        SpriteRenderer spritRend;
        Collider2D col2D;
        protected float liveTime = 0;
        protected bool toDestroy = false;
        float liveTimeAfterHit = 1f;


        void Awake()
        {
            col2D = GetComponent<Collider2D>();
            spritRend = GetComponent<SpriteRenderer>();
            bullet = new StdsBullet(gameObject.GetInstanceID(), damage);
            BulletInit();

            //  Debug.Log(AllyHouseHelthManager.instance);
        }

        protected virtual void BulletInit()
        {

        }


        public void SetuDamage(int amount)
        {
            damage = amount;
            bullet.bulletDamage = damage;


            //bullet.bulletDamage = amount;
        }


        public void AddDamage(int add)
        {
            damage += add;
        }


        void OnEnable()
        {
            rig2D.velocity = transform.up * speed;
            liveTime = 0;
            spritRend.enabled = true;
            col2D.enabled = true;
            toDestroy = false;
        }


        protected void DestroyBullet()
        {
            liveTime += Time.fixedDeltaTime;
            if (toDestroy)
            {
                //  Debug.Log(
                if (liveTime > liveTimeAfterHit)
                    gameObject.SetActive(false);
            }
            else
            {
                if (liveTime > maxBulletLive)
                    gameObject.SetActive(false);
            }
        }


        private void FixedUpdate()
        {
            DestroyBullet();
        }


        void OnTriggerEnter2D(Collider2D other)
        {


            bullet.FillStdsBullet(other.transform.GetInstanceID());

            if (!EnemyManager.instance.DamageInstanceId(bullet))
            {
                if (AllyHouseHelthManager.instance != null)
                    if (AllyHouseHelthManager.instance.DamageHouse(other))
                    {
                        Debug.Log("Ranie");
                        //  Do nothing!!
                    }
                    else
                        audioSource.PlayOneShot(wallImpactClip);
                else
                    audioSource.PlayOneShot(wallImpactClip);
            }
            else
            {
                //gameObject.SetActive(false);
            }

            rig2D.velocity = Vector2.zero;
            spritRend.enabled = false;
            col2D.enabled = false;
            toDestroy = true;
            liveTime = 0;
        }

        //old stuf - maybe i come back some time
        //void OnCollisionEnter2D(Collision2D coll)
        //{
        //    if (!EnemyManager.instance.DamageInstanceId(coll.transform.GetInstanceID(), damage))
        //    {
        //        spritRend.enabled = false;
        //        col2D.enabled = false;
        //        toDestroy = true;
        //        liveTime = 0;

        //        if (AllyHouseHelthManager.instance != null)
        //            if (AllyHouseHelthManager.instance.DamageHouse(coll.collider))
        //            {
        //                //  Debug.Log("Dupa");
        //            }

        //    }
        //    gameObject.SetActive(false);
        //}
    }
}