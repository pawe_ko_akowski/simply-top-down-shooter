﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/PlayerXPAndCoinMultiplier")]
    public class XPAndCoinMultiplierStats : MyScriptableObiect
    {
        public int coinMultiplier = 0;
        public int xpMultiplier = 0;
        public int XPAndCointMultiplierActiveFights = 0;
        public int XPAndCoinMaxActiveFights = 3;
        public int defaultMultipier = 1;
        public int XPAndCoinMultiplyBase = 2;


        void AddFight()
        {
            if(XPAndCointMultiplierActiveFights < XPAndCoinMaxActiveFights)
            {
                XPAndCointMultiplierActiveFights++;
            }
        }


        //public void RemoveFightTwo()
        //{
        //    XPAndCointMultiplierActiveFights--;

        //    XPAndCointMultiplierActiveFights = Mathf.Max(XPAndCointMultiplierActiveFights, 0);
        //}

       public void RemoweFight()
        {
            XPAndCointMultiplierActiveFights--;

            XPAndCointMultiplierActiveFights = Mathf.Max(XPAndCointMultiplierActiveFights, 0);
            // Debug.LogError("Pupa " + XPAndCointMultiplierActiveFights);

            if (storedData != null)
            {
                if (XPAndCointMultiplierActiveFights == 0)
                {
                    //   Debug.LogError("dupa");
                    // XPAndCoinMaxActiveFights = 0;
                    xpMultiplier = 0;
                    coinMultiplier = 0;
                    storedData.stdsData.xpMultiplier = xpMultiplier;
                    storedData.stdsData.coinMultiplier = coinMultiplier;
                }


                storedData.stdsData.multiplierActivity = XPAndCoinMaxActiveFights;
                SaveJson();
            }
        }


        public bool CanProgresCoinMultiplier()
        {
            if (coinMultiplier == MaxMultipier())
                return false;
            return true;
        }
    

        public bool CanProgresXPMultiplier()
        {
            if (xpMultiplier == MaxMultipier())
                return false;
            return true;
        }


        //return false when multiplier is max
        public bool AddCoinMultiplier()
        {
            coinMultiplier += XPAndCoinMultiplyBase;
            AddFight();   
            if (coinMultiplier < MaxMultipier())
                return true;
            return false;
        }

        public bool AddCoinMultiplierTwo()
        {
            coinMultiplier = 1;
            XPAndCointMultiplierActiveFights = XPAndCoinMaxActiveFights;
            return true;
        }

        //return false when multiplier is max
        public bool AddXpMultiplier()
        {
            xpMultiplier += XPAndCoinMultiplyBase;
            AddFight();
            if (xpMultiplier < MaxMultipier())
                return true;
            return false;
        }

        public bool AddXpMultiplierTwo()
        {
            xpMultiplier = 1;
            XPAndCointMultiplierActiveFights = XPAndCoinMaxActiveFights;
            return true;
        }

        public int MaxMultipier()
        {
            return XPAndCoinMaxActiveFights * XPAndCoinMultiplyBase;
        }


        public int GetCoinMultiplier
        {
            get
            {
               // if (coinMultiplier > 0)
                    return coinMultiplier;
              //  else
                 //   return defaultMultipier;
            }
        }


        public int GetXPMultiplier
        {
            get
            {
               // if (xpMultiplier > 0)
                    return xpMultiplier;
               // else
                  //  return defaultMultipier;
            }
        }
    }
}
