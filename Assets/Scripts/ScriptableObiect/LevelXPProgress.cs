﻿using UnityEngine;


namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/PlayerLevelXPProgress")]
    public class LevelXPProgress : ScriptableObject
    {
       // [SerializeField]
     //   int firstLevelXP = 0;
        [SerializeField]
        int levelMultiplierXP = 2;
       // [SerializeField]
        //int levelP



        public int CalculateXpFor(int level)
        {
            int help = 0;// = firstLevelXP;
            help = level * levelMultiplierXP * (2+(level));
          //  for (int i = 0; i <= level; i++)
             //   help += i * levelMultiplierXP;
               // help *= levelMultiplierXP;

            return help;
        }
    }
}
