﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/LanguageSetup")]
    public class TextLanguageSetup : ScriptableObject
    {
        //main menu
        public string PlayBtnName = "Play";
        public string ProgresBtnName = "Progress";
        public string OptionsBtnName = "Options";
        public string QuitBtnName = "Exit";
        public string MoreBtnName = "More";

        public string AllBackBtnName = "Back";

        //play menu
        public string PlayMenuLabelName = "Play game";
        public string CampigniBtnName = "Campign";
        public string EndlessBtnName = "Endless";

        //campign all scenerio
        public string HeroBornCampignTitleName = "Hero born";
        public string ProgressLabelName = "Progress";
        public string ProgressCampignTimerName = "Campign time:";
        public string NextBtnName = "Next", PrevBtnName = "Prev";

        //campign one map chose
        public string mapTaskLabelName = "Obiectives";
        public string PlayCampignsBtnName = "Play";

        //endless play menu
        public string EndlessLabelName = "Endless play";
        public string FirstMapBtnName = "First map";
        public string SecontMapBtnName = "Second map";

        //progress panel
        public string ProgressPanelLabelName = "Upgrade panel";
        public string SkillUpBtnName = "Skill up";
        public string GranadeAndMineShoopPanelBtnName = "Boom shoop";
        public string PremiumGoldPanelBtnName = "Gold shoop";
        public string AdPanelBtnName = "Ad bonus";

        //skill panel
        public string SkillPanelLabelName = "Skills";
        public string CashLabelName = "Cash";
        public string SkillPointsLabelName = "Skill points";
        public string BuySkillName = "Buy";
        public string MaxLevelLabelName = "Max";
        public string LiveSkillLabelName = "Live";
        public string LiveRegenerationSkillLabelName = "Live regeneration";
        public string BulletDamageSkillLabelName = "Bullet damage";
        public string BulletRateSkillLabelName = "Bullet rate";
        public string MovementSpeedSkillLabelName = "Movement speed";
        public string ArmorSkillLabelName = "Armor";
        public string GrenadeDamageSkillLabelName = "Grenade damage";
        public string GrenadeRangeSkillLabelName = "Granade range";
        public string MineDamageSkillLabelName = "Mine damage";
        public string MineRangeSkillLabelName = "Mine range";
        public string GoldTakeRangeSkillLabelName = "Gold take range";
        public string GoldAddSkillLabelName = "Gold add";
        public string GoldFrequencyAddSkillLabelName = "Gold frequency add";

        //grenade and mine store
        public string BoomShoopLabelName = "Boom shoop";
        public string BuyGrenadeBtnName = "Buy grenade";
        public string BuyMineBtnName = "Buy mine";

        //gold for real cash shoop
        public string GoldPanelShoopLabelName = "Gold shoop";

        //ad bonus revenue panel
        public string AdBonusLabelName = "Ad bonus";
        public string AdBonusDescriptionLabelName = "Watch and multiply your revenue from game";
        public string AdBonusActiveForLabelName = "Bonus active for:";
        public string DoubleGoldBtnName = "Double GOLD";
        public string DoubleXPBtnName = "Double XP";

        //options panel
        public string OptionsLabelName = "Options";
        public string LanguageLabelName = "Language";
        public string SoundVolumeLabelName = "Sound volume";
        public string MusicVolumeLabelName = "Music volume";
        public string PlayerNameLabelName = "Player name";
        public string AutioaimLablelName = "Autoaim";
        public string ActiveName = "Active";
        public string DisactiveName = "Disactive";
        public string GrenadeTimerName = "Grenade timer";
        public string MineTimerName = "Mine timer";
        public string ControlLabelName = "Control";
        public string ControlMoveLabelName = "Move";
        public string ControlAimLabelName = "Aim";
        public string ControlGrenadeDropLabelName = "Grenade drop";
        public string ControlMineDropLableName = "Mine drop";
        public string ControlNoneLabelName = "None";
        public string SmothLabelName = "Monster smooth";
        public string FasterLabelName = "Faster";

        //more panel
        public string MorePanelLabelName = "More";
        public string DonateBtnName = "Donate";

        //features panel
        public string featuresBtnName = "Features";
        public string featuresLabelName = "Features panel";
        public string firstFeatureName = "honesty";
        public string secondFeatureName = "patience";
        public string thirdFeatureName = "perseverance";
        public string fourthfFeatureName = "responsibility";
        public string fifthFeatureName = "openness";
        public string sixFeatureName = "prudence";
        public string sevenFeatureName = "diligence";
        public string eightFeatureName = "kindness";
        public string nineFeatureName = "cordiality";
        public string tenFeatureName = "gentleness";
        public string loadingName = "Loading...";

        //endless btn requires 
        public string endlessBtnRequireLabel = "requires completion of level 5 of the campaign";
        public string mysteryEndlesBtnRequireLable = "find the missing items";

    }
}
