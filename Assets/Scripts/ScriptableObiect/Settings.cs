﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/Settings")]
    public class Settings : MyScriptableObiect
    {

        public LANGUAGE lang = LANGUAGE.PL;

        //max = 1 for sound volumes
        public float soundVolume = 1f;
        public float musicVolume = 1f;
        public string playerName = "Player";

    }
}
