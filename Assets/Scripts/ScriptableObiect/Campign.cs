﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/Campign")]
    public class Campign : MyScriptableObiect
    {
        [SerializeField]
        int currentActiveCampignScene = 0;

        [SerializeField]
        int maxCampignScene = 10;


        public void ChangeCurrentActiveCampign(int numb)
        {
            if (numb > currentActiveCampignScene)
            {
                currentActiveCampignScene = numb;
                if(storedData != null)
                storedData.stdsData.CampignLevel = numb;
                SaveJson();
            }
        }

        public void NextMission()
        {
            currentActiveCampignScene++;
            currentActiveCampignScene = Mathf.Min(currentActiveCampignScene, maxCampignScene);
            storedData.stdsData.CampignLevel = currentActiveCampignScene;
            SaveJson();
        }

        public int CurrentActiveScene
        {
            get
            {
                return currentActiveCampignScene;
            }
            //set
            //{
            //    currentActiveCampignScene = value;
            //}
        }

        public int MaxCampignScene
        {
            get
            {
                return maxCampignScene;
            }
        }
    }
}
