﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/PlayerStats")]
    public class PlayerStats : MyScriptableObiect
    {
        [SerializeField]
        LevelXPProgress levelXPProgress = null;

        public string Name;
        public int coins;
        public int xp;
        public int level;
        public int lastLevelXP = 0;
        public int nextLevelXP;
       // public int skillPoints;
        public int maxLevl = 100;

        bool lastLevelSet = false;
        int lastLevel = 0;
        int lastCash = -1;
        //int lastXP = 0;

        public bool LevelWasSet()
        {
            bool ret = lastLevelSet;
            lastLevelSet = false;
            //Debug.Log(ret);
            return ret;
        }

        public int GetLastLevel()
        {
            lastLevelSet = false;
            return ThrowAndResetValue(ref lastLevel);
        }

        public int GetLastXP()
        {
            return ThrowAndResetValue(ref lastLevelXP);
        }

        public int GetLastCash()
        {
            return ThrowAndResetValue(ref lastCash);
        }

        private int ThrowAndResetValue(ref int value, int reset = -1)
        {
            int temp = value;
            value = reset;
            return temp;
        }

        public bool CanLevelUp()
        {
            if (level < maxLevl)
                return true;
            return false;
        }


        public int GetLastLevelXP
        {
            get
            {
                return levelXPProgress.CalculateXpFor(level);
            }
        }


        public int GetNextLevelXP
        {
            get
            {
               // Debug.Log(levelXPProgress.CalculateXpFor(1));
               // Debug.Log(levelXPProgress.CalculateXpFor(2));
               // Debug.Log(levelXPProgress.CalculateXpFor(3));
               // Debug.Log(levelXPProgress.CalculateXpFor(4));

                return levelXPProgress.CalculateXpFor(level + 1);
            }
        }

        public int GetXPForLevel(int lev)
        {
            return levelXPProgress.CalculateXpFor(lev);
        }

        public void AddCoins(int numb)
        {
            //coins += numb;
            if (storedData != null)
            {
                if(lastCash ==-1)
                    lastCash = storedData.stdsData.coins;
                storedData.stdsData.coins += numb;
                SaveJson();
            }
        }

        //public void UseSkilPoint()
        //{
        //    skillPoints--;
        //    skillPoints = Mathf.Max(skillPoints, 0);
        //    storedData.stdsData.skillPoints = skillPoints;
        //    SaveJson();
        //}

        public void ChangeName(string newName)
        {
            Name = newName;
            storedData.stdsData.playerName = Name;
            SaveJson();
        }


        public void AdXP(int exp)
        {
            if (level < maxLevl && exp >0)
            {
                //dupa tu może byc zle
                if (!lastLevelSet)
                {
                    lastLevelSet = true;
                    if (storedData != null)
                    {
                        lastLevel = storedData.stdsData.level;
                        lastLevelXP = storedData.stdsData.xp;
                    }
                   // skillPoints = storedData.stdsData.skillPoints;
                    //lastCash = storedData.stdsData.coins;
                    
                }

                xp += exp;
                if (storedData != null)
                {
                    storedData.stdsData.xp += exp;
                    if (storedData.stdsData.xp > GetXPForLevel(100))
                        storedData.stdsData.xp = GetXPForLevel(100);

                }
               // nextLevelXP = GetNextLevelXP;
              //  if (nextLevelXP == 0)
               // {
                    //lastLevelXP = 0;
                    //nextLevelXP = levelXPProgress.CalculateXpFor(0);
                    //storedData.stdsData.nextLevelUpXP = nextLevelXP;
               // }

                while (GetNextLevelXP <= xp && level<maxLevl)
                {
                    LevelUP();
                }


                SaveJson();
               // Debug.Log("last level = " + lastLevel);
            }
        }


        void LevelUP()
        {
            //if (!lastLevelSet)
            // {
            //lastLevel = level;
            // lastLevelSet = true;
            //  }

                level++;
                //skillPoints++;
                if (storedData != null)
                {

                    storedData.stdsData.level++;
                    storedData.stdsData.skillPoints++;
                }
            
           // nextLevelXP = GetNextLevelXP;
            // lastLevelXP = nextLevelXP;
            //nextLevelXP = levelXPProgress.CalculateXpFor(level);
            //storedData.stdsData.nextLevelUpXP = nextLevelXP;
        }
    }
}
