﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/InGameLanguageSetup")]
    public class InGameLanguageSetup : ScriptableObject
    {
        public string InfoTextStartGameName = "First wave comming";
        public string InfoTextNextWaveName = "Wave end. Next comming";
        public string InfoTextEndGameName = "Fight end";

        public string SuccessTextName = "Mission succed";
        public string FailureTextName = "Mission failed";

        public string ResumeGameTextName = "Resume";
        public string EndGameTextName = "End match";
        public string PlayAgainTextName = "Rematch";
        public string ToMainMenuTextName = "To main menu";

        public string ArenaRewardTextName = "Arena reward";
        public string GoldRewardTextName = "Gold";
        public string XPRewardTextName = "XP";
    }
}
