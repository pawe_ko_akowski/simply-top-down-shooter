﻿using UnityEngine;
using System;

namespace STDS
{
    [Serializable]
    public class LevelEvolution
    {
        public int damageAdd;
        public float speedAdd;
        public int liveAdd;
        public int armorAdd;
        public float timeToProgress;
    }


    [CreateAssetMenu(menuName = "STDS/EnemyEvolution")]
    public class EnemyEvolution : ScriptableObject
    {
        [SerializeField]
        LevelEvolution[] levelEvolution = null;


        public LevelEvolution GetEvolutionFromLevel(int level)
        {
            return levelEvolution[level - 1];
        }


        public bool CanIProgress(int level)
        {
            if (level > levelEvolution.Length)
                return false;
            else
                return true;
        }
    }
}
