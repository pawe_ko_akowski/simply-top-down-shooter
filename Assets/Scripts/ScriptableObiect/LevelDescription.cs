﻿using UnityEngine;
using System;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/LevelDescription")]
    public class LevelDescription : ScriptableObject
    {
        [Serializable]
        public class Description
        {
            public string languageVersionInfo;
            public string missionName;
            [TextArea(0, 25)]
            public string description;
            [TextArea(1, 8)]
            public string missionObiectives;
            public AudioClip missionAudioRecord;

        }

        public Sprite mapSprite;
       /* public string[] missionName ;
        [TextArea(0, 10)]
        public string[] description ;
        [TextArea(1, 4)]
        public string[] missionObiectives;
        public AudioClip[] missionAudioRecord;
        */
        //[SerializeField]
        public Description[] descriptions;
    }
}
