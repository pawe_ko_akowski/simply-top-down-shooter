﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "STDS/EquipmentStats")]
    public class EquipmentStats : MyScriptableObiect
    {
        //public int mines = 0;
        //public int grenades = 0;
        private int minesMAX = 5;
        private int grenadesMAX = 5;
        public int costGrowth = 1000;


        public int MaxGrenadePrice()
        {
            grenadesMAX = storedData.stdsData.MaxGrenades;
            return grenadesMAX * costGrowth;
        }


        public int MaxMinePrice()
        {
            minesMAX = storedData.stdsData.MaxMines;
            return minesMAX * costGrowth;
        }


        public void UseGrenade()
        {
            //grenades--;
           // grenades = Mathf.Max(grenades, 0);
            if (storedData != null)
                storedData.stdsData.grenades -=1;
            //SaveJson();- this will be done in destroy - no lag
        }


        public void UseMine()
        {
            //mines--;
            //mines = Mathf.Max(mines, 0);
            if (storedData != null)
            storedData.stdsData.mines -=1;

            //SaveJson(); - this will be done in destroy - no lag
        }


        public int NextGrenadeCost()
        {
            return NextCost(storedData.stdsData.grenades);
        }


        public int NextMineCost()
        {
            return NextCost(storedData.stdsData.mines);
        }


        int NextCost(int what)
        {
            return costGrowth + what * costGrowth;
        }


        public bool CanIBuyGrenade(int cash)
        {
            MaxGrenadePrice();
            return CanIBuy(cash, storedData.stdsData.grenades, grenadesMAX);
        }


        public bool CanIBuyMine(int cash)
        {
            MaxMinePrice();
            return CanIBuy(cash, storedData.stdsData.mines, minesMAX);
        }


        bool CanIBuy(int cash, int what, int whatMax)
        {
           // Debug.Log(cash + "  " + what + "   " + whatMax);

            if (cash >= NextCost(what) && what < whatMax)
                return true;
            return false;
        }


        public void BuyGrenade()
        {
            storedData.stdsData.coins -= NextCost(storedData.stdsData.grenades);
           // grenades++;
            storedData.stdsData.grenades +=1;
            SaveJson();
        }


        public int Grenades
        {
            get
            {
                if (storedData != null)
                    return storedData.stdsData.grenades;
                else
                    return 0;
            }
        }

        public void BuyMine()
        {
            storedData.stdsData.coins -= NextCost(storedData.stdsData.mines);
           // mines++;
            storedData.stdsData.mines +=1;
            SaveJson();
        }


        public int Mines
        {
            get
            {
                if (storedData != null)
                    return storedData.stdsData.mines;
                else
                    return 0;
            }
        }
    }
}
