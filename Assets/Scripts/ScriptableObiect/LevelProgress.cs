﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

namespace STDS
{
    [Serializable]
    public struct Skill
    {
        public string skilName;
        public int skillBase;
        public int skillLevel;
        public int skillProgress;
    }


    [Serializable]
    public struct SkillCost
    {
        public int cash;
        public int xpPoint;
    }


    public class MyScriptableObiect: ScriptableObject
    {
       protected StoreSTDSGameData storedData;

        public void SetStoreData(StoreSTDSGameData sd)
        {
            storedData = sd;
        }

        protected void SaveJson()
        {
            //dupa wyłączam zapisywanie
            if(SceneManager.GetActiveScene().buildIndex == 0)
                if(storedData != null)
            storedData.Save();
        }
    }


    [CreateAssetMenu(menuName = "STDS/LevelProgress")]
    public class LevelProgress : MyScriptableObiect
    {
        [SerializeField]
        PlayerStats playerStats = null;

        public SkillCost[] skillCostForXpNeeded;
        public SkillCost[] skillCostForXpDontNeded;
        public int maxSkillLevel = 10;
        //public Skill[] skills;
        public int[] levels;




        public PlayerStats GetPlayerStats
        {
            get
            {
                return playerStats;
            }
        }


        public bool MaxLevel(int level)
        {
            if (maxSkillLevel == level)
                return true;
            return false;
        }


        #region Live
        public int liveBase = 100;
        public int liveLevel = 0;
        public int liveProgress = 10;

        public bool LiveLevelUp()
        {
            if(LevleUpXpSkill(ref liveLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.liveLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetLiveLevel
        {
            get
            {
                return liveLevel;
            }
        }

        public int GetLiveStat()
        {
            return GetIntNormalStat(liveBase, liveLevel, liveProgress);
        }
        #endregion

        #region LiveRegeneration
        public int liveRegenPer10SekBase = 0;
        public int liveRegenLevel = 0;
        public int liveRegenProgress = 1;

        public bool LiveRegenerationLevelUp()
        {
            if (LevleUpXpSkill(ref liveRegenLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.liveRegenerationLevel++;
                SaveJson();
                return true; ;
            }
            return false;

            //return LevleUpXpSkill(ref liveRegenLevel);
        }

        public int GetLiveRegenerationLevel
        {
            get
            {
                return liveRegenLevel;
            }
        }

        public int GetLiveRegenerationStat()
        {
            return GetIntNormalStat(liveRegenPer10SekBase, liveRegenLevel, liveRegenProgress);
        }

        #endregion

        #region BulletDamage
        public int bulletDamageBase = 20;
        public int bulletDamageLevel = 0;
        public int bulletDamageProgress = 2;

        public bool BulletDamageLevelUp()
        {
            if (LevleUpXpSkill(ref bulletDamageLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.bulletDamageLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetBulletDamageLevel
        {
            get
            {
                return bulletDamageLevel;
            }
        }

        public int GetBulletDamageStat()
        {
            return GetIntNormalStat(bulletDamageBase, bulletDamageLevel, bulletDamageProgress);
        }
        #endregion

        #region BulletRate
        public float bulletRateBase = 0.2f;
        public int bulletRateLevel = 0;
        public float bulletRateDecrese = 0.01f;

        public bool BulletRateLevelUp()
        {
            if (LevleUpXpSkill(ref bulletRateLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.bulletRateLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetBulletRateLevel
        {
            get
            {
                return bulletRateLevel;
            }
        }

        public float GetBulletRateStat()
        {
            return GetFloatNormalStat(bulletRateBase, bulletRateLevel, bulletRateDecrese);
        }
        #endregion

        #region MovementSpeed
        public float movementBase = 1f;
        public int movementLevel = 0;
        public float movementSpeedProgress = 0.1f;

        public bool MovementSpeedLevelUp()
        {
            if (LevleUpXpSkill(ref movementLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.movementSpeedLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetMovementLevel
        {
            get
            {
                return movementLevel;
            }
        }

        public float GetMovementSpeedStat()
        {
            return GetFloatNormalStat(movementBase, movementLevel, movementSpeedProgress);
        }
        #endregion

        #region Armor
        public int armorBase = 0;
        public int armorLevel = 0;
        public int armorProgress = 1;

        public bool ArmorLevelUp()
        {
            if (LevleUpXpSkill(ref armorLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.armorLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetArmorLevel
        {
            get
            {
                return armorLevel;
            }
        }

        public int GetArmorStat()
        {
            return GetIntNormalStat(armorBase, armorLevel, armorProgress);
        }
        #endregion

        #region GrenadeDamage
        public int grenadeDamageBase = 100;
        public int grenadeDamageLevel = 0;
        public int grenadeDamageProgress = 100;

        public bool GrenadeLevelUp()
        {
            if (LevleUpXpSkill(ref grenadeDamageLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.grenadeDamageLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetGrenadeDamageStat()
        {
            return GetIntNormalStat(grenadeDamageBase, grenadeDamageLevel, grenadeDamageProgress);
        }
        #endregion

        #region GrenadeRange
        public float grenadeRangeBase = 1f;
        public int grenadeRangeLevel = 0;
        public float grenadeRangeProgress = 0.1f;

        public bool GrenadeRangeLevelUp()
        {
            if (LevleUpXpSkill(ref grenadeRangeLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.grenadeRangeLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public float GetGrenadeRangeStat()
        {
            return GetFloatNormalStat(grenadeRangeBase, grenadeRangeLevel, grenadeRangeProgress);
        }
        #endregion

        #region MineDamage
        public int mineeDamageBase = 200;
        public int mineDamageLevel = 0;
        public int mineDamageProgress = 200;
        public bool MineDamageLevelUp()
        {
            if (LevleUpXpSkill(ref mineDamageLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.mineDamageLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetMineDamageStat()
        {
            return GetIntNormalStat(mineeDamageBase, mineDamageLevel, mineDamageProgress);
        }
        #endregion

        #region MineRange
        public float mineRangeBase = 1f;
        public int mineRangeLevel = 0;
        public float mineRangeProgress = 0.1f;

        public bool MineRangeLevelUp()
        {
            if (LevleUpXpSkill(ref mineRangeLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.mineRangeLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public float GetMineRangeStat()
        {
            return GetFloatNormalStat(mineRangeBase, mineRangeLevel, mineRangeProgress);
        }
        #endregion

        #region GoldTakeRange
        public float goldRangeBase = 1f;
        public int goldRangeLevel = 0;
        public float goldRangeProgress = 0.2f;

        public bool GoldRangeLevelUp()
        {
            if (LevelUpNoXPSkill(ref goldRangeLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.goldTakeRangeLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public float GetGoldRangeStat()
        {
            return GetFloatNormalStat(goldRangeBase, goldRangeLevel, goldRangeProgress);
        }
        #endregion

        #region GoldCoinAmountAdd
        public int goldAddBase = 0;
        public int goldAddLevel = 0;
        public int goldAddProgress = 1;

        public bool GoldAddUp()
        {
            if (LevelUpNoXPSkill(ref goldAddLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.goldCoinAddLevel++;
                SaveJson();
                return true; ;
            }
            return false;
        }

        public int GetGoldAddStat()
        {
            return GetIntNormalStat(goldAddBase, goldAddLevel, goldAddProgress);
        }
        #endregion

        #region GoldFrequency
        public float goldFrequencyBase = 1f;
        public int goldFrequencyLevel = 0;
        public float goldFrequencyProgress = 0.2f;

        public bool GoldFrequencyLevelUp()
        {
            if (LevelUpNoXPSkill(ref goldFrequencyLevel))
            {
                //fillScriptableObiect.LiveLevelProgres();
                storedData.stdsData.goldFrequencyLevel++;
                
                SaveJson();
                return true; ;
            }
            return false;
        }

        public float GetGoldFrequencyStat()
        {
            return GetFloatNormalStat(goldFrequencyBase, goldFrequencyLevel, goldFrequencyProgress);
        }
        #endregion


        bool LevleUpXpSkill(ref int skill)
        {
            if (skill >= maxSkillLevel)
                return false;
            if (skillCostForXpNeeded[skill].cash <= storedData.stdsData.coins && skillCostForXpNeeded[skill].xpPoint <= storedData.stdsData.skillPoints)
            {
               // playerStats.coins -= skillCostForXpNeeded[skill].cash;
               // playerStats.skillPoints -= skillCostForXpNeeded[skill].xpPoint;
                storedData.stdsData.coins -= skillCostForXpNeeded[skill].cash;
                storedData.stdsData.skillPoints -= skillCostForXpNeeded[skill].xpPoint;
                skill++;
                return true;
            }

            return false;
        }


        bool LevelUpNoXPSkill(ref int skill)
        {
            if (skill >= maxSkillLevel)
                return false;
            if (skillCostForXpDontNeded[skill].cash <= storedData.stdsData.coins)// playerStats.coins)
            {
               // playerStats.coins -= skillCostForXpDontNeded[skill].cash;
                storedData.stdsData.coins -= skillCostForXpDontNeded[skill].cash;
                skill++;
                return true;
            }
            return false;
        }


        int GetIntNormalStat(int skilBase, int skillLevel, int skillProgress)
        {
            return skillLevel * skillProgress + skilBase;
        }


        float GetFloatNormalStat(float skilBase, int skillLevel, float skillProgress)
        {
            return skillLevel * skillProgress + skilBase;
        }
    }
}
