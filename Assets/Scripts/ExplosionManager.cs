﻿using UnityEngine;

namespace STDS
{
    public class ExplosionManager : MonoBehaviour
    {
        class Explosion
        {
            public Animator anim;
            public Transform tran;
            public BoomAudioPlay boomAudio;

            public Explosion(Animator an, Transform tr)
            {
                anim = an;
                tran = tr;
                boomAudio = tr.GetComponent<BoomAudioPlay>();
            }

            public Explosion(Animator an, Transform tr, BoomAudioPlay boomAu)
            {
                anim = an;
                tran = tr;
                boomAudio = boomAu;
            }
        }

        public static ExplosionManager instance = null;
            
        [SerializeField]
        GameObject explosionPrefab = null;
        [SerializeField]
        int explosionForWork = 8;
        [SerializeField]
        Transform explosionParent = null;

        Explosion[] explosion;
        int last = 0;
        Vector3 helpVec3 = new Vector3(1, 1, 1);


        private void Awake()
        {
            if (instance != null)
                Destroy(this);
            instance = this;
        }


        private void Start()
        {
           // Debug.Log("tworze");
            explosion = new Explosion[explosionForWork];

            for (int i = 0; i < explosionForWork; i++)
            {
               // Debug.Log("tworze2");
                GameObject gob = Instantiate(explosionPrefab, explosionParent);
                Animator anim = gob.GetComponent<Animator>();
                BoomAudioPlay ba = gob.GetComponent<BoomAudioPlay>();
                explosion[i] = new Explosion(anim, gob.transform, ba);
            }
        }


        public void Explode(Vector3 pos)
        {
            explosion[last].tran.position = pos;
            explosion[last].anim.Play("Explosion",-1, 0f);
            explosion[last].boomAudio.PlayAudiosource(1f);

            last++;
            if (last >= explosionForWork)
                last = 0;
        }


        public void Explode(Vector3 pos, float size)
        {
            explosion[last].tran.position = pos;
            helpVec3.x = size;
            helpVec3.y = size;
            explosion[last].tran.localScale = helpVec3;
            explosion[last].anim.Play("Explosion", -1, 0f);
            explosion[last].boomAudio.PlayAudiosource(size);

            last++;
            if (last >= explosionForWork)
                last = 0;
        }
    }
}
