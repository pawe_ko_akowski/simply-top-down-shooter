﻿using UnityEngine;

namespace STDS
{
    public class CoinPickableItem : PickableItem
    {
        [SerializeField]
        int coinCash = 1;


        protected override void DoSomething()
        {
            base.DoSomething();
            CanvasManager.instance.UpdateCoins(coinCash);
        }
    }
}
