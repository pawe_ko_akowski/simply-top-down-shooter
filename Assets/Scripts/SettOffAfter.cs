﻿using UnityEngine;

namespace STDS
{
    public class SettOffAfter : MonoBehaviour
    {
        [SerializeField]
        float timer = 5f;

        float tim = 0;


        private void OnEnable()
        {
            tim = timer;
        }


        private void Update()
        {
            tim -= Time.deltaTime;

            if (tim <= 0)
                gameObject.SetActive(false);
        }
    }
}
