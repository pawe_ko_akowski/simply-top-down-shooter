﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class CoinInstationerManager : MonoBehaviour
    {
        public static CoinInstationerManager instance = null;

        [SerializeField]
        GameObject[] coinTypes = null;
        [SerializeField]
        Transform parentObiectForCoins = null;
        [SerializeField]
        int defaultStocForAnyKindOfCoin = 20;

        List<int> lastIndexOfCoinType;
        List<Transform>[] coinTypesList;
        float coinsRangeAdd = 1;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;

            coinTypesList = new List<Transform>[coinTypes.Length];
            lastIndexOfCoinType = new List<int>();

            for (int i = 0; i < coinTypes.Length; i++)
            {
                lastIndexOfCoinType.Add(new int());
                lastIndexOfCoinType[i] = 0;
                coinTypesList[i] = new List<Transform>();


                for (int j = 0; j < defaultStocForAnyKindOfCoin; j++)
                {
                    Transform tr = Instantiate(coinTypes[i], parentObiectForCoins).transform;
                    tr.gameObject.SetActive(false);
                    tr.GetComponent<CircleCollider2D>().radius *= coinsRangeAdd;
                    coinTypesList[i].Add(tr);
                }
            }
        }


      //  public void SetupCoinsRange(float range)
        //{
         //   coinsRangeAdd = range;
       // }


        public void InstantiateCoin(Vector3 place, int coinType)
        {
            coinTypesList[coinType][lastIndexOfCoinType[coinType]].position = place;
            coinTypesList[coinType][lastIndexOfCoinType[coinType]].gameObject.SetActive(true);
            lastIndexOfCoinType[coinType]++;
            if (lastIndexOfCoinType[coinType] >= defaultStocForAnyKindOfCoin)
                lastIndexOfCoinType[coinType] = 0;
        }
    }
}
