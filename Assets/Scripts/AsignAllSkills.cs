﻿using UnityEngine;

namespace STDS
{
    public class AsignAllSkills : MonoBehaviour
    {
        //[SerializeField]
       // PlayerShoot[] playerShooty = null;
        [SerializeField]
        PlayerMove[] playerMove = null;
        [SerializeField]
        PlayerHealth playerHealth = null;
        [SerializeField]
        LevelProgress levelProgress = null;
        [SerializeField]
        AsignVisualEffects asignVisEffects = null;

       // [SerializeField]
       // CircleCollider2D cointTrigger = null;
        [SerializeField]
        Transform playerTransform = null;
        [SerializeField]
        float coinColliderMultiplier = 2f;

        [SerializeField]
        PlayerGrenadeAndMineDrop[] playerGrenadeAndMineDrop = null;

        [SerializeField]
        Shoot[] playerShoot = null;
        //PlayerMineAttack grenadeBoomSetup = null;
        //[SerializeField]
        //PlayerGrenadeAttack mineBoomSetup = null;


        private void Awake()
        {
            AssignAll();
            Destroy(this);
        }


        private void AssignAll()
        {
            //1
            AssignHealthAmount();
            //2+3
            AssignBulletDamageAndRate();
            //4
            AssignLiveRegeneration();
            //5
            AssignMovementSpedd();
            //6
            AssignArmor();
            //7+8
            AssignGrenadeDamageAndRange();
            //9+10
            AssignMineDamageAndRange();
            //11
            AssignGoldTakeRange();
            //12
            AssignGoldCoinAdd();
            //13
            AssignGoldFrequency();
        }


        private void AssignHealthAmount()
        {
            playerHealth.AssignHealthAmount(levelProgress.GetLiveStat());
            asignVisEffects.SetLiveAmount(levelProgress.GetLiveLevel);
        }


        private void AssignBulletDamageAndRate()
        {
            int damStat = levelProgress.GetBulletDamageStat();
            PlayerDamageManager.instance.SetPlayerDamage(damStat);
            int damStatLevel = 0;
            if (StoreSTDSGameData.instance != null)
                damStatLevel = StoreSTDSGameData.instance.stdsData.bulletDamageLevel;
            asignVisEffects.SetBulletPower(damStatLevel);


            float fireRate = levelProgress.GetBulletRateStat();
            for (int i = 0; i < playerShoot.Length; i++)
            {
                playerShoot[i].SetFireRate(fireRate);
            }

            asignVisEffects.SetBulletRate(levelProgress.GetBulletRateLevel);
           // for (int i = 0; i < playerShooty.Length; i++)
           //{
           //dupa
           //playerShooty[i].SetDamage(levelProgress.GetBulletDamageStat());
           // playerShooty[i].SetFireRate(levelProgress.GetBulletRateStat());
           //}
        }


        private void AssignLiveRegeneration()
        {
            int liveReg = levelProgress.GetLiveRegenerationStat();
            playerHealth.AssignLiveRegeneration(liveReg);
            int liveRegLevel = levelProgress.GetLiveRegenerationLevel;
            //asignVisEffects.SetRegenerationSound(liveRegLevel);
        }


        private void AssignMovementSpedd()
        {
            float speed = levelProgress.GetMovementSpeedStat();
            for (int i = 0; i < playerMove.Length; i++)
            {
                playerMove[i].SetMovementSpeed(speed);
            }
        }


        private void AssignArmor()
        {
            int armor = levelProgress.GetArmorStat();
            playerHealth.SetupArmor(armor);
           // Debug.Log(levelProgress.GetArmorLevel);
            asignVisEffects.SetArmorLevel(levelProgress.GetArmorLevel);
        }


        private void AssignGrenadeDamageAndRange()
        {
            int damage = levelProgress.GetGrenadeDamageStat();
            float range = levelProgress.GetGrenadeRangeStat();
            for (int i = 0; i < playerGrenadeAndMineDrop.Length; i++)
            {
                playerGrenadeAndMineDrop[i].SetupGrenadeDamage(damage, range);
            }
           // grenadeBoomSetup.SetupBoomSize(damage, range);
            //dupa
        }


        private void AssignMineDamageAndRange()
        {
            int damage = levelProgress.GetMineDamageStat();
            float range = levelProgress.GetMineRangeStat();
            for (int i = 0; i < playerGrenadeAndMineDrop.Length; i++)
            {
                playerGrenadeAndMineDrop[i].SetupMineDamage(damage, range);
            }
            // mineBoomSetup.SetupBoomSize(damage, range);
            //dupa
        }


        private void AssignGoldTakeRange()
        {
            float range = levelProgress.GetGoldRangeStat() * coinColliderMultiplier;
            playerTransform.localScale = new Vector3(range, range, 1);
            //cointTrigger.radius = levelProgress.GetGoldRangeStat();
            //CoinInstationerManager.instance.SetupCoinsRange(range);
        }


        private void AssignGoldCoinAdd()
        {
            int add = levelProgress.GetGoldAddStat();
            CanvasManager.instance.SetupCoinAdd(add);
        }


        private void AssignGoldFrequency()
        {
            float freq = levelProgress.GetGoldFrequencyStat();
            CoinsDropChanceAddManager.instance.SetCoinChanceAdd(freq);
        }
    }
}
