﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    [RequireComponent(typeof(CanvasManager))]
    public class CoinsAndXPCalculator: MonoBehaviour
    {
        [SerializeField]
        PlayerStats playerStats = null;
        [SerializeField]
        //XPAndCoinMultiplierStats xpCoinStat = null;

        Text coinBaseText = null, coinMultiplierText = null, coinAllText = null;
        Text xpBaseText = null, xpMultiplierText = null, xpAllText = null;
        CanvasManager cM = null;


        private void Start()
        {
            cM = GetComponent<CanvasManager>();

            InGameFiller inGameFiller = GameObject.FindObjectOfType<InGameFiller>();

            coinBaseText = inGameFiller.coinGainedText;
            coinMultiplierText = inGameFiller.coinMultiplierText;
            coinAllText = inGameFiller.coinAllRewardText;

            xpBaseText = inGameFiller.xpGainedText;
            xpMultiplierText = inGameFiller.xpMultiplierText;
            xpAllText = inGameFiller.xpAllRewardText;
        }


        public void CalculateCoins(int multiplier = 1)
        {
            int coins = cM.GetCoins;

            coinBaseText.text = coins.ToString();
            //coinMultiplierText.text = "X" + multiplier;

            int addMultiplier = 0;//= xpCoinStat.GetCoinMultiplier;
         ////   if(addMultiplier > xpCoinStat.defaultMultipier)
         //       if (addMultiplier > 0)
         //       {
         //       coinMultiplierText.text += " + X" + addMultiplier;
         //   }
            if(StoreSTDSGameData.instance != null)
            addMultiplier = StoreSTDSGameData.instance.stdsData.coinMultiplier;
            coinMultiplierText.text = "x" + (addMultiplier + multiplier);

            int allCoins = coins * (multiplier + addMultiplier);

            playerStats.AddCoins(allCoins);
            coinAllText.text = allCoins.ToString();
        }


        public void CalculateXP(int multiplier = 1)
        {
            int xp = cM.GetXP;

            if (!playerStats.CanLevelUp())
                xp = 0;

            xpBaseText.text = xp.ToString();
            //xpMultiplierText.text = "X" + multiplier;

            int addMultiplier = StoreSTDSGameData.instance.stdsData.xpStaticMultiplier;
            //if (StoreSTDSGameData.instance != null)
            //{
            //    addMultiplier = StoreSTDSGameData.instance.stdsData.xpMultiplier;// = xpCoinStat.GetXPMultiplier;
            //    addMultiplier += StoreSTDSGameData.instance.stdsData.xpStaticMultiplier;
            //}
            //Debug.Log(addMultiplier);

            //if (addMultiplier > xpCoinStat.defaultMultipier)
            //    if (addMultiplier > 0)
            //    {
            //        xpMultiplierText.text += " + X" + addMultiplier;
            //}
            xpMultiplierText.text = "x" + (addMultiplier);// +multiplier);
            int allXP = xp * addMultiplier;// (multiplier + addMultiplier);
            playerStats.AdXP(allXP);
            xpAllText.text = allXP.ToString();
           // xpCoinStat.RemoweFight();
        }


        void ResetMultiplier()
        {
            if (StoreSTDSGameData.instance.stdsData.multiplierActivity <= 0)
            {
                StoreSTDSGameData.instance.stdsData.coinMultiplier = 0;
                StoreSTDSGameData.instance.stdsData.xpMultiplier = 0;
                StoreSTDSGameData.instance.stdsData.multiplierActivity = 0;
            }
        }

        public void DecreaseAdRewardMultiplier()
        {
            if (StoreSTDSGameData.instance != null)
            {
                if (StoreSTDSGameData.instance.stdsData.multiplierActivity > 0)
                {
                    StoreSTDSGameData.instance.stdsData.multiplierActivity -= 1;

                }

                ResetMultiplier();
            }
        }
    }
}
