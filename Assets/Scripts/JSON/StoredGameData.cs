﻿using UnityEngine;
using System.IO;
using System;
using System.Text;
using System.Security.Cryptography;

namespace STDS
{
   // [Serializable]
    public class STDSdata
    {
        public float allCampignTime, actualCampignTime;
        public int liveLevel;
        public int liveRegenerationLevel;
        public int bulletDamageLevel;
        public int bulletRateLevel;
        public int movementSpeedLevel;
        public int armorLevel;

        public int grenadeDamageLevel;
        public int grenadeRangeLevel;
        public int mineDamageLevel;
        public int mineRangeLevel;

        public int goldTakeRangeLevel;
        public int goldCoinAddLevel;
        public int goldFrequencyLevel;


        public int campignLevel;
        public int abilitesLevel;

        public int xp;
        public int coins;
        public string playerName;
        public int level;
        public int nextLevelUpXP;
        public int lastLevelUpXP;
        public int skillPoints;


        public int mines, grenades;
        int maxMines, maxGrenades;
        int baseForUsableItem;

        public int language;
        public float soundVolume;
        public float musicVolume;


        public int coinMultiplier;
        public int xpMultiplier;
        public int multiplierActivity;
        public int autoAim;
        public int[] mobileControlSet;
        public float grenadeBoomTime;
        public float mineBoomTime;

        public float bestTimeForCampign;
        public float currentTimeForCampign;
       // public int firstEndlessHighscore;
       // public int mysteryEndlessHighscore;
        public int endlessOpen = 0;
        public int[] mysteryFind;
        public int mysteryFull = 0;

        public int smallArenaHighscore, bigArenaHighscore;
        public int smallArenaWaveHigscore, bigArenaWaveHighscore;

        public int spectator, founder;
        public int campignEndOnce;
        public int introOutroWatch = 0;
        public int xpStaticMultiplier = 1;
        public int showDisplay = 0;
        public int smooth = 1;
        public int timeScale = 1;

        public int AbilityLevel
        {
            get
            {
                return abilitesLevel;
            }
        }

        public int CampignLevel
        {
            set
            {
                campignLevel = value;

                if (value > abilitesLevel)
                    abilitesLevel = value;

                if (abilitesLevel >= 10)
                    campignEndOnce = 1;

            }
            get
            {
                return campignLevel;
            }
        }


        public bool AutoAim
        {
            set
            {
                if (value == true)
                    autoAim = 1;
                else
                    autoAim = 0;
            }
            get
            {
                if (autoAim > 0)
                    return true;
                else
                    return false;
            }
        }

        public STDSdata()
        {
            liveLevel = liveRegenerationLevel = bulletDamageLevel = bulletRateLevel =
            movementSpeedLevel = armorLevel = grenadeDamageLevel = grenadeRangeLevel =
            mineDamageLevel = mineRangeLevel = goldCoinAddLevel = goldFrequencyLevel =
            goldTakeRangeLevel = campignLevel = xp = mines = grenades = language =
            coinMultiplier = xpMultiplier = multiplierActivity = skillPoints =
            nextLevelUpXP = level = lastLevelUpXP = 0;

            coins = 5000;

            #if UNITY_ANDROID || UNITY_IOS
            soundVolume = 0f;
            musicVolume = -20f;
            autoAim = 1;
            #else
            soundVolume = -23f;
            musicVolume = -48f;
            autoAim = 0;
            #endif

            abilitesLevel = 0;

            //maxGrenades = maxMines = 5;
            baseForUsableItem = 5;
            maxGrenades = baseForUsableItem + grenadeDamageLevel + grenadeRangeLevel;

            maxMines = baseForUsableItem + mineDamageLevel + mineRangeLevel;

            playerName = "Player";
            mobileControlSet = new int[4];
            for (int i = 0; i < mobileControlSet.Length; i++)
            {
                mobileControlSet[i] =(i+1);
            }
            mysteryFind = new int[7];

            grenadeBoomTime = mineBoomTime = 2f;

            smallArenaHighscore = bigArenaHighscore = smallArenaWaveHigscore = bigArenaWaveHighscore = 0;
            spectator = founder = introOutroWatch = 0;
            allCampignTime = 1000000;

            xpStaticMultiplier = 1;
            smooth = 1;
            timeScale =2;
        }

        public int MaxGrenades
        {
            get
            {
                maxGrenades = baseForUsableItem + grenadeDamageLevel + grenadeRangeLevel;
                return maxGrenades;
            }
        }

        public int MaxMines
        {
            get
            {
                maxMines = baseForUsableItem + mineDamageLevel + mineRangeLevel;
                return maxMines;
            }
        }
    }

    //[System.Serializable]
    public class StoreSTDSGameData
    {
        static readonly string PasswordHash = "P@hSw0rdessd";
        static readonly string SaltKey = "S@LT&dswdwKEY";
        static readonly string VIKey = "@1B2c3Dh4e5F6g7H";

        public static string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }


        public static StoreSTDSGameData instance = null;
        
        public STDSdata stdsData = new STDSdata();

        LevelProgress levelProgres;
        //Campign campign;
        EquipmentStats equipmentStats;
        PlayerStats playerStats;
        //Settings settings;
        XPAndCoinMultiplierStats xpAndCoinMultiplierStats;

        string dataFile = "gameData.gd";
        string folderForData = "zap";
        bool codeFile = true;

        void Init()
        {
            instance = this;
            stdsData = new STDSdata();
            if (Application.systemLanguage != SystemLanguage.Polish)
                stdsData.language = 1;
            else
                stdsData.language = 0;
        }

        void InitJson()
        {
            if (File.Exists(GetDataFilePath()))
            {
                Load();
            }
            else
            {
                Save();
                Load();
            }
        }

        public StoreSTDSGameData()
        {
            Init();
        }

        public StoreSTDSGameData(LevelProgress lp, EquipmentStats es, PlayerStats ps, XPAndCoinMultiplierStats xpAnCoinMul, string fileName,  string folder = "",  bool code = false)
        {
            Init();
            levelProgres = lp;
            levelProgres.SetStoreData(instance);
            //campign = cm;
            //campign.SetStoreData(instance);
            equipmentStats = es;
            equipmentStats.SetStoreData(instance);
            playerStats = ps;
            playerStats.SetStoreData(instance);
            //settings = set;
            //settings.SetStoreData(instance);
            xpAndCoinMultiplierStats = xpAnCoinMul;
            xpAndCoinMultiplierStats.SetStoreData(instance);

            dataFile = fileName;
            folderForData = folder;
            codeFile = code;
            //if(initJsonAgain)
            //{
            //    SaveNew();
            //    Load();
            //}
            //    else
            InitJson();
            FillAll();
        }


        //public void SaveNew()
        //{
        //    File.Delete(GetDataFilePath());
        //    FileStream fs = new FileStream(GetDataFilePath(), FileMode.CreateNew);
        //    BinaryFormatter formatter = new BinaryFormatter();

        //    try
        //    {
        //        formatter.Serialize(fs, StoreSTDSGameData.Encrypt(JsonUtility.ToJson(new STDSdata())));
        //    }
        //    catch (SerializationException e)
        //    {
        //        Console.WriteLine("Failed to serialize. Reason: " + e.Message);
        //        throw;
        //    }
        //    finally
        //    {
        //        fs.Close();
        //    }
        //}


        public void Save()
        {

           // if (!File.Exists(GetDataFilePath()))
            {
                // Create a file to write to.
                string createText = JsonUtility.ToJson(stdsData);
                try
                {
                    if (codeFile)
                        File.WriteAllText(GetDataFilePath(), Encrypt(createText));
                    else
                       File.WriteAllText(GetDataFilePath(), createText);
                }
                catch (Exception e)
                {
                    Debug.LogError("Dupa problem z zapisame pliku" + e.ToString());
                }
            }
            //else
            //{
            //    File.
            //}
            //FileStream fs = new FileStream(GetDataFilePath(), FileMode.Create);
            ////fs.w
            ////BinaryWriter bw = new BinaryWriter(
            //BinaryFormatter formatter = new BinaryFormatter();
            ////BinaryWriter bw = new BinaryWriter(fs);

            //try
            //{
            //    //bw.Write(JsonUtility.ToJson(stdsData));
            //    formatter.Serialize(fs, Encrypt( JsonUtility.ToJson(stdsData)));
            //    //formatter.Serialize(fs, JsonUtility.ToJson(stdsData));
            //}
            //catch (SerializationException e)
            //{
            //    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
            //    throw;
            //}
            //finally
            //{
            //    fs.Close();
            //}
        }


        public void Load()
        {
            try
            {
                string readText = File.ReadAllText(GetDataFilePath());
                try
                {
                    if (codeFile)
                        stdsData = JsonUtility.FromJson<STDSdata>(Decrypt(readText));
                   else
                       stdsData = JsonUtility.FromJson<STDSdata>(readText);
                }
                catch (Exception e)
                {
                     Debug.Log("Ktoś kobinuje nadpisz  " + e.Message);
                     File.Delete(GetDataFilePath());
                    instance = new StoreSTDSGameData(levelProgres,  equipmentStats, playerStats, xpAndCoinMultiplierStats, dataFile, folderForData);
                    //   // SaveNew();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("dupa z odczytaniem pliku " + e.Message);
            }
            //finally
            //{

            //}

            //FileStream fs = new FileStream(GetDataFilePath(), FileMode.Open);
            //
            //// fs.
            //try
            //{
            //    BinaryFormatter formatter = new BinaryFormatter();
            //   // BinaryWriter bw = new BinaryWriter(fs);

            //    try {
            //        string str = (string)formatter.Deserialize(fs);
            //        //string str = bw.BaseStream.ToString();
            //        stdsData = JsonUtility.FromJson<STDSdata>(Decrypt(str));
            //        //stdsData = JsonUtility.FromJson<STDSdata>(str);
            //        FillAll();
            //    }
            //    catch(Exception e)
            //    {
            //        Debug.LogError("Error " + e);
            //        fs.Close();
            //        File.Delete(GetDataFilePath());
            //        new StoreSTDSGameData(levelProgres, equipmentStats, playerStats, xpAndCoinMultiplierStats, dataFile, folderForData, true);
            //    }
            //    finally
            //    {

            //    }
            //}
            //catch (SerializationException e)
            //{
            //    Debug.Log("Ktoś kobinuje nadpisz  " + e.Message);
            //   // File.Delete(GetDataFilePath());
            //  //  instance = new StoreSTDSGameData(levelProgres, campign, equipmentStats, playerStats, settings, xpAndCoinMultiplierStats, dataFile, folderForData);
            //   // SaveNew();
            //    throw;
            //}
            //finally
            //{
            //    fs.Close();
            //}
        }

        string GetDataFilePath()
        {
            string add = (folderForData != "") ? folderForData + "/" : "";

#if UNITY_ANDROID || UNITY_IOS || UNITY_WSA || UNITY_WSA_10_0
            //Debug.Log(Application.persistentDataPath);
            return Application.persistentDataPath + "/" + add + dataFile;
            


#else
            return Application.dataPath + "/" + add + dataFile;
#endif
        }

        void FillAll()
        {
            FillLevelProgress();
           // FillCampignProgress();
           // FillEquipmentStats();
            FillPlayerStats();
            //FillSettings();
            FillXpAndCoinMultiplier();
        }

        void FillLevelProgress()
        {
            levelProgres.liveLevel = stdsData.liveLevel;
            levelProgres.liveRegenLevel = stdsData.liveRegenerationLevel;
            levelProgres.armorLevel = stdsData.armorLevel;
            levelProgres.bulletDamageLevel = stdsData.bulletDamageLevel;
            levelProgres.bulletRateLevel = stdsData.bulletRateLevel;
            levelProgres.movementLevel = stdsData.movementSpeedLevel;

            levelProgres.grenadeDamageLevel = stdsData.grenadeDamageLevel;
            levelProgres.grenadeRangeLevel = stdsData.grenadeRangeLevel;
            levelProgres.mineDamageLevel = stdsData.mineDamageLevel;
            levelProgres.mineRangeLevel = stdsData.mineRangeLevel;

            levelProgres.goldAddLevel = stdsData.goldCoinAddLevel;
            levelProgres.goldFrequencyLevel = stdsData.goldFrequencyLevel;
            levelProgres.goldRangeLevel = stdsData.goldTakeRangeLevel;
           

        }

        //void FillCampignProgress()
        //{
        //    //campign.CurrentActiveScene = stdsData.campignLevel;
        //    //campign.ChangeCurrentActiveCampign(stdsData.campignLevel);
        //}


        //void FillEquipmentStats()
        //{
            //equipmentStats.grenades = stdsData.grenades;
            //equipmentStats.mines = stdsData.mines;
        //}

        void FillPlayerStats()
        {
            playerStats.xp = stdsData.xp;
            playerStats.coins = stdsData.coins;
            playerStats.level = stdsData.level;
            playerStats.name = stdsData.playerName;
            playerStats.nextLevelXP = stdsData.nextLevelUpXP;
           // playerStats.skillPoints = stdsData.skillPoints;
        }

        //void FillSettings()
        //{
        //    settings.lang = (LANGUAGE)Enum.ToObject(typeof(LANGUAGE), stdsData.language);
        //    settings.musicVolume = stdsData.musicVolume;
        //    settings.soundVolume = stdsData.soundVolume;
        //    settings.playerName = stdsData.playerName;
        //}

        void FillXpAndCoinMultiplier()
        {
            xpAndCoinMultiplierStats.coinMultiplier = stdsData.coinMultiplier;
            xpAndCoinMultiplierStats.xpMultiplier = stdsData.xpMultiplier;
            xpAndCoinMultiplierStats.XPAndCointMultiplierActiveFights = stdsData.multiplierActivity;
        }
    }
}
