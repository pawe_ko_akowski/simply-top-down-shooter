﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class PlaceToSpawnFinder : PlaceToSpawn
    {
        [SerializeField]
        Rect[] rectPlacesForSpwan = null;
        //[SerializeField]
       // bool customRadius = false;
        [SerializeField]
        float radiusForPositionCheck = 2f;


        void Start()
        {
            List<Vector2> placesForSpawn = new List<Vector2>();
            for (int i = 0; i < rectPlacesForSpwan.Length; i++)
            {
                int startX = (int)(rectPlacesForSpwan[i].x - rectPlacesForSpwan[i].width / 2);
                int startY = (int)(rectPlacesForSpwan[i].y - rectPlacesForSpwan[i].height / 2);
                int endX = (int)(rectPlacesForSpwan[i].x + rectPlacesForSpwan[i].width / 2);
                int endY = (int)(rectPlacesForSpwan[i].y + rectPlacesForSpwan[i].height / 2);

                for (int j = startX; j < endX; j++)
                {
                    for (int k = startY; k < endY; k++)
                    {
                        Vector2 placeCheck = new Vector2(j, k);
                        if (Physics2D.OverlapCircle(placeCheck, radiusForPositionCheck) == null)
                            placesForSpawn.Add(placeCheck);
                    }
                }
            }
            //Debug.Log("Znalazlem miejsca do spawnowania w ilosci - " + placesForSpawn.Count);
            spawnPlace = new Vector2[placesForSpawn.Count];
            for (int m = 0; m < placesForSpawn.Count; m++)
            {
                spawnPlace[m] = placesForSpawn[m];
            }
            placesForSpawn.Clear();
        }


        float GetBigestRadius()
        {
            //if (customRadius)
                return radiusForPositionCheck;
            //for example for now dupa
           // return 1f;
        }
    }
}
