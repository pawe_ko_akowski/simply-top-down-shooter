﻿using UnityEngine;

namespace STDS
{
    public class PlaceToSpawn : MonoBehaviour
    {
        [Header("Put here all spaw transform to fill array")]
        [SerializeField]
        private Transform[] spawnPlacesToAsign = null;
        [Header("Destroy all asigned places obiects after spawn places init")]
        [SerializeField]
        private bool destroyPlacesAfterAsign = false;
        //public string name = "dupa";

        protected Vector2[] spawnPlace;


        void Start()
        {
            if (spawnPlacesToAsign.Length == 0)
            {
                Debug.LogError("Please asign some places");
                return;
            }
            FillSpawnPlaces();
            DestroyAllAsignedPlaceObiect();
        }


        void FillSpawnPlaces()
        {
            spawnPlace = new Vector2[spawnPlacesToAsign.Length];
            for (int i = 0; i < spawnPlacesToAsign.Length; i++)
            {
                spawnPlace[i] = spawnPlacesToAsign[i].position;
            }
        }


        void DestroyAllAsignedPlaceObiect()
        {
            if (destroyPlacesAfterAsign)
                for (int i = 0; i < spawnPlacesToAsign.Length; i++)
                {
                    Destroy(spawnPlacesToAsign[i].gameObject);
                }
        }


        public Vector2 GetRandomPlaceForSpawn()
        {
            //Vector2 plac = spawnPlace[Random.Range(0, spawnPlace.Length)];
           // Debug.Log(plac);
            return spawnPlace[Random.Range(0, spawnPlace.Length)];
        }
    }
}
