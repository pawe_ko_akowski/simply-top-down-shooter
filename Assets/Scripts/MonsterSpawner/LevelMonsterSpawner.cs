﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace STDS
{
    [RequireComponent(typeof(PlaceToSpawn))]
    public class LevelMonsterSpawner : MonoBehaviour
    {
        [Serializable]
        class SpawnForWave
        {
            public int spawnGameobiectIndex =0;
            public int amount = 0;
            int spawnForThis = 0;


            public int SpawnForThis
            {
                get
                {
                    return spawnForThis;
                }

                set
                {
                    spawnForThis = value;
                }
            }

            public void IncreaseSpawn()
            {
                spawnForThis++;
            }

            public bool IsFull()
            {
                if (spawnForThis < amount)
                {
                    spawnForThis++;
                    return false;
                }
                return true;
            }

        }

        [Serializable]
        class Wave
        {
            public float spawnDelay = 0;
            public SpawnForWave[] spawnForWave = null;

            int allWaveSpawn = 0;
            

            public int AllWaveSpawn
            {
                get
                {
                    return allWaveSpawn;
                }
              //  set
              //  {
               //     allWaveSpawn = value;
               // }
            }

            public void IncreaseSpwanValue(int amount)
            {
                allWaveSpawn += amount;
            }
        }

        delegate void EnemySpawner();
        //EnemySpawner enemySpawnerDelegate;
        EnemySpawner scoreCalculation;
        EnemySpawner levelCalculation;

        public static LevelMonsterSpawner instance = null;
        public delegate void nextWaveDelegate();
        public nextWaveDelegate nextWave = null;

        //[SerializeField]
        private Text infoText = null;
        [SerializeField]
        bool automaticStart = true;
        [SerializeField]
        Wave[] waves = null;
        [SerializeField]
        GameObject[] monstersPrefabs = null;
        [SerializeField]
        float waveBreakTime = 0;
        [SerializeField]
        bool waveEndGame = true;
        [SerializeField]
        bool endlesMode = false;
        [SerializeField]
        bool showEnemySpawnPosition = false;
        //[SerializeField]
       // bool randomEnemySpawnInOneWave = true;
        [SerializeField]
        ScorePointsCalculator sPC = null;
      
        public LevelEvolution lE = null;
       // [SerializeField]
        Animator showEnemySpanwnAnimator = null;

        List<GameObject[]> gameObiectList = new List<GameObject[]>();
        List<EnemyEvolutionProgress> evolutionList = new List<EnemyEvolutionProgress>();
        EnemyParticleDamager[] enemyParticleDamager;
        PlaceManager pM = null;
        bool setUP = false;

        PlaceToSpawn placeFinder;
        int[] spawnCreationForWave;
        int currentWave = 0;//, lastWave = -1;
       // int waveLevel = 1;
        //int currentCreation = 0;
        List<int> forEveryEnemyKindInWaveList = new List<int>();
        int currentKind = 0;
        int currentKindIndex = 0;
        //bool spawn = true;
        int waveLiveMonster = 0;
        Vector2 place;
        int actualWaveSpawn = 0;
        //PlayerInGameLanguageSetup playerInGameLangSetup;
        public AudioClip nextBatleAudioclip;
        public bool playNextBattleAudio = true;

        bool readyOne = false;//, readyTwo = false;
        float timer = 0;


        public int CurrentWave
        {
            get
            {
                return currentWave;
            }
        }

        public bool EndlessMode
        {
            get
            {
                return endlesMode;
            }
        }

        //void OneByOneEnemySpawn()
        //{
        //   // Debug.Log("dupa");
        //    StartCoroutine(StartCreateMonster());
        //}

        //void RandomEnemySpawn()
        //{
        //    //  Debug.Log("dupa");
        //   // StopAllCoroutines();
        //    StartCoroutine(StartCreateMonster2());
        //}

        void Start()
        {
            
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            //   lastWave = 0;

            InGameFiller inGameFiller = FindObjectOfType<InGameFiller>();
            infoText = inGameFiller.infoText;

            //if (infoText != null)
            infoText.text = "";

            placeFinder = GetComponent<PlaceToSpawn>();

            if(GameObject.Find("beforeSpawnPrefab") != null)
            showEnemySpanwnAnimator = GameObject.Find("beforeSpawnPrefab").GetComponent<Animator>();

           // playerInGameLangSetup = FindObjectOfType<PlayerInGameLanguageSetup>();

            int monsterKind = monstersPrefabs.Length;
            int[] maxAmountForAllKind = new int[monsterKind];

            spawnCreationForWave = new int[waves.Length];

            for (int i = 0; i < waves.Length; i++)
            {
                if (waves[i].spawnForWave.Length == 0)
                    Debug.LogError("nic nie wypuszczasz - napraw to albo usun fale");

                if (waves[i].spawnForWave[0].amount == 0)
                    Debug.LogError("nie wypuszczasz żadnych stworów napraw to albo usuń wypuszczenie");


                for (int j = 0; j < waves[i].spawnForWave.Length; j++)
                {
                    int spawnIndex = waves[i].spawnForWave[j].spawnGameobiectIndex;
                    int many = waves[i].spawnForWave[j].amount;
                    waves[i].IncreaseSpwanValue(many);

                    if (maxAmountForAllKind[spawnIndex] < many)
                        maxAmountForAllKind[spawnIndex] = many;

                    spawnCreationForWave[i] += many;

                    if (i == currentWave)
                    {
                        forEveryEnemyKindInWaveList.Add(many);
                    }
                }
            }

            AllNededMonsterCreation(maxAmountForAllKind);

            //for (int z = 0; z < spawnCreationForWave.Length; z++)
            // Debug.Log(spawnCreationForWave[z].ToString());

            //if (randomEnemySpawnInOneWave)
            //    enemySpawnerDelegate = RandomEnemySpawn;
            //else
            //    enemySpawnerDelegate = OneByOneEnemySpawn;

            if (automaticStart)
            {
                readyOne = true;
                timer = Time.time + waveBreakTime;
                //  if (infoText != null)
                //infoText.text = "First wave comming";
                //playerInGameLangSetup.SetupStart();
                
               // enemySpawnerDelegate();
               // RandomEnemySpawn();
            }

            waveLiveMonster = waves[currentWave].AllWaveSpawn;// spawnCreationForWave[currentWave];
            SetScorePointCalculatorDelegates();

            if (endlesMode)
                enemyParticleDamager = GameObject.FindObjectsOfType<EnemyParticleDamager>();

            pM = GetComponent<PlaceManager>();

            //Debug.Log("waveob " + waveLiveMonster);
        }


        private void Update()
        {
            if (readyOne)
            {
                if (Time.time > timer)
                {
                   // Debug.Log(placeFinder.name);
                    CreateMonsterRandomly();
                    //enemySpawnerDelegate();
                }
            }
        }

        private void SetScorePointCalculatorDelegates()
        {
            if (sPC != null)
            {
                scoreCalculation = sPC.GiveKilInformation;
                levelCalculation = sPC.GiveLevelInformation;
            }
            else
            {
                scoreCalculation = levelCalculation = NoneFunction;
            }
        }

        void SpawnMonster()
        {
            
           // Debug.Log("Wave " + currentWave + ",  currentKind " + currentKind + ",  currentKindIndex " + currentKindIndex);
            gameObiectList[waves[currentWave].spawnForWave[currentKind].spawnGameobiectIndex][currentKindIndex].transform.position = place;
            gameObiectList[waves[currentWave].spawnForWave[currentKind].spawnGameobiectIndex][currentKindIndex].SetActive(true);
            //  waveLiveMonster++;
        }


        void StopAllCourutines()
        {
            StopAllCoroutines();
        }


        void AllNededMonsterCreation(int[] maxAmountForAllKind)
        {
            // Instantiate(monstersPrefabs[])

            for (int i = 0; i < maxAmountForAllKind.Length; i++)
            {
                GameObject[] obiects = new GameObject[maxAmountForAllKind[i]];
                for (int j = 0; j < maxAmountForAllKind[i]; j++)
                {
                    obiects[j] = Instantiate(monstersPrefabs[i]);
                    if (endlesMode)
                        evolutionList.Add(obiects[j].GetComponent<EnemyEvolutionProgress>());
                    obiects[j].SetActive(false);
                }

                gameObiectList.Add(obiects);
            }


           // Debug.Log("Mam rodzajów potworów w ilości - " + gameObiectList.Count);
           // for (int k = 0; k < gameObiectList.Count; k++)
                //Debug.Log("Index - " + k + " ma w sobie potworow w ilosci " + gameObiectList[k].Length);
        }

        void ChangeRand(ref int rand)
        {
            rand++;
            if (rand >= waves[currentWave].spawnForWave.Length)
                rand = 0;
        }

        void SpawnMonster2()
        {
            int rand = UnityEngine.Random.Range(0, waves[currentWave].spawnForWave.Length);
            //int allSpawn = waves[currentWave].spawnForWave.Length;
            //int counter = 0;

            while(waves[currentWave].spawnForWave[rand].IsFull())
            {
                ChangeRand(ref rand);
               // counter++;
               // if (counter >= allSpawn)
                 //   break;             
            }

           // if (counter < allSpawn)
            //{
                int whatIndex = waves[currentWave].spawnForWave[rand].SpawnForThis - 1;


                ThrowGameobiect(gameObiectList[waves[currentWave].spawnForWave[rand].spawnGameobiectIndex][whatIndex]);
           // }
        }

        void ThrowGameobiect(GameObject gob)
        {
            //Debug.Log(place);
            gob.transform.position = place;
            gob.SetActive(true);
        }


        void ResetAllSpawnObiects()
        {
            for (int i = 0; i < waves[currentWave].spawnForWave.Length; i++)
                waves[currentWave].spawnForWave[i].SpawnForThis = 0;
        }

        void AddSpawnValues()
        {
            //if(actualWaveSpawn >=waves[currentWave].AllWaveSpawn)
            //{
              
            //    ResetAllSpawnObiects();
            //    actualWaveSpawn = 0;
            //    currentWave++;
            //    spawn = false;

            //    if(endlesMode)
            //    {
            //        if (currentWave >= waves.Length)
            //            currentWave = 0;
            //    }

            //    //StopAllCoroutines();
            //}
            //else
          //  {
                actualWaveSpawn++;
                SpawnMonster2();
                if (actualWaveSpawn >= waves[currentWave].AllWaveSpawn)
                {

                    ResetAllSpawnObiects();
                    actualWaveSpawn = 0;
                    currentWave++;

                //  waveLevel++;
                  //  spawn = false;
                    readyOne = false;
                    //readyOne = false;

                if (endlesMode)
                {
                    if (currentWave >= waves.Length)
                    {
                        currentWave = 0;
                        setUP = true;
                        timer = Time.time + waveBreakTime;
                    }
                    //progress enemies

                }
                //else
                //{
                //    readyOne = false;
                //}

                    //StopAllCoroutines();
            //    }

            }

          //  if (spawn)
           // {
               
                //actualWaveSpawn++;
               // currentCreation++;
               // currentKindIndex++;
         //   }



           /* if (currentKindIndex >= forEveryEnemyKindInWaveList[currentKind])
            {
                currentKindIndex = 0;
                currentKind++;

                if (currentKind >= (forEveryEnemyKindInWaveList.Count))
                {
                    //  if(automaticStart)
                    currentWave++;
                    currentCreation = 0;
                    currentKind = 0;
                    forEveryEnemyKindInWaveList.Clear();
                    spawn = false;

                    if (currentWave < waves.Length)
                    {

                        for (int j = 0; j < waves[currentWave].spawnForWave.Length; j++)
                        {
                            int many = waves[currentWave].spawnForWave[j].amount;

                            forEveryEnemyKindInWaveList.Add(many);

                        }


                        // StopCoroutine(StartCreateMonster());
                        
                        // Debug.Log("Pupa");
                    }
                    else
                    {
                        if (!endlesMode)
                            spawn = false;
                        else
                        {
                            currentWave = 0;
                            for (int j = 0; j < waves[currentWave].spawnForWave.Length; j++)
                            {
                                int many = waves[currentWave].spawnForWave[j].amount;

                                forEveryEnemyKindInWaveList.Add(many);

                            }
                        }

                        // Debug.Log("Dupa");
                        //yield return null;
                    }


                }
            }*/
        }

        void CreateMonsterRandomly()
        {
            //if(spawn)
          //  if(Time.time > timer)
          //  {
               // if (actualWaveSpawn < waves[currentWave].AllWaveSpawn)
              //  {
                    //Debug.Log(placeFinder.name);
                    place = placeFinder.GetRandomPlaceForSpawn();
                    if (showEnemySpawnPosition)
                    {
                        showEnemySpanwnAnimator.transform.position = place;
                        showEnemySpanwnAnimator.Play("beforeSpawnShow", -1, 0f);
                    }
               // }

                    timer = Time.time + waves[currentWave].spawnDelay;
                    AddSpawnValues();
          //  }
        }


        //IEnumerator StartCreateMonster2()
        //{
        //    yield return new WaitForSeconds(waveBreakTime);
        //    spawn = true;

        //   // if (infoText != null)
        //        infoText.text = "";




        //    //while(currentCreation < spawnCreationForWave[currentWave] + 1)
        //    while (spawn)
        //    {
        //        if (actualWaveSpawn < waves[currentWave].AllWaveSpawn)
        //        {
        //            place = placeFinder.GetRandomPlaceForSpawn();
        //            if (showEnemySpawnPosition)
        //            {
        //                //  if (currentKind < (forEveryEnemyKindInWaveList.Count) && currentKindIndex < forEveryEnemyKindInWaveList[currentKind])
        //                //   {
        //                showEnemySpanwnAnimator.transform.position = place;
        //                showEnemySpanwnAnimator.Play("beforeSpawnShow", -1, 0f);
        //                //   }
        //            }
        //        }

        //        yield return new WaitForSeconds(waves[currentWave].spawnDelay);
        //        //  if (currentWave < waves.Length)
        //        //   {

        //        //   }
        //        //   else
        //        //    yield return null;

        //        AddSpawnValues();
        //    }
        //}



        //IEnumerator StartCreateMonster()
        //{
        //    yield return new WaitForSeconds(waveBreakTime);
        //    spawn = true;

        //    //if (infoText != null)
        //        infoText.text = "";
        //    //while(currentCreation < spawnCreationForWave[currentWave] + 1)
        //    while (spawn)
        //    {
        //        //if (currentWave != lastWave)
        //        //{
        //        //    lastWave = currentWave;
        //        //    yield return new WaitForSeconds(waveBreakTime);
        //        //}

        //        if (currentWave < waves.Length)
        //        {
        //            place = placeFinder.GetRandomPlaceForSpawn();
        //            if(showEnemySpawnPosition)
        //            {
        //                if (currentKind < (forEveryEnemyKindInWaveList.Count )&& currentKindIndex < forEveryEnemyKindInWaveList[currentKind])
        //                {
        //                    showEnemySpanwnAnimator.transform.position = place;
        //                    showEnemySpanwnAnimator.Play("beforeSpawnShow", -1, 0f);
        //                }
        //            }
        //            yield return new WaitForSeconds(waves[currentWave].spawnDelay);
        //        }
        //        else
        //            yield return null;

        //        if (currentKindIndex >= forEveryEnemyKindInWaveList[currentKind])
        //        {
        //            currentKindIndex = 0;
        //            currentKind++;

        //            if (currentKind >= (forEveryEnemyKindInWaveList.Count))
        //            {
        //              //  if(automaticStart)
        //                currentWave++;
        //                currentCreation = 0;
        //                currentKind = 0;
        //                forEveryEnemyKindInWaveList.Clear();
        //                spawn = false;

        //                if (currentWave < waves.Length)
        //                {

        //                    for (int j = 0; j < waves[currentWave].spawnForWave.Length; j++)
        //                    {
        //                        int many = waves[currentWave].spawnForWave[j].amount;

        //                        forEveryEnemyKindInWaveList.Add(many);

        //                    }


        //                    // StopCoroutine(StartCreateMonster());
                           
        //                    // Debug.Log("Pupa");
        //                }
        //                else
        //                {
        //                    if(!endlesMode)
        //                    spawn = false;
        //                    else
        //                    {
        //                        currentWave = 0;
        //                        for (int j = 0; j < waves[currentWave].spawnForWave.Length; j++)
        //                        {
        //                            int many = waves[currentWave].spawnForWave[j].amount;

        //                            forEveryEnemyKindInWaveList.Add(many);

        //                        }
        //                    }

        //                    // Debug.Log("Dupa");
        //                    //yield return null;
        //                }


        //            }
        //        }

        //        if (spawn)
        //        {
        //            SpawnMonster();
        //            currentCreation++;
        //            currentKindIndex++;
        //        }


        //    }
        //}


        void MonsterCreationChange()
        {
            if (currentKindIndex >= forEveryEnemyKindInWaveList[currentKind])
            {
                currentKindIndex = 0;
                currentKind++;

                if (currentKind >= (forEveryEnemyKindInWaveList.Count))
                {
                    currentWave++;
                    //currentCreation = 0;
                    currentKind = 0;
                    forEveryEnemyKindInWaveList.Clear();

                    if (currentWave <= waves.Length - 1)
                    {
                        for (int j = 0; j < waves[currentWave].spawnForWave.Length; j++)
                        {
                            int many = waves[currentWave].spawnForWave[j].amount;

                            forEveryEnemyKindInWaveList.Add(many);

                        }
                       // Debug.Log("Pupa");
                    }
                    else
                    {
                      //  Debug.Log("Dupa");
                        // yield return null;
                    }
                }
            }
        }


        private void ScoreOnDestroy()
        {

        }

        private void NoneFunction()
        {

        }

        public void DestroyMonster()
        {
            scoreCalculation();
            waveLiveMonster--;
           // Debug.Log(waveLiveMonster);
            // if(currentWave != lastWave)
            if (waveLiveMonster <= 0)
            {
                // Debug.Log("Koniec fali");

                if (nextWave != null)
                    //StopAllCoroutines();
                   nextWave();
                //  lastWave = currentWave;

                if (currentWave < waves.Length)
                {
                    waveLiveMonster = waves[currentWave].AllWaveSpawn;// spawnCreationForWave[currentWave];
                   // waveLiveMonster = spawnCreationForWave[currentWave];
                    if (automaticStart)
                    {
                        if(playNextBattleAudio)
                        AudioManager.instance.Play2DAudio(nextBatleAudioclip);
                       // playerInGameLangSetup.SetupNextWave();
                        levelCalculation();
                        if (endlesMode && setUP)
                        {
                            for (int i = 0; i < enemyParticleDamager.Length; i++)
                                enemyParticleDamager[i].ChangeBuletDamage(lE.damageAdd);

                            for (int i = 0; i < evolutionList.Count; i++)
                                evolutionList[i].ProgresMonster(lE);

                            setUP = false;
                        }

                        // if (infoText != null)
                        //infoText.text = "Wave end, next comming";
                        //RandomEnemySpawn();
                        readyOne = true;
                        timer = Time.time + waveBreakTime;
                        //spawn = true;
                   // enemySpawnerDelegate();
                    }
                }
                else
                {
                   // if (automaticStart)
                   // {
                      //  playerInGameLangSetup.SetupEndGame();
                        //if (infoText != null)
                        // infoText.text = "Wave end";

                   // }

                    if (waveEndGame)
                    {
                        readyOne = false;
                        GameManager.instance.SuccesEndGame();
                    }
                    // Debug.Log("End game or boss fight ");
                }

            }
        }


        public void AddDelegateToNextWaveDelegate(nextWaveDelegate next)
        {
            nextWave += next;
            readyOne = false;
            //nextWave += StopAllCourutines;
        }


        public void AsignPlaceFinder(PlaceToSpawn placeToSpawn)
        {
            placeFinder = placeToSpawn;

            //Debug.Log(placeFinder.name);
        }


        public void StartNextWave(int wave)
        {
           // if (currentWave == 0)
              //  playerInGameLangSetup.SetupStart();
                currentWave = wave;
            
            if (pM != null)
                pM.AsignNext();
            waveLiveMonster = waves[currentWave].AllWaveSpawn;
            //nextWave();
            // if (infoText != null)
            // infoText.text = "First wave comming";
            //  RandomEnemySpawn();
            readyOne = true;
            timer = Time.time + waveBreakTime;
           // enemySpawnerDelegate();
        }
    }
}