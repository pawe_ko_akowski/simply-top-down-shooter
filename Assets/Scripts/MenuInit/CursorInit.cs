﻿using UnityEngine;

namespace STDS
{
    public class CursorInit : MonoBehaviour
    {
        [SerializeField]
        Texture2D cursorTexture = null;


        void Start()
        {
            Cursor.SetCursor(cursorTexture, new Vector2(cursorTexture.width / 2, cursorTexture.height / 2), CursorMode.ForceSoftware);
        }
    }
}
