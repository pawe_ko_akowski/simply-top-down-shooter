﻿using UnityEngine;
using STDS.Steer2D;

namespace STDS
{
    public class MineHideAndBlowUpAttack : MineAttack
    {
        [SerializeField]
        protected float hideDistRange = 7f;
        [SerializeField]
        protected SpriteRenderer spriteRend;
        [SerializeField]
        protected float hidedTime = 6f;
        [SerializeField]
        protected AudioClip soundBeforeBoom;

        SteeringAgent steerAgent = null;
        protected bool hided = false;
        protected float timerForHide;


        private void MakeIfHided()
        {
            CanIDamagePlayer();
            if (boomHere)
            {
                audioManager.Play3DAudio(soundBeforeBoom, transform.position);
                return;
            }
            else
            {
                if (Time.time > timerForHide)
                {
                    //timerForHide = Time.time;
                    hided = false;
                    spriteRend.enabled = true;
                    col.enabled = true;
                    simpFollow.RestartFollow();
                    //simpFollow.enabled = true;
                    steerAgent.AddToAgentList();
                }
            }
        }


        private void MakeItIfVisible()
        {
            damageScale = Vector2.SqrMagnitude(simpFollow.GetDestination() - rig2D.position);
            if (damageScale < hideDistRange)
            {
                spriteRend.enabled = false;
                col.enabled = false;
                hided = true;
                simpFollow.StopFollow();
                //simpFollow.enabled = false;
                steerAgent.RemoveFromAgentList();
                timerForHide = Time.time + hidedTime;
            }
        }


        protected override void Init()
        {
            base.Init();
            hideDistRange *= hideDistRange;
            steerAgent = GetComponent<SteeringAgent>();
        }


        protected override void Reset()
        {
            base.Reset();
            hided = false;
            timerForHide = 0;
            col.enabled = true;
            spriteRend.enabled = true;
            //simpFollow.enabled = true;
            simpFollow.RestartFollow();
        }


        protected override void CheckThings()
        {
            if (hided)
                MakeIfHided();
            else
                MakeItIfVisible();
        }
    }
}
