﻿using UnityEngine;

namespace STDS
{
    public class MeleAttack : MonoBehaviour
    {
        public int damage = 10;
        public float damageBreak = 0.5f;

        private PlayerLiveManager plMan;
        private AllyHouseHelthManager allyHouseMan = null;
        private float timer;
        private bool houseManagerExist = false;


        public void SetNoHouseAttack()
        {
            allyHouseMan = null;
        }


        private void Start()
        {
            plMan = PlayerLiveManager.instance;
            if (AllyHouseHelthManager.instance != null)
            {
                allyHouseMan = AllyHouseHelthManager.instance.GetInstance;
                if (allyHouseMan != null)
                    houseManagerExist = true;
                else
                    houseManagerExist = false;
            }
            else
                houseManagerExist = false;
        }


        private void OnCollisionStay2D(Collision2D coll)
        { 
            if (timer < Time.time)
            {
                if (plMan.ThisPlayer(coll.collider))
                {
                    plMan.DamagePlayer(damage);
                    timer = Time.time + damageBreak;
                }
                else
                {
                    if (houseManagerExist)
                        if (allyHouseMan.DamageHouse(coll.collider))
                            timer = Time.time + damageBreak;
                }
            }
        }


        public void ProgressDamage(int amount)
        {
            damage += amount;
        }
    }
}