﻿using UnityEngine;

namespace STDS
{
    public class EnemyShoot : PlayerShoot
    {
        [SerializeField]
        SimpleFollow simpFollow = null;
        [SerializeField]
        float shootRange = 20f;
        [SerializeField]
        bool smartShoot = false;

        Vector2 dir;
        Bullet[] bulletB;
        bool init = false;
        Rigidbody2D rig;


        private void Start()
        {
            bulletB = new Bullet[bullet.Count];
            for(int i = 0; i<bullet.Count; i++)
            {
                bulletB[i] = bullet[i].GetComponent<Bullet>();
            }
            init = true;
            rig = GetComponent<Rigidbody2D>();
            shootRange *= shootRange;
            if (init)
                for (int i = 0; i < bulletB.Length; i++)
                {
                    bulletB[i].SetuDamage(GetBulletDamage());
                }
        }


        private void OnEnable()
        {
            timer = 0.5f * reloadTime ;
        }


        private bool InRange(Vector2 pos)
        {
            if (Vector2.SqrMagnitude(pos - rig.position) < shootRange)
                return true;
            return false;
        }


        protected override void Update()
        {        
            timer += Time.deltaTime;

            if (timer >= reloadTime)
            if (InRange(simpFollow.GetDestination()))
            {
                if (simpFollow.ISeePlayerOnFront() && simpFollow.ClearViewCheck())
                {
                        if (smartShoot)
                            dir = ((simpFollow.GetDestination() - SimpleFollowManager.instance.GetMoveVectorFromTwoLast()*6f) - rig.position).normalized;
                        else
                            dir = simpFollow.GetPlayerDirectionNormalized();

                        if (bulletForShoot > 1)
                        {
                            Shoot(Quaternion.Euler(0, 0, Mathf.Atan2((dir.y), (dir.x)) * Mathf.Rad2Deg - 90), 5f);
                        }
                        else
                        {
                            Shoot(Quaternion.Euler(0, 0, Mathf.Atan2((dir.y), (dir.x)) * Mathf.Rad2Deg - 90));
                        }                    
                }
            }
        }


        public override void ProgressBulletsDamage(int dam)
        {
            for (int i = 0; i < bulletB.Length; i++)
                bulletB[i].AddDamage(dam);
        }
    }
}
