﻿using UnityEngine;
using System.Collections;
//using EZCameraShake;

namespace STDS
{
    public class DamagerSetter
    {
        private float bigDamageRange, mediumDamageRange;
        private int bigDamage, medimDamage, lowDamage;
        private float squareRange;

        private const float medium = 0.6f, big = 0.3f;

        public DamagerSetter(int damage, float sqrRange)
        {
            //squareRange = sqrRange;
            //bigDamageRange = squareRange * 0.2f;
            //mediumDamageRange = squareRange * 0.5f;
            //lowDamageRange = squareRange;

            //bigDamage = damage;
            //medimDamage = (int) (damage* 0.5f);
            //lowDamage = (int) (damage* 0.2f);
            SetupDamage(damage, sqrRange);
        }

        public void SetupDamage(int dam, float rang)
        {
            squareRange = rang;
            bigDamageRange = squareRange * big;
            mediumDamageRange = squareRange * 0.6f;
           // lowDamageRange = squareRange;

            bigDamage = dam;
            medimDamage = (int)(dam * medium);
            lowDamage = (int)(dam * big);

           // Debug.Log(dam + "   " + medimDamage + "    " + lowDamage);
        }

        public int DamageChose(float range)
        {
            if (range > squareRange)
                return 0;
           // range = 1f - range / squareRange;
            if (range < bigDamageRange)
                return bigDamage;
            if (range < mediumDamageRange)
                return medimDamage;
            return lowDamage;
        }
    }


    public class MineAttack : MonoBehaviour
    {
        [SerializeField]
        protected int damage = 10;
        [SerializeField]
        protected float checkTime = 0.2f;
        [SerializeField]
        protected float range = 1f;
        [SerializeField]
        protected float timeToBoom = 0f;
        [SerializeField]
        protected float shake = 5f;

        protected SimpleFollow simpFollow;
        protected Rigidbody2D rig2D;
        protected float timer;
        protected PlayerLiveManager plMan;
        protected AllyHouseHelthManager allyHouseMan = null;
        protected Collider2D col;
        protected float damageScale;
        protected float squareRange;
        protected bool boomHere = false;
        protected float boomTimer = 0;
        //protected AudioSource audiosource;
        protected AudioManager audioManager;
        protected float bumSize;
        protected WaitForSeconds wait;

        private DamagerSetter damSetter;
        //private bool live = true;

        private void OnEnable()
        {
            Reset();
           // live = true;
        }

        private void OnDisable()
        {
           // live = false;
        }


        private void Awake()
        {
            simpFollow = GetComponent<SimpleFollow>();
            rig2D = GetComponent<Rigidbody2D>();
            col = GetComponent<Collider2D>();
            squareRange = range * range;
            bumSize = range / 2;
            audioManager = AudioManager.instance;
            //audiosource = GetComponent<AudioSource>();
            plMan = PlayerLiveManager.instance;
            if (AllyHouseHelthManager.instance != null)
                allyHouseMan = AllyHouseHelthManager.instance.GetInstance;
            damSetter = new DamagerSetter(damage, squareRange);
            Init();
        }


        private void FixedUpdate()
        {
            if (boomHere)
            {
                if (Time.time > timer)
                {
                    //if(live)
                    Damage();
                    //boomHere = false;
                }
            }
            else
            {
                
               // timer += Time.fixedDeltaTime;
                if (Time.time > checkTime)
                {
                    CheckThings();
                    timer = Time.time + checkTime;
                }
            }
        }



        private void Damage()
        {
           // damageScale = Vector2.SqrMagnitude(simpFollow.GetDestination() - rig2D.position);

            damageScale = Vector2.SqrMagnitude(plMan.PlayerPosition() - rig2D.position);
            if (damageScale < squareRange)
            {
                plMan.DamagePlayer(damSetter.DamageChose(damageScale));
            }

            EnemyManager.instance.DamageInstanceId(col.transform.GetInstanceID(), 1000000000);
            ExplosionManager.instance.Explode(transform.position, bumSize);         
        }





        //private void DamageAfter()
        //{
        //    damageScale = Vector2.SqrMagnitude(plMan.PlayerPosition() - rig2D.position);

        //    if (damageScale >= squareRange)
        //        damageScale = 0;
        //    Damage();
        //}


        protected virtual void Reset()
        {
            boomHere = false;
            timer = 0;
        }


        protected virtual void Init()
        {
            wait = new WaitForSeconds(timeToBoom);
        }


        //protected IEnumerator Boom()
        //{
        //    yield return wait;
        //    if(live)
        //    Damage();
        //}


        protected void CanIDamagePlayer()
        {
            damageScale = Vector2.SqrMagnitude(simpFollow.GetDestination() - rig2D.position);
            if (damageScale < squareRange)
            {
                boomHere = true;
                timer = Time.time + timeToBoom;
            }
        }
  

        protected virtual void CheckThings()
        {
            CanIDamagePlayer();
        }
    }
}
