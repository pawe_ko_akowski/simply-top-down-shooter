﻿using UnityEngine;

namespace STDS
{
    public class CameraSimpleFollow : MonoBehaviour
    {
        [SerializeField]
        private Transform lookAtTransform = null;
        [SerializeField]
        private float speed = 5f;

        private Vector3 lookPos;


        private void Start()
        {
            transform.parent = null;
            lookPos.z = transform.position.z;
        }


        private void Update()
        {
            lookPos.x = lookAtTransform.position.x;
            lookPos.y = lookAtTransform.position.y;
            transform.position = Vector3.Lerp(transform.position, lookPos, Time.deltaTime * speed);
        }
    }
}
