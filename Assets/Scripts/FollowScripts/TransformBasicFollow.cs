﻿using UnityEngine;

namespace STDS
{
    public class TransformBasicFollow : MonoBehaviour
    {
        [SerializeField]
        private bool rotationFollow = true;
        [SerializeField]
        private Transform followTransform = null;


        private void Start()
        {
            transform.parent = null;
        }


        private void Update()
        {
            transform.position = followTransform.position;
            if (rotationFollow)
                transform.rotation = followTransform.rotation;
        }
    }
}
