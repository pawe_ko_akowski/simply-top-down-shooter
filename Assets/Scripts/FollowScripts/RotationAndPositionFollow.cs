﻿using UnityEngine;

namespace STDS
{
    public class RotationAndPositionFollow : MonoBehaviour
    {
        [SerializeField]
        private Transform lookAtTransform = null;
        [SerializeField]
        private float speed = 5f;
        [SerializeField]
        private float rotSpeed = 5f;

        Vector3 lookPos;


        private void Start()
        {
            lookPos.z = transform.position.z;
        }


        private void Update()
        {
            lookPos.x = lookAtTransform.position.x;
            lookPos.y = lookAtTransform.position.y;
            transform.position = Vector3.Lerp(transform.position, lookPos, Time.deltaTime * speed);
            transform.rotation = Quaternion.Lerp(transform.rotation, lookAtTransform.rotation, Time.deltaTime * rotSpeed);
        }
    }
}
