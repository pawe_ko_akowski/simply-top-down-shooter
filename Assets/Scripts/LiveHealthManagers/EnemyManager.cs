﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class StdsBullet
    {
        public int bulletId;
        public int damagedObiectId;
        public int bulletDamage;

        public void FillStdsBullet(int bulletID, int damagedObiectID, int bulletDAmage)
        {
            bulletId = bulletID;
            damagedObiectId = damagedObiectID;
            bulletDamage = bulletDAmage;
        }

        public void FillStdsBullet( int damagedObiectID)
        {
            damagedObiectId = damagedObiectID;
        }

        public StdsBullet(int bulletID, int bulletDAmage)
        {
            bulletId = bulletID;
            bulletDamage = bulletDAmage;
        }
    }

    public class EnemyManager : MonoBehaviour
    {
        List<Helth> EnemyHelthList = new List<Helth>();
        public static EnemyManager instance = null;

       // int lastBuletId = 0;


        void Awake()
        {
            if (instance != null)
                Destroy(this);
            instance = this;
        }


        public void RegisterHealth(Helth he)
        {
            if(!EnemyHelthList.Contains(he))
            EnemyHelthList.Add(he);
          //  Debug.Log("Add " + he.GetInstanceID());
        }


        public void UnRegister(Helth he)
        {
            EnemyHelthList.Remove(he);
           // Debug.Log("Remove " + he.GetInstanceID());

        }


        public bool DamageInstanceId(int id, int dam)
        {
            for (int i = 0; i < EnemyHelthList.Count; i++)
            {
                if (EnemyHelthList[i].transform.GetInstanceID() == id)
                {
                    if (dam > 0)
                    {
                        //Debug.Log(dam);
                        EnemyHelthList[i].Damage(dam);
                        return true;
                    }
                    else
                    {
                        EnemyHelthList[i].Heal(-dam);
                        return false;
                    }
                }
            }
            return false;
        }


        public bool DamageInstanceId(StdsBullet bullet)
        {
           // Debug.Log("dam");
          //  if (bullet.bulletId == lastBuletId)
              //  return false;
           // Debug.Log("dam222");
           // lastBuletId = bullet.bulletId;

            for (int i = 0; i < EnemyHelthList.Count; i++)
            {
                if (EnemyHelthList[i].transform.GetInstanceID() == bullet.damagedObiectId)
                {
                   // Debug.Log("Damage");
                    EnemyHelthList[i].Damage(bullet.bulletDamage);
                    return true;
                }
            }
            return false;
        }
    }
}
