﻿using UnityEngine;

namespace STDS
{
    public class AllyHouseHelthManager : MonoBehaviour
    {
        class House
        {
            public Collider2D col;
            public GameObject gob;
            public Helth health;

            public House(Collider2D co, Helth he)
            {
                health = he;
                col = co;
            }

            public House(GameObject gobb, Collider2D co, Helth he)
            {
                gob = gobb;
                health = he;
                col = co;
            }
        }

        public static AllyHouseHelthManager instance = null;

        [SerializeField]
        int standardDamage = 1;
        [SerializeField]
        bool victoryAfterAllDestroy = false;
        [SerializeField]
        bool houseEndEndGame = true;
        

        int allHousesAmount;
        int savedDamage;
        House[] houses;

        public int GetAllHousesAmount
        {
            get
            {
                return allHousesAmount;
            }
        }

        void Awake()
        {
            HouseHealth[] houHel = FindObjectsOfType<HouseHealth>();

            houses = new House[houHel.Length];
            for (int i = 0; i < houses.Length; i++)
            {
                houses[i] = new House(houHel[i].gameObject, houHel[i].GetComponent<Collider2D>(), houHel[i]);
            }

            allHousesAmount = houses.Length;
            savedDamage = standardDamage;
            instance = this;
        }


        public void DamageHouses(bool did)
        {
            if (did)
                standardDamage = savedDamage;
            else
                standardDamage = 0;
        }


        public void IAmDedHouse()
        {
            allHousesAmount--;

            if(houseEndEndGame)
            if (allHousesAmount <= 0)
            {
                if (!victoryAfterAllDestroy)
                    GameManager.instance.EndGameFinaly();
                else
                    GameManager.instance.SuccesEndGame();
            }
        }


        public bool DamageHouse(Collider2D coll)
        {
            if (standardDamage > 0)
            {
                for (int i = 0; i < houses.Length; i++)
                {
                    if (houses[i].col == coll)
                    {
                        Damage(standardDamage, i);
                            //houses[i].health.Damage(standardDamage);
                           // Debug.Log("Damage");
                            return true;
                    }
                }
            }
            return false;
        }


        public bool DamageHouse(Collider2D coll, int amount)
        {
            for (int i = 0; i < houses.Length; i++)
            {
                if (houses[i].col == coll)
                {
                    Damage(amount, i);
                    //houses[i].health.Damage(amount);
                    return true;
                }
            }
            return false;
        }


        public bool DamageHouse(GameObject gob)
        {
            if (standardDamage > 0)
            {
                for (int i = 0; i < houses.Length; i++)
                {
                    if (houses[i].gob == gob)
                    {
                        Damage(standardDamage, i);
                       // houses[i].health.Damage(standardDamage);
                        //Debug.Log("Damage");
                        return true;
                    }
                }
            }
            return false;
        }

        private void Damage(int dam, int element)
        {
            houses[element].health.Damage(dam);
            //Debug.Log("obrywa dom");
        }


        public bool DamageHouse(GameObject gob, int amount)
        {
            for (int i = 0; i < houses.Length; i++)
            {
                if (houses[i].gob == gob)
                {
                    Damage(amount, i);
                    //houses[i].health.Damage(amount);
                    return true;
                }
            }
            return false;
        }


        public AllyHouseHelthManager GetInstance
        {
            get
            {
                if (victoryAfterAllDestroy)
                    return null;

                return instance;
            }
        }
    }
}