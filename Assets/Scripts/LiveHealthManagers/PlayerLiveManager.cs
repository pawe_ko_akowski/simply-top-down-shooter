﻿using UnityEngine;

namespace STDS
{
    public class PlayerLiveManager : MonoBehaviour
    {
        public static PlayerLiveManager instance = null;

        Collider2D playerCollider;
        GameObject playerGob;
        Helth playerHelth;
        bool playerHealthExist = false;
        Rigidbody2D rig;

        public void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }

        public void RegisterPlayerHelth(Helth hel)
        {
            playerHelth = hel;
            playerHealthExist = true;
            playerCollider = playerHelth.gameObject.GetComponent<Collider2D>();
            playerGob = playerHelth.gameObject;
            rig = playerHelth.gameObject.GetComponent<Rigidbody2D>();
        }


        public Vector2 PlayerPosition()
        {
            return rig.position;
        }

        public void BreakRegister()
        {
            playerHealthExist = true;
        }

        public void UnregisterPlayerHelth()
        {
            playerHealthExist = false;
        }



        public bool ThisPlayer(Collider2D col)
        {
            if (col == playerCollider)
                return true;

            return false;
        }

        public bool ThisPlayer(GameObject gob)
        {
            if (gob == playerGob)
                return true;

            return false;
        }


        public void DamagePlayer(int amount)
        {
            if(playerHealthExist)
            playerHelth.Damage(amount);
        }
    }
}
