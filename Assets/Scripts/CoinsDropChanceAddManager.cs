﻿using UnityEngine;

namespace STDS
{
    public class CoinsDropChanceAddManager : MonoBehaviour
    {
        public static CoinsDropChanceAddManager instance = null;

        float chanceForCoinAdd = 0;


        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        public float GetChanceAdd
        {
            get
            {
                return chanceForCoinAdd;
            }
        }


        public void SetCoinChanceAdd(float chance)
        {
            chanceForCoinAdd = chance;
        }
    }
}
