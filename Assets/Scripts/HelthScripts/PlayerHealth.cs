﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class PlayerHealth : Helth
    {
        public AudioClip regenerationAudioClip;
        public Text liveText = null;
        public Image liveImage = null;
        public int liveWhenHeartBeat = 30;
        public GameObject visibleGob = null;
        public GameObject[] gobToHide = null;
        public int liverRegenerationAmount = 2;
        public Image playerDamageImage;
        public float heartBeatTime = 6f;
        public AudioSource audiosource = null;

        private Color colorek;
        private bool imagePain = false;
        private float imagePainDuration = 3f;
        private float imagePainTimerek = 0;
        private float imagePainSet = 0;
        private float heartBeatTimerr = 0;
        private Animator liveInfoAnimator = null;   
        private int liveRegeneration = 0;
        private float liveRegenerationTime = 0;
        private bool pain = false;
        private int lastHealth;

        delegate void LiveReg();
        private LiveReg liveRe;


        private void Start()
        {
            colorek = playerDamageImage.color;
            colorek.a = 0;
            playerDamageImage.color = colorek;
            PlayerLiveManager.instance.RegisterPlayerHelth(this);
            liveInfoAnimator = liveText.GetComponentInParent<Animator>();
            TextInfo();
           // wait = new WaitForSeconds(heartBeatTime);
        }


        private void Update()
        {
            liveRe();
            if (pain)
            {
                heartBeatTimerr += Time.deltaTime;
                if (heartBeatTimerr > heartBeatTime)
                {
                    pain = false;
                    audiosource.loop = pain;
                    heartBeatTimerr = 0;
                }
            }

            if (imagePain)
            {
                float what = (1 - imagePainTimerek / imagePainDuration);
                if (what < 0)
                {
                    what = 0;
                    imagePain = false;                   
                }

                imagePainSet *= what;
                colorek.a = imagePainSet;
                playerDamageImage.color = colorek;
                imagePainTimerek += Time.deltaTime;
            }
        }


        protected override void OnEnableeThings()
        {
            activeHelth = lastHealth;
            if (pain)
                audiosource.Play();
            PlayerLiveManager.instance.BreakRegister();
        }


        protected override void OnDisableThings()
        {
            base.OnDisableThings();
            PlayerLiveManager.instance.UnregisterPlayerHelth();
            lastHealth = activeHelth;
        }


        private void LiveRegeneration()
        {
            liveRegenerationTime -= Time.deltaTime;
            if (liveRegenerationTime < 0)
            {
                liveRegenerationTime = liveRegeneration + liveRegenerationTime;               
                HealthIncrease();
            }
        }


        private void NoRegeneration()
        {

        }


        private void HealthIncrease()
        {
            if (activeHelth < healthAmount)
            {
                audioMan.Play2DAudio(regenerationAudioClip, 0.5f);
                activeHelth+= liverRegenerationAmount;
                lastHealth = activeHelth;
                LiveMoreCheck();
                liveText.text = activeHelth.ToString();
                FillLiveImage();
                if (pain && activeHelth >= liveWhenHeartBeat)
                {
                    pain = false;
                    audiosource.pitch = 1;
                    audiosource.loop = false;
                    liveInfoAnimator.speed = 1;
                }
            }
        }


        private void FillLiveImage()
        {
            liveImage.fillAmount = ((float)activeHelth / healthAmount);
        }


        private void LiveMoreCheck()
        {
            if (activeHelth > healthAmount)
                activeHelth = healthAmount;
        }


        private void SetupPainAnimatorAndAudio()
        {
            pain = true;
            audiosource.loop = pain;
            if (!audiosource.isPlaying)
                audiosource.Play();
        }


        private void PainEffectActivator()
        {
            if (activeHelth < liveWhenHeartBeat)
            {
                float some = (float)(activeHelth) / (float)(liveWhenHeartBeat);              
                audiosource.pitch = 1f + (1f - some) * 0.3f;
                liveInfoAnimator.speed = audiosource.pitch;
                SetupPainAnimatorAndAudio();
                heartBeatTimerr = 0;
            }
            else
            {
                if (pain)
                {
                    pain = false;
                    audiosource.loop = pain;
                }
                pain = false;
            }
        }


        private void TextInfo()
        {
            PainEffectActivator();
            liveText.text = activeHelth.ToString();
            FillLiveImage();
        }
  

        protected override void MakeThingsBeforeKill()
        {
            visibleGob.SetActive(false);
            for (int i = 0; i < gobToHide.Length; i++)
                gobToHide[i].SetActive(false);
            PlayerLiveManager.instance.UnregisterPlayerHelth();
        }
        

        protected override void MakeThingsAfterKill()
        {         
            audiosource.Stop();
            GameManager.instance.EndGameFinaly();
            base.MakeThingsAfterKill();
        }


        protected override void AddInfo()
        {
            base.AddInfo();
            if (lastHealth > activeHelth)
            {
                imagePain = true;
                if (activeHelth > 0)
                {
                    float what = (lastHealth - activeHelth) / (float)activeHelth;
                    if (what > 0.2f)
                        what = 0.2f;
                    imagePainSet += what;
                    if (imagePainSet > 0.5f)
                        imagePainSet = 0.5f;
                }
                else
                    imagePainSet = 0;
                colorek.a = imagePainSet; 
                playerDamageImage.color = colorek;
                imagePainTimerek = 0;
            }

            lastHealth = activeHelth;
            TextInfo();
        }


        public void AssignHealthAmount(int amount)
        {
            healthAmount = amount;
            lastHealth = healthAmount;
        }


        public void AssignLiveRegeneration(int amount)
        {
            if (amount > 0)
            {
                liveRe = LiveRegeneration;
            }
            else
                liveRe = NoRegeneration;
            liveRegeneration = 11 - amount;
            liveRegenerationTime = liveRegeneration;
        }


        public void SetupArmor(int amount)
        {
            armor = amount;
        }
    }
}
