﻿using UnityEngine;
using UnityEngine.UI;
using STDS.Steer2D;

namespace STDS
{
    public class BossHelth : EnemyHealth
    {
        //dupa
       // [SerializeField]
       // private int moneyAmount = 5;
        [SerializeField]
        private Text liveText = null;
        [SerializeField]
        private BossRigidBodySeek bossRigSeek = null;
        [SerializeField]
        private int livePercentToKeep = 10;
        [SerializeField]
        private BossMonsterSpawner[] bossMonsterSpawner = null;
        [SerializeField]
        private BossShootSetter bossShootSetter = null;

        private BossFightHouseDamageActivator bossFighHouseDamageActivator = null;


        private void SendLiveInfoToAllMonsterSpanwers()
        {
            for (int i = 0; i < bossMonsterSpawner.Length; i++)
            {
                bossMonsterSpawner[i].DataInfo(activeHelth);
            }
        }


        protected override void AddInfo()
        {   
            if (activeHelth < (healthAmount / 2))
            {
                bossFighHouseDamageActivator.Activate(true);

                bossRigSeek.Rescue(true);
                if (activeHelth == 0)
                    liveText.gameObject.SetActive(false);

                if (BossFightHouseCounter.instance.StilLive)
                if (activeHelth < (healthAmount /livePercentToKeep))
                {
                        Debug.Log(healthAmount / livePercentToKeep);
                    activeHelth = (healthAmount / livePercentToKeep);
                }
            }
            else
            {
                if (activeHelth >= healthAmount)
                {
                    bossRigSeek.Rescue(false);
                }
            }
            SendLiveInfoToAllMonsterSpanwers();
            bossShootSetter.SetDataInfo(activeHelth);
            liveText.text = activeHelth.ToString();
        }


        protected override void StartInit()
        {
            bossFighHouseDamageActivator = BossFightHouseDamageActivator.instance;
            bossFighHouseDamageActivator.SetBossCollider(col2D);
        }

        //public override void Damage(int amount)
        //{
        //    if(bossFighHouseDamageActivator.
        //    base.Damage(amount);
        //}
    }
}
