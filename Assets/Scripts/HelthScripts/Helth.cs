﻿using UnityEngine;

namespace STDS
{
    public class Helth : MonoBehaviour
    {
       // private class AudiosourceObiect
       // {
           // public Transform tr;
        //    public AudioSource audioSource;
       // }

        //[SerializeField]
        //private GameObject audiosourcePrefab = null;
        [SerializeField]
        private AudioClip damageClip = null, deadClip = null;
        [SerializeField]
        protected int healthAmount = 10;
        [SerializeField]
        protected int armor = 0;
        [SerializeField]
        MonoBehaviour[] scriptsToDisabled = null;
      //  [SerializeField]
       // float waitToHide = 1.4f;

        private int damage;
       // private AudiosourceObiect aOb;
        protected Collider2D col2D;
        protected int activeHelth, activeArmor;
        protected AudioSource audioSource;
        
        public SpriteRenderer sprRend;
       // protected WaitForSeconds wait;

        protected AudioManager audioMan;
        //Vector3 posFoEnd = new Vector3(1000, 1000, 1000);

        private void Awake()
        {
            //wait = new WaitForSeconds(waitToHide);
            col2D = GetComponent<Collider2D>();
           // GameObject ob = Instantiate(audiosourcePrefab);
            //ob.transform.position = transform.position;
            //ob.SetActive(true);
          //  aOb = new AudiosourceObiect();
          //  aOb.tr = ob.transform;
           // aOb.audioSource = ob.GetComponent<AudioSource>();
            audioSource = GetComponent<AudioSource>();
            if(sprRend == null)
            sprRend = GetComponent<SpriteRenderer>();
            InAwake();
            audioMan = AudioManager.instance;
        }


        protected virtual void InAwake()
        {

        }

        private void OnEnable()
        {
            activeArmor = armor;
            activeHelth = healthAmount ;
            EnemyManager.instance.RegisterHealth(this);
            col2D.enabled = true;
            if (sprRend != null)
            {
                sprRend.transform.position = transform.position;
                sprRend.enabled = true;
            }
            for (int i = 0; i < scriptsToDisabled.Length; i++)
                scriptsToDisabled[i].enabled = true;

            OnEnableeThings();
        }


        private void OnDisable()
        {
            if(sprRend)
            sprRend.enabled = false;
            //sprRend.transform.position = posFoEnd;
            OnDisableThings();
        }

        protected virtual void OnEnableeThings()
        {
            //EnemyManager.instance.UnRegister(this);
        }


        protected virtual void OnDisableThings()
        {
            EnemyManager.instance.UnRegister(this);
        }


        //protected void ToogleAudiosourceMode()
        //{
        //    if (aOb.audioSource.spatialBlend > 0)
        //        aOb.audioSource.spatialBlend = 0;
        //    else
        //        aOb.audioSource.spatialBlend = 1;
        //}


        //protected void AudiosourceTo2DMode()
        //{
        //    aOb.audioSource.spatialBlend = 0;
        //}


        protected virtual void AddInfo()
        {

        }

        //private IEnumerator ShutDown()
        //{
        //    MakeThingsBeforeKill();
        //    yield return wait;
        //    MakeThingsAfterKill();
        //}

        protected virtual void MakeThingsBeforeKill()
        {
            for (int i = 0; i < scriptsToDisabled.Length; i++)
                scriptsToDisabled[i].enabled = false;
            sprRend.enabled = false;
            col2D.enabled = false;
        }

        protected virtual void MakeThingsAfterKill()
        {
            transform.parent.gameObject.SetActive(false);
        }


        public void Heal(int amount)
        {
            activeHelth += amount;
            if (activeHelth > healthAmount)
                activeHelth = healthAmount;
            AddInfo();
        }


        public void Damage(int amount)
        {

            if (activeHelth <= 0)
                return;
           // Debug.Log(activeHelth);

           // Debug.Log(amount);
            damage = Mathf.Max(amount - activeArmor, 1);
            activeHelth -= damage;
           // Debug.Log(this.GetInstanceID() + "   " +damage);
            if (damage > 0)
            {
                if (activeHelth <= 0)
                {
                    activeHelth = 0;
                    col2D.enabled = false;

                    // aOb.tr.position = transform.position;
                    //aOb.audioSource.PlayOneShot(deadClip);
                    //audioSource.PlayOneShot(deadClip);
                    audioMan.Play3DAudio(deadClip, transform.position);
                    AddInfo();
                  
                    MakeThingsBeforeKill();
                    MakeThingsAfterKill();
                    return;
                }
                AddInfo();
                //audioSource.PlayOneShot(damageClip);
                audioMan.Play3DAudio(damageClip, transform.position);
                //aOb.tr.position = transform.position;
                // aOb.audioSource.PlayOneShot(damageClip);
            }
        }


        public int GetStartHelth()
        {
            return healthAmount;
        }


        public int GetStartArmor()
        {
            return armor;
        }


        public void ProgressHelth(int healthAdd)
        {
            // activeHelth += healthAdd;
            healthAmount += healthAdd;
        }


        public void ProgressArmor(int armorAdd)
        {
            //activeArmor += armorAdd;
            armor += armorAdd;
        }


        public int GetActiveHelth
        {
            get
            {
                return activeHelth;
            }
        }
    }
}
