﻿using UnityEngine;

namespace STDS
{
    public class HouseHealth : Helth
    {
       // private MapMarker mapMarker = null;
       // private int damageFromPlayer;


        private void Start()
        {
//mapMarker = GetComponent<MapMarker>();
            //damageFromPlayer = PlayerDamageManager.instance.GetPlayerDamage;
            audioMan = AudioManager.instance;
            //Debug.Log(audioMan);
        }


        private void OnEnable()
        {
           // if (mapMarker != null)
              //  mapMarker.show();

            activeHelth = healthAmount;
        }


        protected void OnParticleCollision(GameObject other)
        {
            //Debug.Log("Particle");
            AllyHouseHelthManager.instance.DamageHouse(gameObject);
        }


        protected override void MakeThingsAfterKill()
        {
            //if(mapMarker != null)
            //mapMarker.hide();   
            AllyHouseHelthManager.instance.IAmDedHouse();
            gameObject.SetActive(false);
        }

        //protected override void AddInfo()
        //{
        //    base.AddInfo();
        //    //Debug.Log(activeHelth);
        //}
    }
}