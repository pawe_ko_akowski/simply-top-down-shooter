﻿using UnityEngine;
using STDS.Steer2D;

namespace STDS
{
    public class EnemyHealth : Helth
    {
        public float moneyChance = 0.25f;
        public int moneyType = 0;
        public int killXP = 10;
        public SplashParticleEmiter splashEmit;

        private bool levelMonsterSpawnerEnemy = true;
        private SteeringAgent steerAgent;
        private LevelMonsterSpawner monsterSpawner;
        private int damageFromPlayer;


        protected void OnParticleCollision(GameObject other)
        {
            Damage(damageFromPlayer);
        }

        void Start()
        {
            monsterSpawner = LevelMonsterSpawner.instance;
            damageFromPlayer = PlayerDamageManager.instance.GetPlayerDamage;
            StartInit();
        }


        protected override void InAwake()
        {
            steerAgent = GetComponent<SteeringAgent>();
        }


        protected virtual void StartInit()
        {

        }


        public void DontCountThisEnemy()
        {
            monsterSpawner = null;
            levelMonsterSpawnerEnemy = false;
        }


        protected override void OnEnableeThings()
        {
            steerAgent.enabled = true;
        }


        //protected override void OnDisableThings()
        //{
        //    base.OnDisableThings();
        //}


        protected override void MakeThingsAfterKill()
        {
            if(levelMonsterSpawnerEnemy)
            monsterSpawner.DestroyMonster();
            gameObject.SetActive(false);        
        }

        protected override void MakeThingsBeforeKill()
        {
            base.MakeThingsBeforeKill();
            splashEmit.EmitParticle();
            steerAgent.enabled = false;
            float rand = Random.Range(0, 1f);
            if (rand < (moneyChance * CoinsDropChanceAddManager.instance.GetChanceAdd))
                CoinInstationerManager.instance.InstantiateCoin(transform.position, moneyType);
            CanvasManager.instance.UpdateXP(killXP);
        }
    }
}
