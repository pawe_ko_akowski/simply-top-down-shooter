﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class MobilePlayerGrenadeAndMineDrop : PlayerGrenadeAndMineDrop
    {
        [SerializeField]
        Button grenadeBtn = null, mineBtn = null;


        void Awake()
        {
            grenadeBtn.onClick.AddListener(GrenadeDrop);
            mineBtn.onClick.AddListener(MineDrop);
        }
    }
}
