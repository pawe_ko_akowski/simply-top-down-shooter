﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class PlayerGrenadeAndMineDrop : MonoBehaviour
    {
        public GameObject grenadePrefab = null, minePrefab = null;
        public float dropBreakTime = 0.5f;
        public Text grenadeText = null, mineText = null;
        public GameObject grenadeOb = null, mineOb = null;
        public Rigidbody2D thisRigid;

        private int maxMines;
        private int maxGrenades;
        private Transform[] grenadeTransform, mineTransform;
        private int actualMine = 0;
        private Rigidbody2D[] grenadeRigid;
        private int actualGrenade = 0;
        private float grenadeTime = 0, mineTime = 0;
        private float grenadeRangeAd, mineRangeAd;
        private int grenadeDamageAd, mineDamageAd;
        private int helpInt;
        private STDSdata data;


        void StartGrenadeAndMineInfo()
        {
            if (maxGrenades <= 0)
                grenadeOb.SetActive(false);
            else
                grenadeOb.SetActive(true);

            if (maxMines <= 0)
                mineOb.SetActive(false);
            else
                mineOb.SetActive(true);
        }


        public void SetupMineDamage(int damage, float range)
        {
            mineDamageAd = damage;
            mineRangeAd = range;
        }
        

        public void SetupGrenadeDamage(int damage, float range)
        {
            grenadeDamageAd = damage;
            grenadeRangeAd = range;
        }


        void Start()
        {
            if (StoreSTDSGameData.instance != null)
                data = StoreSTDSGameData.instance.stdsData;
            if (data != null)
            {
                maxMines = data.mines;
                maxGrenades = data.grenades;
            }
            else
            {
                maxMines = 0;
                maxGrenades = 0;
            }
            grenadeText.text = maxGrenades.ToString();
            mineText.text = maxMines.ToString();
            mineTransform = new Transform[maxMines];
            grenadeRigid = new Rigidbody2D[maxGrenades];
            if (maxMines > 0)
            for (int i = 0; i < maxMines; i++)
            {
                    mineTransform[i] = (Instantiate(minePrefab).transform);
                    mineTransform[i].GetComponent<BoomSetup>().SetupBoomSize(mineDamageAd, mineRangeAd);
                    mineTransform[i].gameObject.SetActive(false);
            }
            if(maxGrenades >0)
            for (int i = 0; i < maxGrenades; i++)
            {
                    grenadeRigid[i] = (Instantiate(grenadePrefab).GetComponent<Rigidbody2D>());
                    grenadeRigid[i].GetComponent<BoomSetup>().SetupBoomSize(grenadeDamageAd, grenadeRangeAd);
                    grenadeRigid[i].gameObject.SetActive(false);
            }
            SetTimersAfterDrop();
            StartGrenadeAndMineInfo();
        }


        void SetTimersAfterDrop()
        {
            grenadeTime = Time.time + dropBreakTime;
            mineTime = Time.time + dropBreakTime; ;
        }


        void GrenadeInfo()
        {
            helpInt = maxGrenades - actualGrenade;
            grenadeText.text = helpInt.ToString();
            if (helpInt <= 0)
             grenadeOb.SetActive(false);
        }

        void MineInfo()
        {
            helpInt = maxMines - actualMine;
            mineText.text = helpInt.ToString();

            if (helpInt <= 0)
                mineOb.SetActive(false);
        }

        protected void GrenadeDrop()
        {
           if(actualGrenade < maxGrenades)
            if (grenadeTime < Time.time )
            {
                grenadeRigid[actualGrenade].transform.position = thisRigid.position;
                grenadeRigid[actualGrenade].gameObject.SetActive(true);  
                grenadeRigid[actualGrenade].AddForce(thisRigid.transform.rotation * Vector2.up * 100);
                actualGrenade++;
                data.grenades--;
               // SetTimersAfterDrop();
                GrenadeInfo();
                grenadeTime = Time.time + dropBreakTime;

            }
        }


        protected void MineDrop()
        {
           if(actualMine < maxMines)
            if (mineTime <Time.time )
            {
                mineTransform[actualMine].position = transform.position;
                mineTransform[actualMine].gameObject.SetActive(true);
                actualMine++;
                data.mines--;
               // SetTimersAfterDrop();
                MineInfo();
                mineTime = Time.time + dropBreakTime;
            }
        }
    }
}
