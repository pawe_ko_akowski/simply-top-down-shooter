﻿using CnControls;

namespace STDS
{
    public class MobilePlayerMove : PlayerMove
    {
        private void FixedUpdate()
        {
            moveDirection.x = CnInputManager.GetAxisRaw("Horizontal");
            moveDirection.y = CnInputManager.GetAxisRaw("Vertical");
            rig2D.AddForce(moveDirection.normalized * moveSpeed);
        }
    }
}
