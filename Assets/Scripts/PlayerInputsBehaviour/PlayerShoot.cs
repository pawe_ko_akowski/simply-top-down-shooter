﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class PlayerShoot : MonoBehaviour
    {
        [SerializeField]
        GameObject shootPrefab = null;
        [SerializeField]
        AudioClip shootClip = null;
        [SerializeField]
        protected float reloadTime = 0.35f;
        [SerializeField]
        int defaultBullet = 10;
        [SerializeField]
        int bulletDamage = 10;
        [SerializeField]
        protected bool autoAim = false;

        //dupa coś z tą zmienną trzeba zrobić jakąś inną klasę pewnie
        [SerializeField]
        GameObject gobWithTrigger = null;
        [SerializeField]
        protected int bulletForShoot = 1;
        [SerializeField]
        protected float rotForBullet = 8f;


        protected float timer = 0;
        protected Vector3 mousePos, mousePosWorld, toSet;
        protected List<Transform> bullet = new List<Transform>();
       // protected float mathVariable;

        int index = 0;
        AudioSource audioSource = null;
        AutoAimScript autoAimBehave;
        Transform nearestEnemy = null;
        //AudioManager audioManager;


        void Awake()
        {
            
            //  mathVariable = Mathf.Rad2Deg - 90;
            for (int i = 0; i < defaultBullet; i++)
            {
                Transform tr = Instantiate(shootPrefab).transform;
                tr.GetComponent<Bullet>().SetuDamage(bulletDamage);
                tr.gameObject.SetActive(false);
                bullet.Add(tr);
            }
            //audioSource = GetComponent<AudioSource>();

            if (StoreSTDSGameData.instance == null)
                autoAim = false;
            else
                autoAim = StoreSTDSGameData.instance.stdsData.AutoAim;

            if (autoAim)
                autoAimBehave = gobWithTrigger.AddComponent<AutoAimScript>();

            //audioManager = AudioManager.instance;
        }


        protected void Shoot(Quaternion rot)
        {
            timer = 0;
            //bullet[index].gameObject.SetActive(false);

            RealShoot(rot);

            //audioManager.Play3DAudio(shootClip
            //audioSource.PlayOneShot(shootClip);
        }

        void RealShoot(Quaternion rot)
        {

            //bullet[index].gameObject.SetActive(false);
            bullet[index].position = transform.position;
            bullet[index].rotation = rot;
            bullet[index].gameObject.SetActive(true);

            index++;

            if (index >= bullet.Count)
                index = 0;
        }

        protected void Shoot(Quaternion rot, float rotChange2)
        {
            float firstRot = rot.eulerAngles.z;

            if (bulletForShoot > 1)
            {
                if (bulletForShoot % 2 > 0)
                {
                    firstRot = firstRot - (bulletForShoot - 1) / 2 * rotForBullet;
                }
                else
                {
                    firstRot = firstRot - (bulletForShoot) / 2 * rotForBullet;
                }

            }

            timer = 0;


            for (int i = 0; i < bulletForShoot; i++)
            {
                RealShoot(Quaternion.Euler(0, 0, firstRot + i * rotForBullet));
            }

            audioSource.PlayOneShot(shootClip);
        }

        protected void AutoAim()
        {
                nearestEnemy = autoAimBehave.GetNearestEnemy();
                if (nearestEnemy != null)
                {
                    mousePosWorld = nearestEnemy.position;
                    mousePosWorld.z = 0;
                    mousePos = transform.position;
                    mousePos.z = 0;
                // toSet.z = Mathf.Atan2((mousePosWorld.y - transform.position.y), (mousePosWorld.x - transform.position.x)) * Mathf.Rad2Deg - 90;
                // transform.eulerAngles = toSet;
            }
            
        }

        protected virtual void Update()
        {
            if(autoAim)
            AutoAim();

            if(nearestEnemy == null)
            {
                mousePos = Input.mousePosition;
                mousePos.z -= Camera.main.transform.position.z;
                mousePosWorld = Camera.main.ScreenToWorldPoint(mousePos);
            }

            toSet.z = Mathf.Atan2((mousePosWorld.y - transform.position.y), (mousePosWorld.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.eulerAngles = toSet;

            timer += Time.deltaTime;

            if (Input.GetButton("Fire1") && timer >= reloadTime)
            {
                Shoot(transform.rotation);
            }
        }

        //protected void AutoAim()
        //{
          


        //}


        public void SetDamage(int amount)
        {
            bulletDamage = amount;
           // shootPrefab.GetComponent<Bullet>().SetuDamage(amount);
          //  Debug.Log(amount + " obrazen");


        }


        public void SetFireRate(float rate)
        {
            reloadTime = rate;
        }



        public virtual void ProgressBulletsDamage(int dam)
        {

        }


        public int GetBulletDamage()
        {
            return bulletDamage;
        }

    }
}
