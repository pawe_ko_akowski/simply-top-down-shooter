﻿using UnityEngine;

namespace STDS
{
    public class PCPlayerGrenadeAndMineDrop : PlayerGrenadeAndMineDrop
    {
        [SerializeField]
        string grenadeDropString = "Grenade", mineDropString = "Mine";


        void Update()
        {
            if (Input.GetButton(grenadeDropString))
            {
                GrenadeDrop();
            }
            if (Input.GetButton(mineDropString))
            {
                MineDrop();
            }
        }
    }
}
