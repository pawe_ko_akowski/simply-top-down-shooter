﻿using UnityEngine;

namespace STDS
{
    public class PlayerMove : MonoBehaviour
    {
        public float moveSpeed = 2f;
        protected Rigidbody2D rig2D;
        protected Vector2 moveDirection;
 
        void Start()
        {
            rig2D = GetComponent<Rigidbody2D>();
        }

        public void SetMovementSpeed(float amount)
        {
            moveSpeed *= amount;
        }

        private void FixedUpdate()
        {
            moveDirection.x = Input.GetAxisRaw("Horizontal");
            moveDirection.y = Input.GetAxisRaw("Vertical");
            rig2D.AddForce(moveDirection.normalized * moveSpeed);
        }
    }
}
