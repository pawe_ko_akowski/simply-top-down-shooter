﻿using UnityEngine;

namespace STDS
{
    public class PadRotation : MonoBehaviour
    {
        public Rigidbody2D rig;
        public float timeToRot = 0.2f;
        public float angle = 90;

        private Vector2 oldRotation, lastRot, last;
        private float timer = 0;


        void Start()
        {
            oldRotation = Vector2.zero;
            timeToRot = 1 / timeToRot;
        }


        public void PadMotion()
        {
            Vector2 rot;
            rot.x = Input.GetAxis("RotX");
            rot.y = Input.GetAxis("RotY");
            rot = rot.normalized;

            if (rot != Vector2.zero)
            {
                if (rot != last)
                {
                    oldRotation = lastRot;
                    timer = 0;
                }

                last = rot;

                timer += Time.deltaTime * timeToRot;
                if (timer <= 1)
                    lastRot = Vector2.Lerp(oldRotation, rot, timer);
                rig.rotation = (Mathf.Atan2(lastRot.y, -lastRot.x) * Mathf.Rad2Deg + angle);
            }
            else
            {
                oldRotation = lastRot;
                timer = 0;
            }
        }
    }
}
