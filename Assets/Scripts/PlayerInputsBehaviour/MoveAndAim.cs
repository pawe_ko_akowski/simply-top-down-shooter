﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MoveAndAim : MonoBehaviour {
    Rigidbody2D rig;
    public float speed = 20;
    public float timeToRot = 0.2f;
    Vector2 oldRotation, lastRot, last;
    float timer = 0;
    //bool set = true;

    // Use this for initialization
    void Start () {
        rig = GetComponent<Rigidbody2D>();
        oldRotation = Vector2.zero;
        timeToRot = 1 / timeToRot;


   
        //string[] str = Input.GetJoystickNames();

       
        //foreach (string s in str)
        //{
        //    Debug.Log(s + "\\n");

        //    }
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 move;
        move.y = Input.GetAxisRaw("Vertical");
        move.x = Input.GetAxisRaw("Horizontal");

        rig.AddForce(move.normalized * speed * Time.deltaTime);


        Vector2 rot;
        rot.x = Input.GetAxis("RotX");
        rot.y = Input.GetAxis("RotY");
        rot = rot.normalized;

        // Debug.Log(rot);


       // oldRotation = transform.up;

        if (rot != Vector2.zero)
        {

            if (rot != last)
            {
                oldRotation = lastRot;
                timer = 0;
            }

            last = rot;

            timer += Time.deltaTime * timeToRot;
            //oldRotation = rot;
            if(timer <= 1)
            lastRot = Vector2.Lerp(oldRotation, rot, timer);



            rig.rotation = (Mathf.Atan2(lastRot.y, -lastRot.x) * Mathf.Rad2Deg);

        }
        else
        {
            oldRotation = lastRot;
            timer = 0;

        }

       // detectPressedKeyOrButton();
    }


    float FlowRot(float newRot)
    {
        if (Mathf.Abs(newRot - rig.rotation) < 180f)
            if (newRot > rig.rotation)
                return rig.rotation + (newRot - rig.rotation) * Time.deltaTime;
            else
                return rig.rotation + (rig.rotation - newRot) * Time.deltaTime;
        else
            return rig.rotation - rig.rotation* Time.deltaTime;
    }

    public void detectPressedKeyOrButton()
    {
        foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("KeyCode down: " + kcode);
        }
    }
}
