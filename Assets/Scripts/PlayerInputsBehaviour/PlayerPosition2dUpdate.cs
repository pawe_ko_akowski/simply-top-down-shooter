﻿using UnityEngine;

namespace STDS
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerPosition2dUpdate : MonoBehaviour
    {
        public float updatePositionFrequency = 0.3f;
        public Rigidbody2D rig2d;

        private SimpleFollowManager simpleFollowManager = null;
        private float lastTimer = 0;

        void Start()
        {
            simpleFollowManager = SimpleFollowManager.instance;
        }

        void Update()
        {
            if (Time.time > lastTimer)
            {
                lastTimer = Time.time + updatePositionFrequency;
                simpleFollowManager.UpdatePlayerPosition(rig2d.position);
            }
        }
    }
}
