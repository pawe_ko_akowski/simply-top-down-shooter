﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAimScript : MonoBehaviour {


    class enemyAim
    {
        public int id = 0;
        public Transform tr;
        public Collider2D col;

        public enemyAim(Transform t, Collider2D c)
        {
            tr = t;
            col = c;
            id = col.GetInstanceID();
        }

        public bool StillLive()
        {
            return col.enabled;
        }
    }
    
    List<enemyAim> transformList = new List<enemyAim>();

    enemyAim helpTransform;
    float helpDist, helpDist2;
    int helpId;
    

    public Transform GetNearestEnemy()
    {
        for (int i = 0; i < transformList.Count; i++)
            if (!transformList[i].StillLive())
            {
                transformList.RemoveAt(i);
                i--;
            }

        if (transformList.Count == 0)
            return null;

        helpTransform = transformList[0]; 

        if (transformList.Count > 1)
        {
            helpDist = Vector3.SqrMagnitude(transform.position - helpTransform.tr.position);
            for (int i = 1; i < transformList.Count; i++)
            {
                helpDist2 = Vector3.SqrMagnitude(transform.position - transformList[i].tr.position);
                if (helpDist2 < helpDist)
                {
                    helpDist = helpDist2;
                    helpTransform = transformList[i];
                }
            }
        }

        return helpTransform.tr;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.isTrigger)
        transformList.Add(new enemyAim(collision.transform, collision));
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        helpId = collision.GetInstanceID();
        for (int i = 0; i < transformList.Count; i++)
        {
            if (transformList[i].id == helpId)
            {
                transformList.RemoveAt(i);
                return;
            }
        }
        //transformList.Remove(collision.transform);
        //Debug.Log("Usuwam");
    }
}
