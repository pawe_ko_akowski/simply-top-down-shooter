﻿using UnityEngine;

namespace STDS
{
    public class MobilePlayerShoot : Shoot
    {
        public CnControls.SimpleJoystick joy = null;
        public Transform stick = null, stickBase = null;


        private void Update()
        {
            if (joy.Stick.IsActive())
            {
                mousePos = stickBase.position;
                mousePos.z = 0;
                mousePosWorld = stick.position;
                mousePosWorld.z = 0;

                if (autoAim)
                    AutoAim();

                toSet.z = Mathf.Atan2((mousePosWorld.y - mousePos.y), (mousePosWorld.x - mousePos.x)) * Mathf.Rad2Deg - 90;
                toSet.x = toSet.y = 0;
                transform.eulerAngles = toSet;

                if (Time.time > timer)
                {
                    timer = Time.time + reloadTime;
                    ShootBullet(transform.rotation);
                }
            }
        }
    }
}
