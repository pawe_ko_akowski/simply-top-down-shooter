﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class IsMobile : MonoBehaviour
    {
        [SerializeField]
        MonoBehaviour[] behviourForMobile = null;
        [SerializeField]
        MonoBehaviour[] behaviourForPc = null;
        [SerializeField]
        GameObject[] gobForMobile = null;
        RectTransform[] mobileGobRectTransform = null;


        private void Start()    
        {
         
            #if UNITY_ANDROID || UNITY_IOS
                {
                    ActionForMobileVersion();
                    MoreMobileAction();
                }
            #else
                {
                    ActionForPcVersion();
                }
            #endif
            Destroy(this);
        }


        void ActionForMobileVersion()
        {
            for (int i = 0; i < behviourForMobile.Length; i++)
            {
                behviourForMobile[i].enabled = true;
            }

            for (int i = 0; i < behaviourForPc.Length; i++)
            {
                Destroy(behaviourForPc[i]);
            }
        }


        void MoreMobileAction()
        {
            FillAllRectTransform();

            if (StoreSTDSGameData.instance != null)
            {
                SetAnchorsFromSettings();
            }
            else
            {
                SetDefaultAnchors();
            }
        }

        void FillAllRectTransform()
        {
            mobileGobRectTransform = new RectTransform[gobForMobile.Length];
            for (int i = 0; i < mobileGobRectTransform.Length; i++)
            {
                mobileGobRectTransform[i] = gobForMobile[i].GetComponent<RectTransform>();
            }
        }


        void SetDefaultAnchors()
        {
            int[] set = new int[] { 1, 2, 3, 4 };
            RectSet(set);
        }

        void SetAnchorsFromSettings()
        {
            int[] set = StoreSTDSGameData.instance.stdsData.mobileControlSet;
            RectSet(set);
        }

        void RectSet(int[] set)
        {
            for (int i = 0; i < mobileGobRectTransform.Length; i++)
            {
                if (set[i] == 1)
                {
                    mobileGobRectTransform[i].anchorMin = new Vector2(0, 0.5f);
                    mobileGobRectTransform[i].anchorMax = new Vector2(0.5f, 1f);
                }
                if (set[i] == 2)
                {
                    mobileGobRectTransform[i].anchorMin = new Vector2(0, 0);
                    mobileGobRectTransform[i].anchorMax = new Vector2(0.5f, 0.5f);
                }
                if (set[i] == 3)
                {
                    mobileGobRectTransform[i].anchorMin = new Vector2(0.5f, 0.5f);
                    mobileGobRectTransform[i].anchorMax = new Vector2(1f, 1f);
                }
                if (set[i] == 4)
                {
                    mobileGobRectTransform[i].anchorMin = new Vector2(0.5f, 0f);
                    mobileGobRectTransform[i].anchorMax = new Vector2(1f, 0.5f);
                }
            }
        }

        void ActionForPcVersion()
        {
            for (int i = 0; i < behaviourForPc.Length; i++)
            {
                behaviourForPc[i].enabled = true;
            }

            for (int i = 0; i < behviourForMobile.Length; i++)
            {
                Destroy(behviourForMobile[i]);
            }

            for (int i = 0; i < gobForMobile.Length; i++)
            {
                Destroy(gobForMobile[i]);
            }
        }


    }
}
