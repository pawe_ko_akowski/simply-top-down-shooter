﻿using UnityEngine;

namespace STDS
{
    [CreateAssetMenu(menuName = "PlayerProgress")]
    public class GamePlayerProgressData : ScriptableObject
    {
        public int experience, cash, levelExperiencePoints;
    }
}