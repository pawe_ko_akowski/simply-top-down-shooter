﻿using UnityEngine;

namespace STDS
{
    public class PlayerStatsSaverAndLoader : MonoBehaviour
    {
        public static PlayerStatsSaverAndLoader instance = null;

        [SerializeField]
        PlayerStats playerStat = null;


        void Awake()
        {
            if (instance != null)
            {
                //Debug.LogError("uwaga conajmniej 2 instancje byly");
                Destroy(gameObject);
                return;
            }

            instance = this;
            //dupa
            //DontDestroyOnLoad(gameObject);
        }


        public void SaveCoins(int add)
        {
            playerStat.coins += add;
        }
    }
}
