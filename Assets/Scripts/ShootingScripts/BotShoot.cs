﻿using UnityEngine;
namespace STDS
{
    public class BotShoot : Shoot
    {
        public float shootRange = 20f;
        public bool smartShoot = false;
        public Rigidbody2D rig;
        public SimpleFollow simpFollow = null;

        private Vector2 dir;


        protected override void StartThingsToDo()
        {
            base.StartThingsToDo();
            shootRange *= shootRange;
        }


        bool InRange(Vector2 pos)
        {
            if (Vector2.SqrMagnitude(pos - rig.position) < shootRange)
                return true;
            return false;
        }


        private void Update()
        {
            if (Time.time > timer)
            {
                if (InRange(simpFollow.GetDestination()))
                {
                    if (simpFollow.ISeePlayerOnFront() && simpFollow.ClearViewCheck())
                    {
                        if (smartShoot)
                            dir = ((simpFollow.GetDestination() - SimpleFollowManager.instance.GetMoveVectorFromTwoLast() * 6f) - rig.position).normalized;
                        else
                            dir = simpFollow.GetPlayerDirectionNormalized();

                        if (bulletForShoot > 1)
                        {
                            ShootBulletSecond(Quaternion.Euler(0, 0, Mathf.Atan2((dir.y), (dir.x)) * Mathf.Rad2Deg - 90));
                            
                        }
                        else
                        {
                            ShootBullet(Quaternion.Euler(0, 0, Mathf.Atan2((dir.y), (dir.x)) * Mathf.Rad2Deg - 90));
                        }
                       // timer = Time.time + reloadTime;
                    }
                }
            }
        }
    }
}
