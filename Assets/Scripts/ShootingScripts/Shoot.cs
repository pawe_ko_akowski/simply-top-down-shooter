﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class Shoot : MonoBehaviour
    {
        public bool autoAim = false;
        public GameObject gobWithTrigger = null;
        public float reloadTime = 0.35f;
        public int bulletForShoot = 1;
        public float rotForBullet = 8f;
        public AudioClip shootClip = null;
        public int particleBulletSystem = 0;
        protected ParticleSystem particleLauncher;
        public float bulletSpeed = 20f;
        public bool playerShoot = false;

        protected AudioManager audioManager;
        protected Vector3 mousePos, mousePosWorld, toSet;
        protected float timer = 0, timer2 = 0;
        protected StdsBullet bullet;
        protected int bulletDamage = 10;
        protected List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

        private ParticleSystem.EmitParams emiPar = new ParticleSystem.EmitParams();
        private bool padMotion = false;
        private PadRotation padRotation;
        private Transform nearestEnemy = null;
        private AutoAimScript autoAimBehave;


        public void SetParticleColor(Color color)
        {
            ParticleSystem.MainModule settings = particleLauncher.main;
            settings.startColor = new ParticleSystem.MinMaxGradient(color);
        }

        private void Awake()
        {
            bullet = new StdsBullet(gameObject.GetInstanceID(), bulletDamage);
            audioManager = AudioManager.instance;
        }


        private void SetCursorVisibility(bool to)
        {
            Cursor.visible = !to;
        }


        private void OnEnable()
        {
            if (Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0] != "")
            {
                padMotion = true;
                SetCursorVisibility(padMotion);
            }
            else
            {
                padMotion = false;
                SetCursorVisibility(padMotion);
            }
        }
        

        void Start()
        {
            padRotation = GetComponent<PadRotation>();
            if (StoreSTDSGameData.instance != null)
                autoAim = StoreSTDSGameData.instance.stdsData.AutoAim;
            if (gobWithTrigger != null && autoAim)
                autoAimBehave = gobWithTrigger.AddComponent<AutoAimScript>();
            StartThingsToDo();
        }


        protected virtual void StartThingsToDo()
        {
            particleLauncher = BulletParticleSystemManager.instance.GetParticleSystem(particleBulletSystem);
        }


        private void ControlPlayerTransform()
        {
            toSet.z = Mathf.Atan2((mousePosWorld.y - transform.position.y), (mousePosWorld.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.eulerAngles = toSet;
        }


        protected void AutoAim()
        {
            nearestEnemy = autoAimBehave.GetNearestEnemy();
            if (nearestEnemy != null)
            {
                mousePosWorld = nearestEnemy.position;
                mousePosWorld.z = 0;
                mousePos = transform.position;
                mousePos.z = 0;
            }
        }

        private void Update()
        {
            //dupa przepisz coś z gabi rescue - tam masz lepszy skrypt strzelania - dokładniejszy
            if (autoAim)
            {
                nearestEnemy = autoAimBehave.GetNearestEnemy();
                if (nearestEnemy != null)
                {
                    mousePosWorld = nearestEnemy.position;
                }
            }

            if (nearestEnemy == null)
            {
                if (padMotion)
                {
                    padRotation.PadMotion();
                }
                else
                {
                    mousePos = Input.mousePosition;
                    mousePos.z -= Camera.main.transform.position.z;
                    mousePosWorld = Camera.main.ScreenToWorldPoint(mousePos);
                    ControlPlayerTransform();
                }
            }
            else
            {
                ControlPlayerTransform();
            }

            if (Input.GetButton("Fire1") && Time.time > timer)
            {
                ShootBullet(transform.rotation);
            }
        }


        private void PlayAudioShoot()
        {
            if (playerShoot)
            {
                audioManager.Play2DAudio(shootClip, 0.1f);
            }
            else
                audioManager.Play3DAudio(shootClip, transform.position);
        }


        protected void ShootBullet(Quaternion rot)
        {
            RealShoot(rot);
            timer = Time.time + reloadTime;
            PlayAudioShoot();
        }


        protected void ShootBulletSecond(Quaternion rot)
        {
            float firstRot = rot.eulerAngles.z;
            if (bulletForShoot > 1)
            {
                if (bulletForShoot % 2 > 0)
                {
                    firstRot = firstRot - (bulletForShoot - 1) / 2 * rotForBullet;
                }
                else
                {
                    firstRot = firstRot - (bulletForShoot) / 2 * rotForBullet;
                }
            }
            for (int i = 0; i < bulletForShoot; i++)
            {
                RealShoot(Quaternion.Euler(0, 0, firstRot + i * rotForBullet));
            }
            timer = Time.time + reloadTime;
            PlayAudioShoot();
        }


        void RealShoot(Vector3 pos)
        {
            emiPar.position = transform.position;
            emiPar.velocity = pos * bulletSpeed;
            particleLauncher.Emit(emiPar, 1);
        }


        void RealShoot(Quaternion rot)
        {
            emiPar.position = transform.position;
            emiPar.velocity = rot * Vector3.up * bulletSpeed;
            particleLauncher.Emit(emiPar, 1);
        }


        public void SetFireRate(float rate)
        {
            reloadTime = rate;
        }
    }
}
