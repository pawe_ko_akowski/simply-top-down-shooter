﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class NavigationManager : MonoBehaviour
    {
        public static NavigationManager instance = null;

        //[SerializeField]
        //MapMarker[] navigatePointsMarkers = null;
        [SerializeField]
        bool succesWhenEnd = true;
        [SerializeField]
        Text timeText = null;
        //[SerializeField]
       // float timeCountDown = 100;

        int actualPoint = 0;
        float timer;
        bool endTimer = false;


        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
        }


        void FixedUpdate()
        {
            if (!endTimer)
            {
                timer -= Time.fixedDeltaTime;

                if (timer <= 0 && succesWhenEnd)
                {
                    GameManager.instance.EndGameFinaly();
                    endTimer = true;
                    timer = 0;
                }

                timeText.text = timer.ToString("F2");
            }
        }


        public void Init()
        {
            actualPoint++;
            //if (actualPoint >= navigatePointsMarkers.Length)
            //{
               // actualPoint = 0;
                //for (int i = 0; i < navigatePointsMarkers.Length; i++)
                //{
                    //if (i > actualPoint)
                    //{
                        //navigatePointsMarkers[i].hide();
                        //navigatePointsMarkers[i].gameObject.SetActive(false);
                   // }
               // }
               // timer = timeCountDown;
            //}
        }


        public void Next()
        {
            //actualPoint++;
            //if (actualPoint >= navigatePointsMarkers.Length && succesWhenEnd)
            //{
               // GameManager.instance.SuccesEndGame();
               // endTimer = true;
            //}
            //else
           // {
                //if (actualPoint < navigatePointsMarkers.Length)
                //{
                    //navigatePointsMarkers[actualPoint].gameObject.SetActive(true);
                    //navigatePointsMarkers[actualPoint].show();
                //}
            //}
        }
    }
}
