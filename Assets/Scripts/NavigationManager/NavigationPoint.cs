﻿using UnityEngine;

namespace STDS
{
    public class NavigationPoint : MonoBehaviour
    {
        NavigationManager nM;


        void Start()
        {
            nM = NavigationManager.instance;
        }


        void OnTriggerEnter2D(Collider2D other)
        {
            Destroy(gameObject);
            nM.Next();
        }
    }
}
