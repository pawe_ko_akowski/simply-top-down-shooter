﻿using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class CashObserverManager : MonoBehaviour
    {
        public static CashObserverManager instance = null;

        List<ICashObserver> cashObserverList;
        // Use this for initialization
        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            cashObserverList = new List<ICashObserver>();
        }

        public void RegisterICashObserver(ICashObserver iCashObser)
        {
            cashObserverList.Add(iCashObser);
        }

        public void GiveNotesAll()
        {
            for (int i = 0; i < cashObserverList.Count; i++)
            {
                cashObserverList[i].CashChange();
            }
        }
    }
}
