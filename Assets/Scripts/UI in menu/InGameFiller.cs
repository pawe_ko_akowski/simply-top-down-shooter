﻿using UnityEngine;
using UnityEngine.UI;

public class InGameFiller : MonoBehaviour
{
    public Text xpGainedText, xpMultiplierText, xpAllRewardText, coinGainedText, coinMultiplierText, coinAllRewardText;
    public GameObject failureTextGob, succesTextGob, breakMenuGob, rewardMenuGob;

    public Text infoText;

    public AudioClip winAudioClip, loseAudioclip, btnAudioClip;

    public Button resumeBtn, endMatchBtn, restartMatchBtn, toMainMenuBtn;
}
