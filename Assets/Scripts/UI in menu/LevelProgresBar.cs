﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class LevelProgresBar : MonoBehaviour, ICashObserver
    {
        [SerializeField]
        Text cashText = null, skilPointText = null;

        public void CashChange()
        {
            cashText.text = StoreSTDSGameData.instance.stdsData.coins.ToString();
            skilPointText.text = StoreSTDSGameData.instance.stdsData.skillPoints.ToString();
            // levelProgress.GetPlayerStats.coins.ToString();
        }

        // Use this for initialization
        void Start()
        {
            CashObserverManager.instance.RegisterICashObserver(this);
        }
    }
}
