﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace STDS
{
    public class JosticButtonSetterManager : MonoBehaviour
    {

        private static JosticButtonSetterManager instance = null;

        public JosticButtonSetterManager Instance
        {
            get
            {

                if (Input.GetJoystickNames().Length == 0 || Input.GetJoystickNames()[0] == "")
                    return null;
                if (instance == null)
                    instance = this;
                return instance;

            }
        }


        public GameObject select;
        public EventSystem es;
#if UNITY_STANDALONE || UNITY_WSA_10_0 || UNITY_WSA

        public void SetupSelected(GameObject gob)
        {
            if (select != gob)
            {
                select = gob;
                Select();
            }
        }

        private void Select()
        {
            es.SetSelectedGameObject(select);
            //es.UpdateModules();
            //es.
            
        }

        //private void Start()
        //{
            
        //}


        // Update is called once per frame
        void Update()
        {
            if (Input.GetButtonDown("Submit"))
                {
                if (es.alreadySelecting == false)
                {
                    //es.firstSelectedGameObject = select;
                    Select();
                }

            }
        }
#endif
    }


}
