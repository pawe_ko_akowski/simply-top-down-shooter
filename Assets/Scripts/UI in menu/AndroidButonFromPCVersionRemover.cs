﻿using UnityEngine;
using UnityEngine.UI;

 namespace STDS
{
    public class AndroidButonFromPCVersionRemover : MonoBehaviour
    {
        [SerializeField]
        Button[] btnToHide = null;

        [SerializeField]
        GameObject[] gobToHide = null;


//        #if !UNITY_ANDROID && !UNITY_IOS
//        //[SerializeField]
//        //string mobileOnlyMessage = " - moblie only";
//#else
//        [SerializeField]
//        string pcOnlyMessage = " - PC only";   
//                [SerializeField]
//        Button[] btnToHideOnMobile = null;
//#endif
        [SerializeField]
        bool hide = true;

        void Start()
        {

            // if(Application.


#if UNITY_WSA_10_0 || UNITY_WSA || UNITY_STANDALONE
            for (int i = 0; i<btnToHide.Length; i++)
            {
                if (hide)
                {
                    btnToHide[i].gameObject.SetActive(false);
                }
                else
                {
                    btnToHide[i].interactable = false;    
                }
                           
            }
            
            for (int j = 0; j < gobToHide.Length; j++)
                gobToHide[j].SetActive(false);
#else
            for (int i = 0; i<btnToHide.Length; i++)
            {
                if (hide)
                {
                    btnToHide[i].gameObject.SetActive(false);
                }
                else
                {
                    btnToHide[i].interactable = false;    
                }
                           
            }
            StoreSTDSGameData.instance.stdsData.autoAim = 1;
            Application.targetFrameRate = 60;
            //{
            // for(int i = 0; i<btnToHideOnMobile.Length; i++)
            //{
            //    btnToHide[i].interactable = false;
            //    btnToHide[i].GetComponentInChildren<Text>().text += mobileOnlyMessage;
            //    if(hide)
            //    btnToHide[i].gameObject.SetActive(false);
            //}

            //            for (int j = 0; j < gobToHide.Length; j++)
            //    gobToHide[j].SetActive(false);
            //}

#endif
        }
    }
}
