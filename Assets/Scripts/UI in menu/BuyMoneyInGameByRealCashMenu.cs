﻿using UnityEngine;
using UnityEngine.UI;
using System;
//using CompleteProject;

namespace STDS
{
    public class BuyMoneyInGameByRealCashMenu : MonoBehaviour, ICashObserver
    {
        [Serializable]
        class DolarForMoney
        {
            public int dollar = 0;
            public int money = 0;
        }
        //[SerializeField]
        //Purchaser purchaser;
        [SerializeField]
        DolarForMoney[] dolarForMoney = null;
        [SerializeField]
        Button buy25000By1DollarBtn = null,
            buy75000By2DollarsBtn = null,
            buy250000By3DollarsBtn = null,
            buy20000000Bt99DollarsBtn = null;
        [SerializeField]
        Text coinText = null;

        StoreSTDSGameData ssGD;
        //FounderAndSpectatorFill founderAndSpecatorFill = null;

        //private SkillBtnsManager sBM;


        void Start()
        {
            ssGD = StoreSTDSGameData.instance;
            CashObserverManager.instance.RegisterICashObserver(this);
            //sBM = GetComponent<SkillBtnsManager>();
            buy25000By1DollarBtn.onClick.AddListener(delegate { BuyMoney(dolarForMoney[0]); });
            buy75000By2DollarsBtn.onClick.AddListener(delegate { BuyMoney(dolarForMoney[1]); });
            buy250000By3DollarsBtn.onClick.AddListener(delegate { BuyMoney(dolarForMoney[2]); });
            buy20000000Bt99DollarsBtn.onClick.AddListener(delegate { BuyMoney(dolarForMoney[3]); });
            //founderAndSpecatorFill = FindObjectOfType<FounderAndSpectatorFill>();
        }


        void BuyMoney(DolarForMoney dollar)
        {
            // Debug.Log("I spent " + dollar.dollar + " $ and get " + dollar.money + " !!!");
            //purchaser.

          //  purchaser.BuyConsumable(dollar.money);
          // ssGD.stdsData.coins += dollar.money;
           // ssGD.stdsData.founder = 1;
           // if (founderAndSpecatorFill != null)
             //   founderAndSpecatorFill.Show();
            
           // ssGD.Save();
            CashObserverManager.instance.GiveNotesAll();

        }

        public void CashChange()
        {
            coinText.text = ssGD.stdsData.coins.ToString();
            //Debug.Log("Update " + ssGD.stdsData.coins);
            //sBM.Refresh();
        }
    }
}
