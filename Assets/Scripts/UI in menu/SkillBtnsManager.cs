﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace STDS
{
    public class SkillBtnsManager : MonoBehaviour, ICashObserver
    {
        public static SkillBtnsManager instance = null;

        [SerializeField]
        Image fillLevelImage = null;

        [SerializeField]
        Text playerNameText = null, playerLevelText = null, playerXPToNextLevelText = null;

        [SerializeField]
        Text playerFillCashText = null, playerFillXPPointText = null;

        [SerializeField]
        Button[] btnStopingFiller = null;

        [SerializeField]
        Text[] playerCashText = null, playerXpText = null;

        [SerializeField]
        SkillUIElemen liveBtn = null, liveRegBtn = null, bulletDmgBtn = null, bulletRateBtn = null, movementBtn = null, armorBtn = null, grenadeDamageBtn = null,
            grenadeRangeBtn = null, mineDamageBtn = null, mineRangeBtn = null, goldTakeBtn = null, goldCoinAddBtn = null,
            goldFrequencyBtn = null;

        [SerializeField]
        LevelProgress levelProgress = null;

        [SerializeField]
        Color forActiveBtnColor = Color.green, forNoActiveBtnColor = Color.gray;

        [SerializeField]
        AudioClip progressAudioClip = null;
        [SerializeField]
        AudioClip skillUpClip = null;

        SkillCost helpSkilCost;
        AudioSource audioSource;
       // bool filling = true;
        StoreSTDSGameData ssGD;
        List<LevelToFill> levelToFillList = new List<LevelToFill>();
        bool work = false;


        private class LevelToFill
        {
            public int allLevelXP;
            public int startLevelXP;
            public int endLevelXP;
            public float fullFillTime;
            public float timer;
            public float currentLevelFill = 0;
            public bool uiFilled;
            
            public LevelToFill(int startXP, int endXP, int lastXP, int currentXP, float fill = 2f)
            {
                startLevelXP = startXP;
                endLevelXP = endXP;
                currentLevelFill = 0;
                allLevelXP = endXP - startXP;
                fullFillTime = fill;
                uiFilled = false;

                int howMuch = 0;

                if (lastXP > startXP)
                    howMuch = (lastXP - startXP);

                timer = fullFillTime * ((float)howMuch / allLevelXP);

                if (currentXP < endXP)
                    currentLevelFill = fullFillTime - fullFillTime * ((float)(endXP - currentXP) / allLevelXP);
            }
        }


        private void PlayProgressSound()
        {
            audioSource.PlayOneShot(progressAudioClip);
        }


        private void AddOnClicStopFilling()
        {
            for (int i = 0; i < btnStopingFiller.Length; i++)
            {
                btnStopingFiller[i].onClick.AddListener(StopFillingInfo);
            }
        }


        void Start()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;

            ssGD = StoreSTDSGameData.instance;
            CashObserverManager.instance.RegisterICashObserver(this);

            //filling = true;
            audioSource = GetComponent<AudioSource>();

            liveBtn.GetBtn.onClick.AddListener(LiveLevelUpAction);
            liveRegBtn.GetBtn.onClick.AddListener(LiveRegenerationLevelUpAction);
            bulletDmgBtn.GetBtn.onClick.AddListener(BulletDamageLevelUpAction);
            bulletRateBtn.GetBtn.onClick.AddListener(BulletRateLevelUpAction);
            movementBtn.GetBtn.onClick.AddListener(MovementSpeedLevelUpAction);
            armorBtn.GetBtn.onClick.AddListener(ArmorLevelUpAction);
            grenadeDamageBtn.GetBtn.onClick.AddListener(GrenadeDamageLevelUpAction);
            grenadeRangeBtn.GetBtn.onClick.AddListener(GrenadeRangeLevelUpAction);
            mineDamageBtn.GetBtn.onClick.AddListener(MineDamageLevelUpAction);
            mineRangeBtn.GetBtn.onClick.AddListener(MineRangeLevelUpAction);
            goldTakeBtn.GetBtn.onClick.AddListener(GoldTakeRangeLevelUpAction);
            goldCoinAddBtn.GetBtn.onClick.AddListener(GoldAddLevelUpAction);
            goldFrequencyBtn.GetBtn.onClick.AddListener(GoldFrequencyLevelUpAction);

            AddOnClicStopFilling();


            if (levelProgress.GetPlayerStats.LevelWasSet())
            {
                FillLevelFiller();
                StartCoroutine(FillAndShow());
            }
            else
            {
                Refresh();
                FillPlayerIdentifyInfo();
            }

            work = true;
        }


        private void FillLevelFiller()
        {
            int lastXP = levelProgress.GetPlayerStats.GetLastXP();
            int lastLevel = levelProgress.GetPlayerStats.GetLastLevel();

           // Debug.Log(lastLevel);

            int currentLevel = levelProgress.GetPlayerStats.level;
            //dupa blokada na sztywno ten level max chyba trzeba skądś pobierać bo już w paru miejscach dałem na sztywno 100
            if (currentLevel >= 100)
                currentLevel = 99;
            int currentXP = levelProgress.GetPlayerStats.xp;

           // Debug.Log(currentLevel);

            //int allSkillPoints = ssGD.stdsData.skillPoints;

            while (lastLevel <= currentLevel)
            {
                LevelToFill lTF = new LevelToFill(levelProgress.GetPlayerStats.GetXPForLevel(lastLevel), 
                    levelProgress.GetPlayerStats.GetXPForLevel(lastLevel + 1),
                    lastXP, 
                    currentXP,
                    3f);

                levelToFillList.Add(lTF);
                lastLevel++;
            }
        }

        


        IEnumerator FillAndShow()
        {
            int start = 0;
            int listLength = levelToFillList.Count;
            //dupa małe zmiany bo coś źle liczyło
            int allSkillPoints = StoreSTDSGameData.instance.stdsData.skillPoints;//levelProgress.GetPlayerStats.skillPoints;
            int lastCash = levelProgress.GetPlayerStats.GetLastCash();
             playerFillCashText.text = lastCash.ToString();

            while (start < listLength)
            {
                yield return null;

                float filTime = 0;
                float fullTime = 0;
                float checkTime = 0;

                filTime = levelToFillList[start].timer;
                    fullTime = levelToFillList[start].fullFillTime;

                    if (levelToFillList[start].currentLevelFill != 0)
                        checkTime = levelToFillList[start].currentLevelFill;
                    else
                        checkTime = fullTime;

                    if (!levelToFillList[start].uiFilled)
                    {
                        playerFillXPPointText.text = (allSkillPoints - (listLength - 1 - start)).ToString();
                        playerLevelText.text = (ssGD.stdsData.level - (listLength - 1 - start)).ToString();
                        playerFillCashText.text = lastCash.ToString();
                        levelToFillList[start].uiFilled = true;
                    }


                if (filTime < checkTime)
                {
                    int levelXP = levelToFillList[start].allLevelXP;
                    int actualLevelXP = (int)(levelXP * (filTime/fullTime));

                    FillXPToNextLevel(actualLevelXP, levelXP);
                    levelToFillList[start].timer += Time.deltaTime;
                }
                else
                {

                    //Debug.Log("Play sound dupa");
                    start++;
                    if(start < listLength)
                        AudioManager.instance.Play2DAudio(skillUpClip);
                }
            }

            FillXPToNextLevel();
            int currentCash = ssGD.stdsData.coins;
            float timerek = 0;
            float allTimerek = 2f;

            if(lastCash < currentCash)
            while (timerek < allTimerek)
            {
                //Debug.Log(lastCash);
                //Debug.Log("zapelniam");
                yield return null;
                playerFillCashText.text = (lastCash + (currentCash - lastCash) * (timerek / allTimerek)).ToString("F0");
                timerek += Time.deltaTime;
            }

            playerFillCashText.text = ssGD.stdsData.coins.ToString();
            playerLevelText.text = ssGD.stdsData.level.ToString();
           
            Refresh();
        }

        private void OnApplicationFocus(bool focus)
        {
            if(work)
            StopFillingInfo();
        }


        private void StopFillingInfo()
        {
            StopAllCoroutines();
            FillPlayerIdentifyInfo();
        }


        void FillXPToNextLevel()
        {
            FillXPToNextLevel(
                levelProgress.GetPlayerStats.xp - levelProgress.GetPlayerStats.GetLastLevelXP,
                levelProgress.GetPlayerStats.GetNextLevelXP - levelProgress.GetPlayerStats.GetLastLevelXP);
        }


        void FillXPToNextLevel(int xp, int allXp)
        {
            if (ssGD.stdsData.level >= 100)
            {
                fillLevelImage.enabled = false;
                playerXPToNextLevelText.enabled = false;
            }
            else
            {
                fillLevelImage.fillAmount = (float)xp / allXp;
                playerXPToNextLevelText.text = (xp).ToString() + " / " + (allXp).ToString();
            }
        }


        void FillPlayerIdentifyInfo()
        {
            playerNameText.text = ssGD.stdsData.playerName;
            playerLevelText.text = ssGD.stdsData.level.ToString();
                                                               
            FillXPToNextLevel();
            
            playerFillCashText.text = ssGD.stdsData.coins.ToString();
            playerFillXPPointText.text = ssGD.stdsData.skillPoints.ToString();         
        }


        public void Refresh()
        {
            InfoAction();
            UpdatePlayerStatsInfo();
        }


        void InfoAction()
        {
            InfoActionForLiveSkill();
            InfoActionForLiveRegenerationSkill();
            InfoActionForBulletDamageSkill();
            InfoActionForBulletRateSkill();
            InfoActionForMovementSpeedSkill();
            InfoActionForArmorSkill();
            InfoActionForGrenadeDamageSkill();
            InfoActionForGrenadeRangeSkill();
            InfoActionForMineDamageSkill();
            InfoActionForMineRangeSkill();
            InfoActionForGoldRangeSkill();
            InfoActionForGoldAddSkill();
            InfoActionForGoldFrequencySkill();
        }


        void InfoActionForLiveSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.liveLevel))
                liveBtn.UpdateInfo(levelProgress.liveLevel, levelProgress.GetLiveStat().ToString());
            else
                liveBtn.UpdateInfo(levelProgress.liveLevel,
                    levelProgress.skillCostForXpNeeded[levelProgress.liveLevel], 
                    levelProgress.GetLiveStat().ToString());
        }


        void InfoActionForLiveRegenerationSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.liveRegenLevel))
                liveRegBtn.UpdateInfo(levelProgress.liveRegenLevel, levelProgress.GetLiveRegenerationStat().ToString());
            else
                liveRegBtn.UpdateInfo(levelProgress.liveRegenLevel, levelProgress.skillCostForXpNeeded[levelProgress.liveRegenLevel], levelProgress.GetLiveRegenerationStat().ToString());
        }


        void InfoActionForBulletDamageSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.bulletDamageLevel))
                bulletDmgBtn.UpdateInfo(levelProgress.bulletDamageLevel, levelProgress.GetBulletDamageStat().ToString());
            else
                bulletDmgBtn.UpdateInfo(levelProgress.bulletDamageLevel, levelProgress.skillCostForXpNeeded[levelProgress.bulletDamageLevel], levelProgress.GetBulletDamageStat().ToString());
        }


        void InfoActionForBulletRateSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.bulletRateLevel))
                bulletRateBtn.UpdateInfo(levelProgress.bulletRateLevel, levelProgress.GetBulletRateStat().ToString("F2"));
            else
                bulletRateBtn.UpdateInfo(levelProgress.bulletRateLevel, levelProgress.skillCostForXpNeeded[levelProgress.bulletRateLevel], levelProgress.GetBulletRateStat().ToString("F2"));
        }


        void InfoActionForMovementSpeedSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.movementLevel))
                movementBtn.UpdateInfo(levelProgress.movementLevel, levelProgress.GetMovementSpeedStat().ToString("F2"));
            else
                movementBtn.UpdateInfo(levelProgress.movementLevel, levelProgress.skillCostForXpNeeded[levelProgress.movementLevel], levelProgress.GetMovementSpeedStat().ToString("F2"));
        }

        void InfoActionForArmorSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.armorLevel))
                armorBtn.UpdateInfo(levelProgress.armorLevel, levelProgress.GetArmorStat().ToString());
            else
                armorBtn.UpdateInfo(levelProgress.armorLevel, levelProgress.skillCostForXpNeeded[levelProgress.armorLevel], levelProgress.GetArmorStat().ToString());
        }

        void InfoActionForGrenadeDamageSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.grenadeDamageLevel))
                grenadeDamageBtn.UpdateInfo(levelProgress.grenadeDamageLevel, levelProgress.GetGrenadeDamageStat().ToString());
            else
                grenadeDamageBtn.UpdateInfo(levelProgress.grenadeDamageLevel, levelProgress.skillCostForXpNeeded[levelProgress.grenadeDamageLevel], levelProgress.GetGrenadeDamageStat().ToString());
        }


        void InfoActionForGrenadeRangeSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.grenadeRangeLevel))
                grenadeRangeBtn.UpdateInfo(levelProgress.grenadeRangeLevel, levelProgress.GetGrenadeRangeStat().ToString("F2"));
            else
                grenadeRangeBtn.UpdateInfo(levelProgress.grenadeRangeLevel, levelProgress.skillCostForXpNeeded[levelProgress.grenadeRangeLevel], levelProgress.GetGrenadeRangeStat().ToString("F2"));
        }

        void InfoActionForMineDamageSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.mineDamageLevel))
                mineDamageBtn.UpdateInfo(levelProgress.mineDamageLevel, levelProgress.GetMineDamageStat().ToString());
            else
                mineDamageBtn.UpdateInfo(levelProgress.mineDamageLevel, levelProgress.skillCostForXpNeeded[levelProgress.mineDamageLevel], levelProgress.GetMineDamageStat().ToString());
        }

        void InfoActionForMineRangeSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.mineRangeLevel))
                mineRangeBtn.UpdateInfo(levelProgress.mineRangeLevel, levelProgress.GetMineRangeStat().ToString("F2"));
            else
                mineRangeBtn.UpdateInfo(levelProgress.mineRangeLevel, levelProgress.skillCostForXpNeeded[levelProgress.mineRangeLevel], levelProgress.GetMineRangeStat().ToString("F2"));
        }

        void InfoActionForGoldRangeSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.goldRangeLevel))
                goldTakeBtn.UpdateInfo(levelProgress.goldRangeLevel, levelProgress.GetGoldRangeStat().ToString("F2"));
            else
                goldTakeBtn.UpdateInfo(levelProgress.goldRangeLevel, levelProgress.skillCostForXpDontNeded[levelProgress.goldRangeLevel], levelProgress.GetGoldRangeStat().ToString("F2"));
        }

        void InfoActionForGoldAddSkill()
        {
            if (levelProgress.MaxLevel(levelProgress.goldAddLevel))
                goldCoinAddBtn.UpdateInfo(levelProgress.goldAddLevel, levelProgress.GetGoldAddStat().ToString());
            else
                goldCoinAddBtn.UpdateInfo(levelProgress.goldAddLevel, levelProgress.skillCostForXpDontNeded[levelProgress.goldAddLevel], levelProgress.GetGoldAddStat().ToString());
        }

        void InfoActionForGoldFrequencySkill()
        {
            if (levelProgress.MaxLevel(levelProgress.goldFrequencyLevel))
                goldFrequencyBtn.UpdateInfo(levelProgress.goldFrequencyLevel, levelProgress.GetGoldFrequencyStat().ToString("F2"));
            else
                goldFrequencyBtn.UpdateInfo(levelProgress.goldFrequencyLevel, levelProgress.skillCostForXpDontNeded[levelProgress.goldFrequencyLevel], levelProgress.GetGoldFrequencyStat().ToString("F2"));
        }

        void LiveLevelUpAction()
        {
            if (levelProgress.LiveLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void LiveRegenerationLevelUpAction()
        {
            if (levelProgress.LiveRegenerationLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void BulletDamageLevelUpAction()
        {
            if (levelProgress.BulletDamageLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void BulletRateLevelUpAction()
        {
            if (levelProgress.BulletRateLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void MovementSpeedLevelUpAction()
        {
            if (levelProgress.MovementSpeedLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void ArmorLevelUpAction()
        {
            if (levelProgress.ArmorLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void GrenadeDamageLevelUpAction()
        {
            if (levelProgress.GrenadeLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void GrenadeRangeLevelUpAction()
        {
            if (levelProgress.GrenadeRangeLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void MineDamageLevelUpAction()
        {
            if (levelProgress.MineDamageLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void MineRangeLevelUpAction()
        {
            if (levelProgress.MineRangeLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void GoldTakeRangeLevelUpAction()
        {
            if (levelProgress.GoldRangeLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void GoldAddLevelUpAction()
        {
            if (levelProgress.GoldAddUp())
            {
                ActionForLevelUpdate();
            }
        }


        void GoldFrequencyLevelUpAction()
        {
            if (levelProgress.GoldFrequencyLevelUp())
            {
                ActionForLevelUpdate();
            }
        }


        void ActionForLevelUpdate()
        {
            PlayProgressSound();
            CashObserverManager.instance.GiveNotesAll();
             //Refresh();
             //UpdatePlayerStatsInfo();
             //InfoAction();
        }


        void UpdatePlayerStatsInfo()
        {
            for (int i = 0; i < playerCashText.Length; i++)
            {
                playerCashText[i].text = ssGD.stdsData.coins.ToString();
            }
            for (int i = 0; i < playerXpText.Length; i++)
            {
                playerXpText[i].text = ssGD.stdsData.skillPoints.ToString(); 
            }
        }


        public SkillCost GetPlayerCashAndSkilPoints()
        {
            helpSkilCost.cash = ssGD.stdsData.coins;
            helpSkilCost.xpPoint = ssGD.stdsData.skillPoints;
            return helpSkilCost;
        }


        public Color GetColorForActive()
        {
            return forActiveBtnColor;
        }


        public Color GetColorForNoActive()
        {
            return forNoActiveBtnColor;
        }

        public void CashChange()
        {
            Refresh();
        }
    }
}
