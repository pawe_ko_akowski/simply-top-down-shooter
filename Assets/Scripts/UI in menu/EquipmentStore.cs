﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public interface ICashObserver
    {
        void CashChange();
    }

    public class EquipmentStore : MonoBehaviour, ICashObserver
    {
        //[SerializeField]
       // PlayerStats playerStats = null;

        [SerializeField]
        EquipmentStats equipmentStats = null;

        [SerializeField]
        Button buyGrenadeBtn = null, buyMineBtn = null;

        [SerializeField]
        Text grenadeAmountText = null, mineAmountText = null, coinText = null;

        Text buyGrenadeBtnText, buyMineBtnText;

        //[SerializeField]
        //string buyGrenadeStr = "Buy grenade ", buyMineStr = "Buy mine ";
        SimpleMainMenuBehaviour simpleMenu;
        MainMenuLanguageSetup langSetup;
        STDSdata sddsGameData;
        //int maxGrenadePrice;
        //int maxMinePrice;


        void Start()
        {
            sddsGameData = StoreSTDSGameData.instance.stdsData;
            CashObserverManager.instance.RegisterICashObserver(this);
            simpleMenu = GetComponent<SimpleMainMenuBehaviour>();
            buyGrenadeBtnText = buyGrenadeBtn.GetComponentInChildren<Text>();
            buyMineBtnText = buyMineBtn.GetComponentInChildren<Text>();
            langSetup = GetComponent<MainMenuLanguageSetup>();

            buyGrenadeBtn.onClick.AddListener(GrenadeBuy);
            buyMineBtn.onClick.AddListener(MineBuy);
            AllCheck();
            
        }


        //void OnEnable()
        //{
        //    AllCheck();
        //}

        void GrenadeAndMinesCheck()
        {
            buyGrenadeBtnText.text = langSetup.GetGrenadeBtnName + AddPirceToBtnName(equipmentStats.NextGrenadeCost(), equipmentStats.MaxGrenadePrice());
            buyMineBtnText.text = langSetup.GetMineBtnName + AddPirceToBtnName(equipmentStats.NextMineCost(), equipmentStats.MaxMinePrice());
        }


        void AllCheck()
        {
            grenadeAmountText.text = equipmentStats.Grenades.ToString();
            mineAmountText.text =  equipmentStats.Mines.ToString();
            //coinText.text = playerStats.coins.ToString();
            GrenadeAndMinesCheck();
            //GrenadeBtnCheck();
            //MineBtnCheck();
            CashObserverManager.instance.GiveNotesAll();
        }


        string AddPirceToBtnName(int price, int maxPrice)
        {
            if (price > maxPrice)
                return "\n(" + langSetup.GetMaX + ")";

            return "\n(" + price + ")";
        }

        
        void GrenadeBuy()
        {
            simpleMenu.PlayBtnSound();
            // playerStats.coins -= equipmentStats.NextGrenadeCost();
            //sddsGameData.stdsData.coins -= equipmentStats.NextGrenadeCost();
            equipmentStats.BuyGrenade();
            AllCheck(); 
        }

        
        void GrenadeBtnCheck()
        {
            IfBoolInteractableThern(equipmentStats.CanIBuyGrenade(sddsGameData.coins), ref buyGrenadeBtn);
        }

        void IfBoolInteractableThern(bool what, ref Button btn)
        {
            btn.interactable = what;
        }

        void MineBuy()
        {
            simpleMenu.PlayBtnSound();
            //sddsGameData.stdsData.coins -= equipmentStats.NextMineCost();
            equipmentStats.BuyMine();
            AllCheck();
        }


        void MineBtnCheck()
        {
            IfBoolInteractableThern(equipmentStats.CanIBuyMine(sddsGameData.coins), ref buyMineBtn);
        }

        public void CashChange()
        {
            coinText.text = sddsGameData.coins.ToString();
            //StoreSTDSGameData.instance.stdsData
            GrenadeBtnCheck();
            MineBtnCheck();
            GrenadeAndMinesCheck();
            //AllCheck();
        }
    }
}
