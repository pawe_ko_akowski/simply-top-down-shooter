﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace STDS
{
    public class EndlessPlayPanelBehaviour : MonoBehaviour
    {
        [Serializable]
        class BtnAndMapToEndlessPlay
        {
            public Button btn = null;
            public int mapToPlay = 0;
        }

        [SerializeField]
        BtnAndMapToEndlessPlay[] btnAndMapToPlay = null;


        void PlayScene(int numb)
        {
            SceneManager.LoadScene(numb);
        }


        void Start()
        {
            for (int i = 0; i < btnAndMapToPlay.Length; i++)
            {
                int numbero = btnAndMapToPlay[i].mapToPlay;
                btnAndMapToPlay[i].btn.onClick.AddListener(delegate{ PlayScene(numbero); });
            }
        }
    }
}
