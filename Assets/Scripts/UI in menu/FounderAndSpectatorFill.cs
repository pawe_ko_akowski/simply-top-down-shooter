﻿using UnityEngine;

namespace STDS
{
    public class FounderAndSpectatorFill : MonoBehaviour
    {
        public GameObject spectatorGob, founderGob;


        private void Start()
        {
            Show();
        }

        public void Show()
        {
            if (StoreSTDSGameData.instance.stdsData.founder > 0)
            {
                founderGob.SetActive(true);
            }
            else
            {
                founderGob.SetActive(false);
            }

            if (StoreSTDSGameData.instance.stdsData.spectator > 0)
            {
                spectatorGob.SetActive(true);
            }
            else
            {
                spectatorGob.SetActive(false);
            }
        }
    }
}
