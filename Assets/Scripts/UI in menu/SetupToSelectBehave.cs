﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace STDS
{
    public class SetupToSelectBehave : MonoBehaviour
    {

        public GameObject toSelect;
        public JosticButtonSetterManager jBSM;

        public Button[] btnToSetupActive;
       //Button activeBtn;
        bool check;
        float timer = 0;
        GameObject lastSelected;
#if UNITY_WSA_10_0 || UNITY_WSA_10_0 || UNITY_STANDALONE
        private void Start()
        {
            foreach (Button bt in btnToSetupActive)
            {
                Button butt = bt;
                bt.onClick.AddListener(delegate { ChangeActive(butt); });

            }
        }


        private void OnEnable()
        {
            ChangeToSelected(toSelect);
            //Debug.Log(toSelect.name);
            //oldSelect = toSelect;

        }

        private void ChangeToSelected(GameObject gob)
        {
            if (jBSM.Instance != null)
                jBSM.Instance.SetupSelected(gob);

        }

        private void ChangeActive(Button btn)
        {
            lastSelected = btn.gameObject;
            //jBSM.Instance.SetupSelected(lastSelected);
            ChangeToSelected(lastSelected);
            check = true;
            timer = 0;
        }

        private void Update()
        {
            if (check)
            {
                timer += Time.deltaTime;
                if (timer > 0.1)
                {
                    if(!lastSelected.activeSelf || !lastSelected.transform.parent.gameObject.activeSelf)
                        ChangeToSelected(toSelect);
                }
            }
        }
        //private void OnDisable()
        //{
        //    toSelect = oldSelect;
        //}
#endif
    }
}
