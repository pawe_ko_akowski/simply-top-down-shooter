﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class SkillUIElemen : MonoBehaviour
    {
        [SerializeField]
        Button btn = null;
        [SerializeField]
        GameObject notMaxed = null, maxed = null;
        [SerializeField]
        Text statText = null, currentLevelCashCostText = null, currentLevelXPCostText = null, currentLevelText = null;
        [SerializeField]
        RectTransform imageRect = null;

       // Vector2 sizeDel;


       // void Awake()
        //{
          //  sizeDel = imageRect.sizeDelta;
        //}


        void ColorAndActivityForBtn(SkillCost skillCost, SkillCost playerCash)
        {
            if (playerCash.cash >= skillCost.cash && playerCash.xpPoint >= skillCost.xpPoint)
            {
               // Debug.Log("Dupsko " + playerCash.cash + "  " + playerCash.xpPoint);

                btn.interactable = true;
                btn.image.color = SkillBtnsManager.instance.GetColorForActive();
            }
            else
            {
                btn.interactable = false;
                btn.image.color = SkillBtnsManager.instance.GetColorForNoActive();
            }
        }


        public void UpdateInfo(int currentLevel, SkillCost cost, string infoForStatText, bool max = false)
        {
            maxed.SetActive(max);
            notMaxed.SetActive(!max);

            currentLevelCashCostText.text = cost.cash.ToString();
            currentLevelXPCostText.text = cost.xpPoint.ToString();
            currentLevelText.text = currentLevel.ToString();

            statText.text = infoForStatText;
            imageRect.sizeDelta = new Vector2(Screen.width * 0.85f * 0.25f * (float)(currentLevel / 10f), imageRect.sizeDelta.y);
            ColorAndActivityForBtn(cost, SkillBtnsManager.instance.GetPlayerCashAndSkilPoints());
        }


        public void UpdateInfo(int currentLevel, string infoForStatText)
        {
            maxed.SetActive(true);
            notMaxed.SetActive(false);
            currentLevelText.text = currentLevel.ToString();
            statText.text = infoForStatText;
            imageRect.sizeDelta = new Vector2(Screen.width * 0.85f * 0.25f * (float)(currentLevel / 10f), imageRect.sizeDelta.y);
        }


        public Button GetBtn
        {
            get
            {
                return btn;
            }
        }
    }
}
