﻿using UnityEngine;

namespace STDS
{
    public class AsignVisualEffects : MonoBehaviour
    {
        [SerializeField]
        Color[] colorForLips = null;

        [SerializeField]
        Color[] colorForBody = null;

        [SerializeField]
        Color[] colorForBullets = null;

        [SerializeField]
        Color[] colorForArmor = null;

        [SerializeField]
        SpriteRenderer armorSprite = null, lipsSprite = null, bodySprite = null;

        //[SerializeField]
       // AudioSource regenAudiosource = null;

        //[SerializeField]
        //float regenPitchChange = 0.04f;

        //[SerializeField]
        //ParticleSystem playerBulletPartSys = null;

        [SerializeField]
        int particleBulletSystem = 0;


        private int GetLevel(int level)
        {
            return level - 1;
        }


        //public void SetRegenerationSound(int regAmount)
        //{
        //    regenAudiosource.pitch += regenPitchChange * regAmount;
        //}


        public void SetBulletPower(int bulletDamLevel)
        {
           // if (bulletDamLevel > 0)
           // {
                //particleLauncher = BulletParticleSystemManager.instance.GetParticleSystem(particleBulletSystem);
                var main = BulletParticleSystemManager.instance.GetParticleSystem(particleBulletSystem).main;
            //Debug.Log(BulletParticleSystemManager.instance.GetParticleSystem(particleBulletSystem).name);
           // Debug.Log(bulletDamLevel);
            main.startColor = colorForBullets[colorForBullets.Length -1 - bulletDamLevel];// new Color(colorForBullets[bulletDamLevel].r, colorForBullets[bulletDamLevel].g, colorForBullets[bulletDamLevel].b) ;//new ParticleSystem.MinMaxGradient(colorForBullets[GetLevel(bulletDamLevel)]);
                                                                                                                                                  // }
            //main.maxParticles = 0;
        }


        public void SetBulletRate(int bulletRateLevel)
        {
            if (bulletRateLevel > 0)
            {
                lipsSprite.color = colorForLips[GetLevel(bulletRateLevel)];
            }
        }


        public void SetLiveAmount(int liveLevel)
        {
            if (liveLevel > 0)
            {
                bodySprite.color = colorForBody[GetLevel(liveLevel)];
            }
        }


        public void SetArmorLevel(int armorLevel)
        {
            if (armorLevel < 1)
            {
                armorSprite.gameObject.SetActive(false);
                return;
            }
            armorSprite.color = colorForArmor[GetLevel(armorLevel)];
        }
    }
}
