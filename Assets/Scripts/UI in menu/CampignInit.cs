﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace STDS
{
    [RequireComponent(typeof(AudioSource))]
    public class CampignInit : MonoBehaviour
    {
        [SerializeField]
        bool readDescription = true;

        [SerializeField]
        bool hideCampignBtns = false;

        [SerializeField]
        Text campingProgressText = null, campignTimerBestText = null, campignTimerActualText = null;

        [Serializable]
        class helpContainer
        {
            public int sceneIndex = 0;
            //public string missionName = "No name";
            //public string missionObiectives = "Any obiectives";
            //public string missionDescription = "Any description";
            //public AudioClip missionDescriptionAudioClip = null;
            public LevelDescription levelDescription = null;
            public Button buttonForScene = null;


            public string GetMissionDescription
            {
                get
                {
                    //return levelDescription.description[SettingsSet.instance.GetLanguageNumber()];
                    return levelDescription.descriptions[StoreSTDSGameData.instance.stdsData.language].description;
                }
            }

            public string GetMissionName
            {
                get
                {
                    // return levelDescription.missionName[SettingsSet.instance.GetLanguageNumber()];
                    return levelDescription.descriptions[StoreSTDSGameData.instance.stdsData.language].missionName;
                }
            }

            public string GetMissionObiectives
            {
                get
                {
                    //return levelDescription.missionObiectives[SettingsSet.instance.GetLanguageNumber()];
                    return levelDescription.descriptions[StoreSTDSGameData.instance.stdsData.language].missionObiectives;
                }
            }

            public AudioClip GetAudioRecord
            {
                get
                {
                    // if (levelDescription.missionAudioRecord[SettingsSet.instance.GetLanguageNumber()] != null)
                    //  return levelDescription.missionAudioRecord[SettingsSet.instance.GetLanguageNumber()];
                    //else
                    // return null;
                    return levelDescription.descriptions[StoreSTDSGameData.instance.stdsData.language].missionAudioRecord;
                }
            }

            public Sprite GetMissionImage
            {
                get
                {
                    return levelDescription.mapSprite;
                }
            }

            public bool DidThisBtn(Button btn)
            {
                if (buttonForScene == btn)
                    return true;
                return false;
            }

        }

        // [SerializeField]
        // int startIndexCampignScene = 1;

        //[SerializeField]
        //Campign campign = null;
        [SerializeField]
        int maxCampign = 10;

        [SerializeField]
        Sprite spriteForEndedBatles = null;

        [SerializeField]
        Sprite spriteForBeginBatles = null;

        [SerializeField]
        Button backFromInfoMap = null;

        [SerializeField]
        GameObject allCampignMapsGob = null, activeMapInfoGob = null;

        public Button campignResetBtn = null;
        public Text multiplierText = null;
        const string multiplierInfo = "XP x ";


        // [SerializeField]
        // int startSceneFrom = 3;

        //  [SerializeField]
        //  Button[] buttons = null;

        [SerializeField]
          helpContainer[] campignScense = null;

        [SerializeField]
        Button playBtn = null;

        [SerializeField]
        Text missionTitle = null, missionObiectives = null, missionDescription = null;

        [SerializeField]
        Image missionImage = null;

        // [SerializeField]
        int currentActiveCampignScene = 0;
        [SerializeField]
        AudioSource audiosource = null;
        SimpleMainMenuBehaviour simpleMenu;
        STDSdata data;

        void Start()
        {
            data = StoreSTDSGameData.instance.stdsData;
            //campingProgressText.text = campign.CurrentActiveScene + "/" + campign.MaxCampignScene;

            //audiosource = GetComponent<AudioSource>();
            InitActiveBtn();
            backFromInfoMap.onClick.AddListener(GoBackToAllMap);
            simpleMenu = GetComponent<SimpleMainMenuBehaviour>();

            if (data.CampignLevel >= 10)
            {
                if(data.actualCampignTime > 0)
                if (data.actualCampignTime < data.allCampignTime)
                    data.allCampignTime = data.actualCampignTime;
            }

            //multiplierText.text = multiplierInfo + (data.xpStaticMultiplier + 1);

            if (data.campignEndOnce > 0)
                campignTimerBestText.enabled = true;
            else
                campignTimerBestText.enabled = false;

            if (data.campignEndOnce > 0 && data.CampignLevel >=10 || data.xpStaticMultiplier >5)
            {
                campignResetBtn.interactable = true;
                campignResetBtn.onClick.AddListener(ResetAction);
            }
            else
            {
                campignResetBtn.interactable = false;
            }
            //campign.InitActiveBtn();
        }


        private void ResetAction()
        {
            if (data.xpStaticMultiplier < 5)
            {
                campignResetBtn.interactable = false;
                data.xpStaticMultiplier *=2;
            }

            data.CampignLevel = 0;
            data.actualCampignTime = 0;
            StoreSTDSGameData.instance.Save();            
            //StoreSTDSGameData.instance.Load();
            InitActiveBtn();
        }

        void InitActiveBtn()
        {
            multiplierText.text = multiplierInfo + (data.xpStaticMultiplier);
            currentActiveCampignScene = StoreSTDSGameData.instance.stdsData.CampignLevel;
           // Debug.Log(currentActiveCampignScene);

           // Debug.Log(StoreSTDSGameData.instance.stdsData.campignLevel);
           // Debug.Log(StoreSTDSGameData.instance.stdsData.level);
            for (int i = 0; i < campignScense.Length; i++)
            {
                if (i <= currentActiveCampignScene)
                {
                    if(i== currentActiveCampignScene)
                       // buttons[i].GetComponent<Image>().sprite = spriteForBeginBatles;
                    campignScense[i].buttonForScene.GetComponent<Image>().sprite = spriteForBeginBatles;
                    else
                    campignScense[i].buttonForScene.GetComponent<Image>().sprite = spriteForEndedBatles;

                    int what = i;
                    campignScense[i].buttonForScene.onClick.AddListener(delegate { LoadIndexScene(what); });
                    campignScense[i].buttonForScene.interactable = true;

                }
                else
                {
                    if (hideCampignBtns)
                    {
                        campignScense[i].buttonForScene.gameObject.SetActive(false);
                    }
                    else
                    {
                        campignScense[i].buttonForScene.GetComponent<Image>().sprite = spriteForBeginBatles;
                        campignScense[i].buttonForScene.interactable = false;
                    }
                    //buttons[i].GetComponent<Image>().sprite = spriteForBeginBatles;
                    //buttons[i].interactable = false;
                }
            }

            campingProgressText.text = data.CampignLevel + "/" + maxCampign;
            campignTimerBestText.text = data.allCampignTime.ToString();
            campignTimerActualText.text = data.actualCampignTime.ToString();
            //buttons[currentActiveCampignScene].interactable = true;
            //buttons[currentActiveCampignScene].onClick.AddListener(LoadActiveScene);
        }


        //void LoadActiveScene()
        //{
        //    Debug.Log("Loading " + currentActiveCampignScene);
        //    ToMapInfo();
        //    // SceneManager.LoadScene(startSceneFrom + currentActiveCampignScene);
        //}

        void LoadIndexScene(int index)
        {
            //simpleMenu.PlayBtnSound();
            // Debug.Log("Loading " + currentActiveCampignScene);

        
               // audiosource.PlayOneShot(campignScense[index].GetAudioRecord);

            playBtn.onClick.RemoveAllListeners();
            playBtn.onClick.AddListener(delegate { StartGame(campignScense[index].sceneIndex); });
            missionTitle.text = campignScense[index].GetMissionName;
            missionObiectives.text = campignScense[index].GetMissionObiectives;
            missionDescription.text = campignScense[index].GetMissionDescription;
            missionImage.sprite = campignScense[index].GetMissionImage;

            ToMapInfo();

            if (readDescription && campignScense[index].GetAudioRecord != null)
            {
                audiosource.clip = campignScense[index].GetAudioRecord;
                audiosource.Stop();
                audiosource.Play();
            }
            //SceneManager.LoadScene(startSceneFrom + index);
        }


        void StartGame(int scene)
        {
            SceneManager.LoadScene(scene);
        }

        void ToMapInfo()
        {
            simpleMenu.PlayBtnSound();
            allCampignMapsGob.SetActive(false);
            activeMapInfoGob.SetActive(true);
        }


        void GoBackToAllMap()
        {
            //Debug.Log("dupa");
            audiosource.Stop();
            simpleMenu.PlayBtnSound();
            allCampignMapsGob.SetActive(true);
            activeMapInfoGob.SetActive(false);
           
        }
    }
}
