﻿using UnityEngine;

namespace STDS
{
    public class HandleToZero : MonoBehaviour
    {

        RectTransform rectTransform;

        void Start()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        private void OnDisable()
        {
            rectTransform.anchoredPosition = Vector2.zero;
        }
    }
}
