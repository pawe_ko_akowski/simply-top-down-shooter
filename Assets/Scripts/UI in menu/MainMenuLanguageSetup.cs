﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class MainMenuLanguageSetup : MonoBehaviour
    {
        int currentLang = 0;

        
        public TextLanguageSetup[] textLanguageSetup;
        //[SerializeField]
       // GameOptions gameOptions = null;

        //main menu
        public Text PlayBtnText;
        public Text ProgresBtnText;
        public Text OptionsBtnText;
        public Text QuitBtnText;
        public Text MoreBtnText;
        public Text FeaturesBtnText;

        public Text[] AllBackBtnText;
        public Text[] AllCashLabelText;

        //play menu
        public Text PlayMenuLabelText;
        public Text CampigniBtnText;
        public Text EndlessBtnText;

        //campign menu
        public Text CampignLabelText;
        public Text PrevLabelText;
        public Text NextLabelText;
        public Text ProgressCampignLabelText;
        public Text ProgressCampignTimerLabelText;

        //campigns scenerio description
        public Text ObiectivesLabelText;
        public Text RunScenerioBtnText;

        //endless menu
        public Text EndlessPlayLabelText;
        public Text FirstMapBtnText;
        public Text SecondMapBtnText;

        //progress menu
        public Text ProgressPanelLabelText;
        public Text SkillLevelBtnText;
        public Text BoomShoopBtnText;
        public Text GoldShoopBtnText;
        public Text AdRevenueBtnText;

        //skill panel
        public Text SkillPanelLabelText;
        public Text[] SkillBuyBtnText;
        public Text[] SkillMaxLabelText;
        public Text SkillPointsLabelText;
        public Text LiveSkillLabelText;
        public Text LiveRegenSkillLabelText;
        public Text BulletDamageSkillLabelText;
        public Text BulletRateSkillLabelText;
        public Text MovementSpeedSkillLabelText;
        public Text ArmorSkillLabelText;
        public Text GrenadeDamageSkillLabelText;
        public Text GrenadeRangeSkillLabelText;
        public Text MineDamageSkillLabelText;
        public Text MineRangeSkillLabelText;
        public Text GoldTakeRangeSkillLabelText;
        public Text GoldAddSkillLabelText;
        public Text GoldFrequencySkillLabelText;

        //grenade and mine shoop
        public Text BoomShoopLabelText;
        public Text BuyGrenadeBtnText;
        public Text BuyMineBtnText;

        //gold for real cash shoop
        public Text GoldPanelShoopLabelText;

        //ad bonus revenue panel
        public Text AdBonusLabelText;
        public Text AdBonusDescriptionLabelText;
        public Text AdBonusActiveForLabelText;
        public Text DoubleGoldBtnText;
        public Text DoubleXPBtnText;

        //options panel
        public Text OptionsLabelText;
        public Text LanguageLabelText;
        public Text SoundVolumeLabelText;
        public Text MusicVolumeLabelText;
        public Text PlayerNameLabelText;
        public Text AutioaimLablelText;
        public Text ActiveText;
        public Text GrenadeTimerText;
        public Text MineTimerText;
        public Text ControlLabelText;
        public Text SmoothLabelText;
        public Text FasterLabelText;
        //public Text FirstControlSlotLabelText;
        //public Text SecondControlSlotLabelText;
        //public Text ThirdControlSlotLabelText;
        //public Text FourthControlSlotLabelText;
        //public Text DisactiveText;

        //more panel
        public Text MorePanelLabelText;
        public Text DonateBtnText;

        //features panel
        public Text FeaturesLabelText;
        public Text firstFeatureText;
        public Text secondFeatureText;
        public Text thirdFeatureText;
        public Text fourthFeatureText;
        public Text fifthFeatureText;
        public Text sixFeatureText;
        public Text sevenFeatureText;
        public Text eightFeatureText;
        public Text nineFeatureText;
        public Text tenFeatureText;
        public Text loadingText;

        //require texts
        public Text endlesRequireText;
        public Text mysteryEndlesRequireText;

        AbilitesAndEndlessRunHider AAERH = null;

        private void Awake()
        {
            AAERH = GetComponent<AbilitesAndEndlessRunHider>();
        }

        public void LanguageSetup(int langNumb)
        {
            currentLang = langNumb;
           // Debug.Log(langNumb);
            for (int i = 0; i < AllBackBtnText.Length; i++)
                AllBackBtnText[i].text = textLanguageSetup[langNumb].AllBackBtnName;

            for(int i =0;i<AllCashLabelText.Length; i++)
                AllCashLabelText[i].text = textLanguageSetup[langNumb].CashLabelName;

            PlayBtnText.text = textLanguageSetup[langNumb].PlayBtnName;
            ProgresBtnText.text = textLanguageSetup[langNumb].ProgresBtnName;
            OptionsBtnText.text = textLanguageSetup[langNumb].OptionsBtnName;
            QuitBtnText.text = textLanguageSetup[langNumb].QuitBtnName;
            MoreBtnText.text = textLanguageSetup[langNumb].MoreBtnName;
            FeaturesBtnText.text = textLanguageSetup[langNumb].featuresBtnName;

            //play menu
            PlayMenuLabelText.text = textLanguageSetup[langNumb].PlayMenuLabelName;
            CampigniBtnText.text = textLanguageSetup[langNumb].CampigniBtnName;
            EndlessBtnText.text = textLanguageSetup[langNumb].EndlessBtnName ;

            //campign menu
            CampignLabelText.text = textLanguageSetup[langNumb].HeroBornCampignTitleName;
            PrevLabelText.text = textLanguageSetup[langNumb].PrevBtnName;
            NextLabelText.text = textLanguageSetup[langNumb].NextBtnName;
            ProgressCampignLabelText.text = textLanguageSetup[langNumb].ProgressLabelName;
            ProgressCampignTimerLabelText.text = textLanguageSetup[langNumb].ProgressCampignTimerName;

            //campigns scenerio description
            ObiectivesLabelText.text = textLanguageSetup[langNumb].mapTaskLabelName;
            RunScenerioBtnText.text = textLanguageSetup[langNumb].PlayCampignsBtnName;

            //endless menu
            EndlessPlayLabelText.text = textLanguageSetup[langNumb].EndlessLabelName;
            FirstMapBtnText.text = textLanguageSetup[langNumb].FirstMapBtnName ;
            SecondMapBtnText.text = textLanguageSetup[langNumb].SecontMapBtnName;

            //progress menu
            ProgressPanelLabelText.text = textLanguageSetup[langNumb].ProgressPanelLabelName;
            SkillLevelBtnText.text = textLanguageSetup[langNumb].SkillUpBtnName;
            BoomShoopBtnText.text = textLanguageSetup[langNumb].GranadeAndMineShoopPanelBtnName;
            GoldShoopBtnText.text = textLanguageSetup[langNumb].PremiumGoldPanelBtnName;
            AdRevenueBtnText.text = textLanguageSetup[langNumb].AdPanelBtnName;

            //skill panel
            SkillPanelLabelText.text = textLanguageSetup[langNumb].SkillPanelLabelName;

            for (int i = 0; i < SkillBuyBtnText.Length; i++)
                SkillBuyBtnText[i].text = textLanguageSetup[langNumb].BuySkillName;

            for (int i = 0; i < SkillMaxLabelText.Length; i++)
                SkillMaxLabelText[i].text = textLanguageSetup[langNumb].MaxLevelLabelName;

            SkillPointsLabelText.text = textLanguageSetup[langNumb].SkillPointsLabelName;
            LiveSkillLabelText.text = textLanguageSetup[langNumb].LiveSkillLabelName;
            LiveRegenSkillLabelText.text = textLanguageSetup[langNumb].LiveRegenerationSkillLabelName;
            BulletDamageSkillLabelText.text = textLanguageSetup[langNumb].BulletDamageSkillLabelName;
            BulletRateSkillLabelText.text = textLanguageSetup[langNumb].BulletRateSkillLabelName;
            MovementSpeedSkillLabelText.text = textLanguageSetup[langNumb].MovementSpeedSkillLabelName ;
            ArmorSkillLabelText.text = textLanguageSetup[langNumb].ArmorSkillLabelName;
            GrenadeDamageSkillLabelText.text = textLanguageSetup[langNumb].GrenadeDamageSkillLabelName;
            GrenadeRangeSkillLabelText.text = textLanguageSetup[langNumb].GrenadeRangeSkillLabelName;
            MineDamageSkillLabelText.text = textLanguageSetup[langNumb].MineDamageSkillLabelName;
            MineRangeSkillLabelText.text = textLanguageSetup[langNumb].MineRangeSkillLabelName;
            GoldTakeRangeSkillLabelText.text = textLanguageSetup[langNumb].GoldTakeRangeSkillLabelName;
            GoldAddSkillLabelText.text = textLanguageSetup[langNumb].GoldAddSkillLabelName;
            GoldFrequencySkillLabelText.text = textLanguageSetup[langNumb].GoldFrequencyAddSkillLabelName;

            //grenade and mine shoop
            BoomShoopLabelText.text = textLanguageSetup[langNumb].BoomShoopLabelName;
            BuyGrenadeBtnText.text = textLanguageSetup[langNumb].BuyGrenadeBtnName;
            BuyMineBtnText.text = textLanguageSetup[langNumb].BuyMineBtnName;

            //gold for real cash shoop
            GoldPanelShoopLabelText.text = textLanguageSetup[langNumb].GoldPanelShoopLabelName;

            //ad bonus revenue panel
            AdBonusLabelText.text = textLanguageSetup[langNumb].AdBonusLabelName;
            AdBonusDescriptionLabelText.text = textLanguageSetup[langNumb].AdBonusDescriptionLabelName;
            AdBonusActiveForLabelText.text = textLanguageSetup[langNumb].AdBonusActiveForLabelName;
            DoubleGoldBtnText.text = textLanguageSetup[langNumb].DoubleGoldBtnName;
            DoubleXPBtnText.text = textLanguageSetup[langNumb].DoubleXPBtnName;

            //options panel
            OptionsLabelText.text = textLanguageSetup[langNumb].OptionsLabelName;
            LanguageLabelText.text = textLanguageSetup[langNumb].LanguageLabelName;
            SoundVolumeLabelText.text = textLanguageSetup[langNumb].SoundVolumeLabelName;
            MusicVolumeLabelText.text = textLanguageSetup[langNumb].MusicVolumeLabelName;
            PlayerNameLabelText.text = textLanguageSetup[langNumb].PlayerNameLabelName;
            AutioaimLablelText.text = textLanguageSetup[langNumb].AutioaimLablelName;
            GrenadeTimerText.text = textLanguageSetup[langNumb].GrenadeTimerName;
            MineTimerText.text = textLanguageSetup[langNumb].MineTimerName;
            ControlLabelText.text = textLanguageSetup[langNumb].ControlLabelName;
            SmoothLabelText.text = textLanguageSetup[langNumb].SmothLabelName;
            FasterLabelText.text = textLanguageSetup[langNumb].FasterLabelName;
            // gameOptions.FillAllControl();


            if (StoreSTDSGameData.instance.stdsData.AutoAim)
                ActiveText.text = textLanguageSetup[langNumb].ActiveName;
            else
                ActiveText.text = textLanguageSetup[langNumb].DisactiveName;
            // DisactiveText.text = textLanguageSetup[langNumb].DisactiveName;

            //more panel
            MorePanelLabelText.text = textLanguageSetup[langNumb].MorePanelLabelName;
            DonateBtnText.text = textLanguageSetup[langNumb].DonateBtnName;

            //feature panel
            FeaturesLabelText.text = textLanguageSetup[langNumb].featuresLabelName;
            firstFeatureText.text = textLanguageSetup[langNumb].firstFeatureName;
            secondFeatureText.text = textLanguageSetup[langNumb].secondFeatureName;
            thirdFeatureText.text = textLanguageSetup[langNumb].thirdFeatureName;
            fourthFeatureText.text = textLanguageSetup[langNumb].fourthfFeatureName;
            fifthFeatureText.text = textLanguageSetup[langNumb].fifthFeatureName;
            sixFeatureText.text = textLanguageSetup[langNumb].sixFeatureName;
            sevenFeatureText.text = textLanguageSetup[langNumb].sevenFeatureName;
            eightFeatureText.text = textLanguageSetup[langNumb].eightFeatureName;
            nineFeatureText.text = textLanguageSetup[langNumb].nineFeatureName;
            tenFeatureText.text = textLanguageSetup[langNumb].tenFeatureName;
            loadingText.text = textLanguageSetup[langNumb].loadingName;

            //require text
            if (!AAERH.EndlesActive())
            endlesRequireText.text = textLanguageSetup[langNumb].endlessBtnRequireLabel;
            if(!AAERH.MysteryActive())
            mysteryEndlesRequireText.text = textLanguageSetup[langNumb].mysteryEndlesBtnRequireLable;
        }

        //public void ControlSetup(int[] table)
        //{
        //    FirstControlSlotLabelText.text = textLanguageSetup[currentLang].ControlMoveLabelName;
        //    SecondControlSlotLabelText.text = textLanguageSetup[currentLang].ControlAimLabelName;
        //    ThirdControlSlotLabelText.text = textLanguageSetup[currentLang].ControlGrenadeDropLabelName;
        //    FourthControlSlotLabelText.text = textLanguageSetup[currentLang].ControlMineDropLableName;
        //}

        public string[] ControlSlotLabelNames()
        {
            string[] str = new string[]{ textLanguageSetup[currentLang].ControlNoneLabelName,
                 textLanguageSetup[currentLang].ControlMineDropLableName,
                textLanguageSetup[currentLang].ControlMoveLabelName,
                textLanguageSetup[currentLang].ControlGrenadeDropLabelName,
                textLanguageSetup[currentLang].ControlAimLabelName,
                
               };
            return str;
        }

        public string GetGrenadeBtnName
        {
            get
            {
                return textLanguageSetup[currentLang].BuyGrenadeBtnName;
            }
        }

        public string GetMineBtnName
        {
            get
            {
                return textLanguageSetup[currentLang].BuyMineBtnName;
            }
        }

        public string GetMaX
        {
            get
            {
                return textLanguageSetup[currentLang].MaxLevelLabelName;
            }
        }

        public string GetActive
        {
            get
            {
                return textLanguageSetup[currentLang].ActiveName;
            }
        }

        public string GetDisactive
        {
            get
            {
                return textLanguageSetup[currentLang].DisactiveName;
            }
        }
    }
}
