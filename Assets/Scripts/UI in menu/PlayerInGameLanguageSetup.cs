﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class PlayerInGameLanguageSetup : MonoBehaviour
    {
        //public Text InfoTextStartGame;
        //public Text InfoTextNextWave;
        //public Text InfoTextEndGame;
        public Text InfoText;

        public Text SuccessText;
        public Text FailureText;

        public Text ResumeGameText;
        public Text EndGameText;
        public Text PlayAgainText;
        public Text ToMainMenuText;

        public Text ArenaRewardText;
        public Text GoldRewardText;
        public Text XPRewardText;


        
        public InGameLanguageSetup[] inGameLanguageSetup;

        int lang = 0;

        // Use this for initialization
        void Start()
        {
            SetupInGameLanguage();
        }

        public void SetupStart()
        {
            InfoText.text = inGameLanguageSetup[lang].InfoTextStartGameName;
        }

        public void SetupEmpty()
        {
            InfoText.text = "";
        }

        public void SetupNextWave()
        {
            if (InfoText != null)//inGameLanguageSetup[lang] != null) 
            InfoText.text = inGameLanguageSetup[lang].InfoTextNextWaveName;
        }

        public void SetupEndGame()
        {
            InfoText.text = inGameLanguageSetup[lang].EndGameTextName;
        }

        private void SetupInGameLanguage()
        {
            //int lang = 0;
            if (StoreSTDSGameData.instance != null)
                lang = StoreSTDSGameData.instance.stdsData.language;

            SuccessText.text = inGameLanguageSetup[lang].SuccessTextName;
            FailureText.text = inGameLanguageSetup[lang].FailureTextName;

            ResumeGameText.text = inGameLanguageSetup[lang].ResumeGameTextName;
            EndGameText.text = inGameLanguageSetup[lang].EndGameTextName;
            PlayAgainText.text = inGameLanguageSetup[lang].PlayAgainTextName;
            ToMainMenuText.text = inGameLanguageSetup[lang].ToMainMenuTextName;

            ArenaRewardText.text = inGameLanguageSetup[lang].ArenaRewardTextName;
            GoldRewardText.text = inGameLanguageSetup[lang].GoldRewardTextName;
            XPRewardText.text = inGameLanguageSetup[lang].XPRewardTextName;
    }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
