﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

namespace STDS
{
    public class SimpleMainMenuBehaviour : MonoBehaviour
    {
        [SerializeField]
        private AudioMixer masterMixer = null;

        [SerializeField]
        AudioClip btnAudioClip = null;

        [SerializeField]
        Button playBtn = null, levelSkillBtn = null, storeBtn = null, addMultiplyBtn = null, progressMenuBtn = null, addonPanelBtn = null
            , backFromLevelSkillBtn = null, backFromStoreBtn = null, backFromBonusBtn = null, backFromAddonBtn = null,
            quitBtn = null, backFromCampignBtn = null, backFromProgressMenuBtn = null,
            buyForRealCashBtn = null, backFromOptionsBtn = null, backFromRealCashStoreBtn = null,
            optionsBtn = null, abilitesMenuBtn = null, backFromAbilitesBtn = null;

        [SerializeField]
        GameObject skilPanelGobToOpen = null, mainMenuPanel = null, campignPanelGobToOpne = null,
            progressMenuPanelGob = null, storeMenuPanelGob = null, multiplyMenuPanelGob = null,
            realCashMenuPanelGob = null, addonMenuPanelGob = null, optionsMenuPanelGob = null,
            abilitesMenuPanelGob = null;

        //[SerializeField]
       // int levelToLoad = 0;

        SkillBtnsManager skillManager;
       // [SerializeField]
        AudioSource audioSource;

        void Start()
        {
            //UnityEditor.PlayerSettings.dis
            audioSource = GetComponent<AudioSource>(); 
            playBtn.onClick.AddListener(PlayAction);
            levelSkillBtn.onClick.AddListener(GoToSkillLevelAction);
            backFromLevelSkillBtn.onClick.AddListener(delegate { BackToProgressMenuAction(skilPanelGobToOpen); });
            backFromBonusBtn.onClick.AddListener(delegate { BackToProgressMenuAction(multiplyMenuPanelGob); });
            backFromStoreBtn.onClick.AddListener(delegate { BackToProgressMenuAction(storeMenuPanelGob); });
            quitBtn.onClick.AddListener(QuitAction);
            backFromCampignBtn.onClick.AddListener(delegate { BackToMainMenuAction(campignPanelGobToOpne); });
            progressMenuBtn.onClick.AddListener(GoToProgressMenu);
            backFromProgressMenuBtn.onClick.AddListener(delegate { BackToMainMenuAction(progressMenuPanelGob); });
            backFromRealCashStoreBtn.onClick.AddListener(delegate { BackToProgressMenuAction(realCashMenuPanelGob); });
            backFromOptionsBtn.onClick.AddListener(delegate { BackToMainMenuAction(optionsMenuPanelGob); });
            backFromAbilitesBtn.onClick.AddListener(delegate { BackToMainMenuAction(abilitesMenuPanelGob); });
            abilitesMenuBtn.onClick.AddListener(AbilitesAction);

            storeBtn.onClick.AddListener(GoToStoreMenu);
            buyForRealCashBtn.onClick.AddListener(GoToRealCashStore);
            addMultiplyBtn.onClick.AddListener(GoToMultiplyMenu);
            optionsBtn.onClick.AddListener(GoToOptionsMenu);
            skillManager = GetComponent<SkillBtnsManager>();

            addonPanelBtn.onClick.AddListener(GoToAddonPanel);
            backFromAddonBtn.onClick.AddListener(BackToMainMenuFromAddonPanel);
        }


        public void PlayBtnSound()
        {
            audioSource.PlayOneShot(btnAudioClip);
        }

        void GoToStoreMenu()
        {
            PlayBtnSound();
            skillManager.Refresh();
            progressMenuPanelGob.SetActive(false);
            storeMenuPanelGob.SetActive(true);
        }


        void GoToRealCashStore()
        {
            PlayBtnSound();
            skillManager.Refresh();
            progressMenuPanelGob.SetActive(false);
            realCashMenuPanelGob.SetActive(true);
        }


        void GoToMultiplyMenu()
        {
            PlayBtnSound();
            progressMenuPanelGob.SetActive(false);
            multiplyMenuPanelGob.SetActive(true);
        }


        void GoToOptionsMenu()
        {
            PlayBtnSound();
            optionsMenuPanelGob.SetActive(true);
            mainMenuPanel.SetActive(false);
        }


        void GoToProgressMenu()
        {
            PlayBtnSound();
            skillManager.Refresh();
            progressMenuPanelGob.SetActive(true);
            mainMenuPanel.SetActive(false);
        }


        void PlayAction()
        {
            PlayBtnSound();
            mainMenuPanel.SetActive(false);
            campignPanelGobToOpne.SetActive(true);
            // SceneManager.LoadScene(levelToLoad);
        }


        void AbilitesAction()
        {
            PlayBtnSound();
            mainMenuPanel.SetActive(false);
            abilitesMenuPanelGob.SetActive(true);
        }


        void GoToSkillLevelAction()
        {
            PlayBtnSound();
            skillManager.Refresh();
            progressMenuPanelGob.SetActive(false);
            skilPanelGobToOpen.SetActive(true);
            
        }


        void GoToAddonPanel()
        {
            PlayBtnSound();
            addonMenuPanelGob.SetActive(true);
            mainMenuPanel.SetActive(false);
        }


        void BackToMainMenuAction()
        {
            PlayBtnSound();
            mainMenuPanel.SetActive(true);
            skilPanelGobToOpen.SetActive(false);
        }


        void BackToProgressMenuAction(GameObject gob)
        {
            PlayBtnSound();
            skillManager.Refresh();
            progressMenuPanelGob.SetActive(true);
            gob.SetActive(false);
        }


        void BackToMainMenuAction(GameObject gob)
        {
            PlayBtnSound();
            mainMenuPanel.SetActive(true);
            gob.SetActive(false);
        }


        void BackToMainMenuFromAddonPanel()
        {
            PlayBtnSound();
            addonMenuPanelGob.SetActive(false);
            mainMenuPanel.SetActive(true);
        }


        void QuitAction()
        {
            Application.Quit();
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus == false)
                masterMixer.SetFloat("masterVolume", -80);
            else
                masterMixer.SetFloat("masterVolume", 0);
        }
    }
}
