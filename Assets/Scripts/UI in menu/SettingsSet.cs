﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public enum LANGUAGE
    {
        PL = 0,
        EN = 1,
        RU = 2
    }

    public class SettingsSet {

        public static SettingsSet instance;
        Settings settings;


        public SettingsSet(Settings set)
        {
            if (instance != null)
                return;
            settings = set;
            instance = this;
        }

        public LANGUAGE Language
        {
            get { return settings.lang; }
            set { settings.lang = value; }
        }

        public int GetLanguageNumber()
        {
            return (int)settings.lang;
        }

        public float SoundVolume
        {
            get { return settings.soundVolume; }
            set { settings.soundVolume = value; }
            
        }

        public float MusicVolume
        {
            get { return settings.musicVolume; }
            set { settings.musicVolume = value; }
        }


        public string PlayerName
        {
            get { return settings.playerName; }
            set { settings.playerName = value; }
        }
    }
}
