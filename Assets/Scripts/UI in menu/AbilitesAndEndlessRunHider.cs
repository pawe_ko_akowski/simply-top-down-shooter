﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

namespace STDS
{
    public class AbilitesAndEndlessRunHider : MonoBehaviour
    {
        [Serializable]
        private class EndlessBtnToWatch
        {
            public GameObject btnGob;
            public Button btn;
            public Text text;
            public Text waveText;
            public Image image;

            //public virtual void Show()
            //{
            //    btnGob.SetActive(true);
            //    btn.interactable = true;
            //    text.text = "0";
            //    image.color = Color.white;
            //}

            public EndlessBtnToWatch(GameObject btG, Button bt, Text tx, Text wa, Image im)
            {
                btnGob = btG;
                btn = bt;
                text = tx;
                waveText = wa;
                image = im;
            }

            public virtual void Show(int score, int ready)
            {
                btnGob.SetActive(true);
                if (ready > 0)
                {
                    text.text = score.ToString();
                    btn.interactable = true;
                   // image.color = Color.white;
                    image.enabled = true;
                }

            }

            public virtual void Show(int score, int waveScore, int ready = 1)
            {
                btnGob.SetActive(true);
                if (ready > 0)
                {
                    text.text = score.ToString();
                    waveText.gameObject.SetActive(true);
                    waveText.text = waveScore.ToString();
                    btn.interactable = true;
                    //image.color = Color.white;
                    image.enabled = true;
                }

            }

            //public virtual void Show(float time)
            //{
            //    btnGob.SetActive(true);
            //    text.text = time.ToString("F3");
            //    btn.interactable = true;
            //    image.color = Color.white;
            //}

            public void Hide()
            {
                //btnGob.SetActive(false);

                waveText.gameObject.SetActive(false);
                image.enabled = false;
               // Debug.Log(waveText.gameObject.activeSelf);
            }

            public virtual void Show(int score)
            {
                text.text = score.ToString();
            }

            public virtual void ShowWaveScore(int score)
            {
                waveText.text = score.ToString() ;
            }
        }

        [Serializable]
        private class EndlessMysteryBtnToWatch : EndlessBtnToWatch
        {
            public Text[] mystery;
            public Color forHide = Color.black, forUnhide = Color.red;

            public EndlessMysteryBtnToWatch(GameObject btG, Button bt, Text tx, Text wa, Image im, Text[] ms, Text[] mystery, Color forHide, Color forUnhide) : base(btG, bt, tx, wa, im)
            {
                this.mystery = mystery;
                this.forHide = forHide;
                this.forUnhide = forUnhide;
            }

            public virtual void Show(int[] myst, int score)
            {
                bool openBtn = true;
                for (int i = 0; i < myst.Length; i++)
                {
                    if (myst[i] == 0)
                    {
                        openBtn = false;
                        mystery[i].color = forHide;
                    }
                    else
                        mystery[i].color = forUnhide;

                }
                if (openBtn)
                    base.Show(score);

            }



            public void ShowFull(int score, int waveScore)
            {
                for (int i = 0; i < mystery.Length; i++)
                {
                    mystery[i].gameObject.SetActive(false);
                }
                btn.interactable = true;
                Show(score);
                ShowWaveScore(waveScore);
            }

            public void ShowSome(int[] myst)
            {
                for (int i = 0; i < myst.Length; i++)
                {
                    if (myst[i] == 0)
                    {
                        mystery[i].color = forHide;
                    }
                    else
                        mystery[i].color = forUnhide;

                }
            }
        }

        public int levelEndRequireForFirstEndless = 5;

        public int firsEndlesSceneIndex = 11;
        public int secondEndlesSceneIndex = 12;

        //[SerializeField]
       // private int campignRequirementsForFirstBtn = 1, campignRequirementsForMysteryBtn = 5;

        //[SerializeField]
       // private int campignRequirementForInteractableFirstBtn = 5;

        [SerializeField]
        private EndlessBtnToWatch endlessBtn = null;

        [SerializeField]
        private EndlessMysteryBtnToWatch endlessMysteryBtn = null;

       // [SerializeField]
       // Button abilitesBtn = null;
        //[SerializeField]
       // private Text[] abilityText = null;

        [SerializeField]
        bool endlessActiveInGame = false;
        [SerializeField]
        bool mysteryActiveInGame = true;

       



        private bool endlessActive = false;
        private bool mysteryActive = false;


        STDSdata gameData;

        private void LoadFirstArena()
        {
            SceneManager.LoadScene(firsEndlesSceneIndex);
        }


        private void LoadSecondArena()
        {
            SceneManager.LoadScene(secondEndlesSceneIndex);
        }

        // Use this for initialization
        void Start()
        {
            endlessBtn.btn.onClick.AddListener(LoadFirstArena);
            endlessMysteryBtn.btn.onClick.AddListener(LoadSecondArena);

            gameData = StoreSTDSGameData.instance.stdsData;
            //Debug.Log(gameData.endlessOpen + "   " + campignRequirementForInteractableFirstBtn);
            if (endlessActiveInGame)
                if (gameData.endlessOpen >= levelEndRequireForFirstEndless || gameData.campignEndOnce > 0 )
                {
                    //endlessBtn.btnGob.SetActive(true);
                    //int showEndless = 0;
                    //if (gameData.endlessOpen >= campignRequirementForInteractableFirstBtn)
                    //{
                    //    showEndless = 1;
                    //    endlessActive = true;
                    //}
                    //endlessBtn.Show(gameData.smallArenaHighscore, showEndless);
                    endlessBtn.Show(gameData.smallArenaHighscore, gameData.smallArenaWaveHigscore, 1);

                    //  abilitesBtn.gameObject.SetActive(true);
                    //AbilitesHide();

                }
                else
                {
                    endlessBtn.Hide();
                      

                }
            //else
            //{
            //    endlessBtn.Hide();
            //    endlessMysteryBtn.Hide();
            //    abilitesBtn.gameObject.SetActive(false);
            //}

            if (mysteryActiveInGame)
                ShowMysteryLevel();
           // else
              //  endlessMysteryBtn.Hide();
        }

        public bool EndlesActive()
        {
            return endlessActive;
        }

        public bool MysteryActive()
        {
            return mysteryActive;
        }

        //private void AbilitesHide()
        //{
        //    for (int i = 0; i < abilityText.Length; i++)
        //    {
        //        if (StoreSTDSGameData.instance.stdsData.AbilityLevel <= i)
        //        {
        //            abilityText[i].color = new Color(1, 1, 1, 0);
        //        }
        //    }
        //}

        void ShowMysteryLevel()
        {
            if (gameData.mysteryFull > 0)
            {
                //Debug.Log("full");
                endlessMysteryBtn.ShowFull(gameData.bigArenaHighscore, gameData.bigArenaWaveHighscore);
                mysteryActive = true;
            }
            else
            {
                // if(gameData.mysteryFind >= campignRequirementsForMysteryBtn)
                endlessMysteryBtn.ShowSome(gameData.mysteryFind);
                endlessMysteryBtn.Hide();
            }
        }
    }
}
