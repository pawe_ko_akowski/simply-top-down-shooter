﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace STDS
{
    public class SimpleQuitFromLevel : MonoBehaviour
    {
        [SerializeField]
        int mainMenuLevel = 0;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(mainMenuLevel);
            }
        }
    }
}
