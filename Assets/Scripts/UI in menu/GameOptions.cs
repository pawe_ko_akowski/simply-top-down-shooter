﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections.Generic;

namespace STDS
{
    
    public class GameOptions : MonoBehaviour
    {
        [SerializeField]
        Dropdown languageDropdown = null;
        [SerializeField]
        Slider soundSlider = null;
        [SerializeField]
        Slider musicSlider = null;
        [SerializeField]
        Text playerNameText = null;
        [SerializeField]
        InputField playerTextInput = null;
        [SerializeField]
        Button exitFromMenuSaveBtn = null;

        [SerializeField]
        Button[] controlBtn = null;
        Text[] controlBtnText = null;
        int activeControl = -1;
        int activeDic = -1;

        [SerializeField]
        AudioMixer audioMixer = null;
        [SerializeField]
        Button autoAimBtn = null;
        [SerializeField]
        Button smoothBtn = null;
        Text smoothText;
        //[SerializeField]
        //string activeString = "ON", disactiveString = "OFF";
        [SerializeField]
        Slider grenadeTimerSlider = null, mineTimerSlider = null;

        public Slider timeSlider;

        MainMenuLanguageSetup mainMenuLangSetup;
        bool init = false;
     

        private enum CONTROL
        {
            MOVE,
            AIM,
            GRENADE_DROP,
            MINE_DROP
        }

        private Dictionary<int, string> dic = new Dictionary<int, string>();

        Text autoAimBtnText = null;
        STDSdata stdsData = null;

        void Start()
        {
            Time.timeScale = 1f;
            mainMenuLangSetup = GetComponent<MainMenuLanguageSetup>();
            stdsData = StoreSTDSGameData.instance.stdsData;
            languageDropdown.onValueChanged.AddListener(OnChangeDropdown);
            languageDropdown.value = stdsData.language;// SettingsSet.instance.GetLanguageNumber();
            languageDropdown.RefreshShownValue();
            mainMenuLangSetup.LanguageSetup(stdsData.language);

            soundSlider.onValueChanged.AddListener(OnChangeSoundSlider);
            soundSlider.value = stdsData.soundVolume;
            audioMixer.SetFloat("soundVolume", soundSlider.value);

            musicSlider.onValueChanged.AddListener(OnChangeMusicSlider);
            musicSlider.value = stdsData.musicVolume;
            audioMixer.SetFloat("musicVolume", musicSlider.value);

            timeSlider.onValueChanged.AddListener(OnChangeTimeSlider);
            timeSlider.value = stdsData.timeScale;

            grenadeTimerSlider.value = stdsData.grenadeBoomTime;
            mineTimerSlider.value = stdsData.mineBoomTime;
            grenadeTimerSlider.onValueChanged.AddListener(OnChangeGranadeTimerSlider);
            mineTimerSlider.onValueChanged.AddListener(OnChangeMineTimerSlider);
            

            playerNameText.text = stdsData.playerName;
            playerTextInput.text = stdsData.playerName;

            playerTextInput.onValueChanged.AddListener(OnChangeName);
            exitFromMenuSaveBtn.onClick.AddListener(OnBtnSave);

            autoAimBtnText = autoAimBtn.GetComponentInChildren<Text>();
            autoAimBtn.onClick.AddListener(AutoAimChange);
            InitAutoAimBtn();

            smoothText = smoothBtn.GetComponentInChildren<Text>();
            smoothBtn.onClick.AddListener(SmoothChange);
            InitSmoothBtn();

            controlBtnText = new Text[controlBtn.Length];

            FillAllControl();
            init = true;
        }

        void FillAllControl()
        {
            InitDic();
            for (int i = 0; i < controlBtn.Length; i++)
            {
                int ind = i;
                controlBtn[i].onClick.AddListener(() => { OnClickControlBtn(ind); });
                //btn.onClick.AddListener(() => { Function(param); OtherFunction(param); });
                controlBtnText[i] = controlBtn[i].GetComponentInChildren<Text>();
                controlBtnText[i].text = dic[stdsData.mobileControlSet[i]];
            }

            FillAllNoneControl();
        }

        void FillNames()
        {
            if (init)
            {
                InitDic();
                for (int i = 0; i < controlBtn.Length; i++)
                {
                    controlBtnText[i].text = dic[stdsData.mobileControlSet[i]];
                }

                FillAllNoneControl();
            }
        }
        void OnClickControlBtn(int index)
        {
            stdsData.mobileControlSet[index] = WhatCanIDo(index);
            InitAllAndRemoveBusy();
            //Debug.Log(index + "    ma wartosc   " + stdsData.mobileControlSet[index]);
        }

        void InitDic()
        {
            string[] control = mainMenuLangSetup.ControlSlotLabelNames();
            dic.Clear();
            dic.Add(0, control[0]);
            dic.Add(1, control[1]);
            dic.Add(2, control[2]);
            dic.Add(3, control[3]);
            dic.Add(4, control[4]);
            //dic.Add(0, "None");
            //dic.Add(1, "Mine drop");
            //dic.Add(2, "Move");
            //dic.Add(3, "Grenade drop");
            //dic.Add(4, "Aim");
         

        }

        void InitAllAndRemoveBusy()
        {
            InitDic();

            for (int i = 0; i < stdsData.mobileControlSet.Length; i++)
            {
                if (stdsData.mobileControlSet[i] > 0)
                {
                    dic.Remove(stdsData.mobileControlSet[i]);
                }
            }
        }


        int WhatCanIDo(int cont)
        {
            if (cont != activeControl)
            {
                activeControl = cont;
                activeDic = -1;
                InitAllAndRemoveBusy();
            }
            activeDic++;
            //int to;

            while (!dic.ContainsKey(activeDic) || stdsData.mobileControlSet[cont] == (activeDic))
            {
               // Debug.Log(activeDic);
                activeDic++;
                
                if (activeDic > 4)
                {
                    activeDic = 0;
                    activeControl = -1;
                }


            }

            controlBtnText[cont].text = dic[activeDic];

            //string what = dic[0];
            //if(dic.ContainsKey(activeDic))
            //  ac
            // if (activeDic < dic.Count)

            //    return dic[activeDic].

            // for (int j = 0; j < dic.Count; j++)
            //{
            //   dic[
            // }

            //  Debug.Log(activeDic);
            return activeDic;
        }

        public void LanguageChangeForControlSlot()
        {

        }

        void FillAllNoneControl()
        {
            InitAutoAimBtn();
            for (int i = 0; i < stdsData.mobileControlSet.Length; i++)
            {
                if (stdsData.mobileControlSet[i] == 0)
                {
                    stdsData.mobileControlSet[i] = WhatCanIDo(i);
                    InitAllAndRemoveBusy();
                }
            }
        }

        void InitAutoAimBtn()
        {
            if (stdsData.AutoAim)
                autoAimBtnText.text = mainMenuLangSetup.GetActive;
            else
                autoAimBtnText.text = mainMenuLangSetup.GetDisactive;
        }

        void AutoAimChange()
        {
            if (stdsData.AutoAim)
            {
                stdsData.AutoAim = false;
               // autoAimBtnText.text = disactiveString;
            }
            else
            {
                stdsData.AutoAim = true;
               // autoAimBtnText.text = activeString;
            }
            InitAutoAimBtn();
        }


        void InitSmoothBtn()
        {
            if (stdsData.smooth > 0)
                smoothText.text = mainMenuLangSetup.GetActive;
            else
                smoothText.text = mainMenuLangSetup.GetDisactive;
        }

        void SmoothChange()
        {
            if (stdsData.smooth > 0)
            {
                stdsData.smooth = 0;
                // autoAimBtnText.text = disactiveString;
            }
            else
            {
                stdsData.smooth = 1;
                // autoAimBtnText.text = activeString;
            }
            InitSmoothBtn();
        }


        void OnChangeDropdown(int whatLang)
        {
            //if (whatLang == 0)
               // SettingsSet.instance.Language = LANGUAGE.PL;
           // if (whatLang == 1)
              //  SettingsSet.instance.Language = LANGUAGE.EN;

            stdsData.language = whatLang;
            mainMenuLangSetup.LanguageSetup(stdsData.language);

            FillNames();
            activeControl = -1;
            activeDic = -1;
            //FillAllNoneControl();
        }


        void OnChangeSoundSlider(float what)
        {
            //SettingsSet.instance.SoundVolume = what;
            what = Mathf.Round(what);
            stdsData.soundVolume = what;
            audioMixer.SetFloat("soundVolume", what);
        }


        void OnChangeMusicSlider(float what)
        {
            // SettingsSet.instance.MusicVolume = what;
            what = Mathf.Round(what);
            stdsData.musicVolume = what;
            audioMixer.SetFloat("musicVolume", what);
        }



        void OnChangeGranadeTimerSlider(float what)
        {
            what = Mathf.Round(what);
            stdsData.grenadeBoomTime = what;
        }


        void OnChangeTimeSlider(float what)
        {
            what = Mathf.Round(what);
           // Debug.Log((int)what);
            stdsData.timeScale = (int)what;
        }


        void OnChangeMineTimerSlider(float what)
        {
            what = Mathf.Round(what);
            stdsData.mineBoomTime = what;
        }

        void OnChangeName(string name)
        {
           // SettingsSet.instance.PlayerName = name;
            stdsData.playerName = name;
            
        }

        void OnBtnSave()
        {
            //FillAllNoneControl();

            StoreSTDSGameData.instance.Save();
            playerNameText.text = stdsData.playerName;
        }
    }
}
