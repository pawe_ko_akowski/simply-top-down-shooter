﻿using UnityEngine;
using UnityEngine.UI;

namespace STDS
{
    public class LayoutSizeController : MonoBehaviour
    {
        [SerializeField]
        RectTransform scrollBar = null, panelRect = null;
        [SerializeField]
        GameObject Content = null;


        void Start()
        {
            LayoutElement[] layouts = Content.GetComponentsInChildren<LayoutElement>();

            for (int i = 0; i < layouts.Length; i++)
                layouts[i].minWidth = Screen.width * 0.85f;

            scrollBar.anchoredPosition = new Vector2(Screen.width * 0.85f, 0);
            scrollBar.sizeDelta = new Vector2(Screen.width * 0.15f, panelRect.rect.height);
        }
    }
}
