﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Runtime.Serialization;

namespace STDS
{
    public class FillAllScriptableObiect : MonoBehaviour
    {

        //StoreSTDSGameData storData = new StoreSTDSGameData();

        //public StoreSTDSGameData data
        //{
        //    get
        //    {
        //        return storData;
        //    }
        //}


        [SerializeField]
        LevelProgress levelProgres = null;

        //[SerializeField]
        //Campign campign = null;

        [SerializeField]
        EquipmentStats equipmentStats = null;

        // [SerializeField]
        // LevelXPProgress levelXPProgress;

        [SerializeField]
        PlayerStats playerStats = null;

        //[SerializeField]
       // Settings settings = null;

        [SerializeField]
        XPAndCoinMultiplierStats xpAndCoinMultiplierStats = null;

        [SerializeField]
        string dataFile = "gameData.gd";
        [SerializeField]
        string folderForData = "zap";


        //string GetDataFilePath()
        //{
        //    string add = (folderForData != "") ? folderForData + "/" : "";

        //    return Application.dataPath + "/" + add + dataFile;
        //}

        //string GetDataFilePath(string file)
        //{
        //    string add = (folderForData != "") ? folderForData + "/" : "";

        //    return Application.dataPath + "/" + add + file;
        //}

        // Use this for initialization
        void Awake()
        {
            if(StoreSTDSGameData.instance == null)
            {
                //Debug.Log("to");
                new StoreSTDSGameData(levelProgres, equipmentStats, playerStats, xpAndCoinMultiplierStats, dataFile, folderForData, true);//, false, true);
            }
            else
            {
                StoreSTDSGameData.instance.Save();
            }

            Destroy(this);


            // string json = JsonUtility.ToJson(storeGameData);
            // storeGameData = JsonUtility.FromJson<StoredGameData>(Application.persistentDataPath + "storeGameData");

            //if (File.Exists(GetDataFilePath()))
            //{
            //    Load();
            //}
            //else
            //{
            //    Save();
            //    Load();
            //}


            //FillLevelProgress();
            //Debug.Log(storData.armorLevel.ToString());

        }


        //void FillLevelProgress()
        //{
        //    levelProgres.liveLevel = storData.liveLevel;
        //    levelProgres.liveRegenLevel = storData.liveRegenerationLevel;
        //    levelProgres.armorLevel = storData.armorLevel;
        //    levelProgres.bulletDamageLevel = storData.bulletDamageLevel;
        //    levelProgres.bulletRateLevel = storData.bulletRateLevel;
        //    levelProgres.movementLevel = storData.movementSpeedLevel;

        //    levelProgres.grenadeDamageLevel = storData.grenadeDamageLevel;
        //    levelProgres.grenadeRangeLevel = storData.grenadeRangeLevel;
        //    levelProgres.mineDamageLevel = storData.mineDamageLevel;
        //    levelProgres.mineRangeLevel = storData.mineRangeLevel;

        //    levelProgres.goldAddLevel = storData.goldCoinAddLevel;
        //    levelProgres.goldFrequencyLevel = storData.goldFrequencyLevel;
        //    levelProgres.goldRangeLevel = storData.goldTakeRangeLevel;
        //}


        //public void SaveLevelProgress()
        //{


        //}


        //void Save(string file)
        //{
        //    //// SaveLoad.savedGames.Add(Game.current);
        //    // BinaryFormatter bf = new BinaryFormatter();
        //    // //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //    // FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
        //    // bf.Serialize(file, JsonUtility.ToJson(storData));
        //    // file.Close();

        //    FileStream fs = new FileStream(file, FileMode.Create);

        //    // Construct a BinaryFormatter and use it to serialize the data to the stream.
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    try
        //    {
        //        formatter.Serialize(fs, JsonUtility.ToJson(storData));
        //    }
        //    catch (SerializationException e)
        //    {
        //        Console.WriteLine("Failed to serialize. Reason: " + e.Message);
        //        throw;
        //    }
        //    finally
        //    {
        //        fs.Close();
        //    }
        //}

        ////it's static so we can call it from anywhere
        //public void Save()
        //{
        //    //// SaveLoad.savedGames.Add(Game.current);
        //    // BinaryFormatter bf = new BinaryFormatter();
        //    // //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //    // FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
        //    // bf.Serialize(file, JsonUtility.ToJson(storData));
        //    // file.Close();

        //    FileStream fs = new FileStream(GetDataFilePath(), FileMode.Create);

        //    // Construct a BinaryFormatter and use it to serialize the data to the stream.
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    try
        //    {
        //        formatter.Serialize(fs, JsonUtility.ToJson(storData));
        //    }
        //    catch (SerializationException e)
        //    {
        //        Console.WriteLine("Failed to serialize. Reason: " + e.Message);
        //        throw;
        //    }
        //    finally
        //    {
        //        fs.Close();
        //    }
        //}

        //public void Load()
        //{

        //    FileStream fs = new FileStream(GetDataFilePath(), FileMode.Open);
        //    try
        //    {
        //        BinaryFormatter formatter = new BinaryFormatter();
        //        string str = (string)formatter.Deserialize(fs);
        //        //storData = new STDS.StoredGameData2();
        //        storData = JsonUtility.FromJson<StoreSTDSGameData>(str);
        //        FillLevelProgress();
        //        // Deserialize the hashtable from the file and 
        //        // assign the reference to the local variable.
        //        //addresses = (Hashtable)formatter.Deserialize(fs);
        //    }
        //    catch (SerializationException e)
        //    {
        //        Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
        //        throw;
        //    }
        //    finally
        //    {
        //        fs.Close();
        //    }

        //    // Debug.Log(Application.dataPath + "/savedGames.gd");

        //    //if (File.Exists(Application.dataPath + "/zap/to.gd"))
        //    //{
        //    //    Debug.Log("czytam");
        //    //    BinaryFormatter bf = new BinaryFormatter();
        //    //    FileStream file = File.Open(Application.dataPath + "/zap/to.gd", FileMode.Open);
        //    //    string str = (string)bf.Deserialize(file);

        //    //    Debug.Log(str);
        //    //   // string data = JsonUtility.FromJson<StoredGameData>(bf.Deserialize(file));
        //    //  //  SaveLoad.savedGames = (List<Game>)bf.Deserialize(file);
        //    //    file.Close();
        //    //}
        //    //else
        //    //{
        //    //    Debug.Log("Tworze");

        //    //    storData = new STDS.StoredGameData2();
        //    //    BinaryFormatter bf = new BinaryFormatter();
        //    //    FileStream file = File.Create(Application.dataPath + "/zap/to.gd"); //you can call it anything you want
        //    //    string json = JsonUtility.ToJson(storData);
        //    //    bf.Serialize(file, json);
        //    //    file.Close();
        //    //}
        //}

    }
}
