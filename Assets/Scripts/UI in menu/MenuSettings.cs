﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STDS
{
    public class MenuSettings : MonoBehaviour
    {
        [SerializeField]
        Settings settings = null;

        void Awake()
        {
            if(settings == null)
            {
                Debug.LogError("No settings set");
                return;
            }

            new SettingsSet(settings);
        }


    }
}
