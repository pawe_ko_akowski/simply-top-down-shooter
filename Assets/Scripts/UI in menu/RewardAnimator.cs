﻿using UnityEngine;

namespace STDS
{
    public class RewardAnimator : MonoBehaviour
    {
        [SerializeField]
        Animator anim = null;


        public void StartFirst()
        {

        }

        public void GoNextAnimator()
        {
            if (anim != null)
                anim.enabled = true;
        }
    }
}
