﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

namespace STDS
{
    public class AdBonusStore : MonoBehaviour
    {
//#if UNITY_ANDROID
       // [SerializeField]
        //PlayerStats playerStats = null;
       // [SerializeField]
       // XPAndCoinMultiplierStats xpAndCoinsMutipStat = null;
        [SerializeField]
        Text bonusActivityText = null;//, coinMultiplierText = null, xpMultiplierText = null;
        [SerializeField]
        Button coinMultiplierAdBtn = null, xpMultiplierAdBtn = null;
        //[SerializeField]
       // string forCoinAdStr = "Increase COIN multiplier", forXPAdStr = "Increase XP multipier";
        [SerializeField]
        private Image xpImage = null, cashImage = null;
        [SerializeField]
        private Image xpInMainMenuImage = null, cashiInMainMenuImage = null;
        [SerializeField]
        private Color forActiveXpColor = Color.white,forActiveCashColor = Color.white;
        [SerializeField]
        private Sprite readySprite = null, unreadySprite = null;

       // Text coinAdBtnText , xpAdBtnText;
        int chosen = 0;
        STDSdata stdsData;
        FounderAndSpectatorFill founderAndSpectatorFill = null;
        //bool ready = false;



        void Awake()
        {


  
        }

        private void Start()
        {

            coinMultiplierAdBtn.onClick.AddListener(CoinMultiplierAd);
            xpMultiplierAdBtn.onClick.AddListener(XPMultiplierAd);

           // coinAdBtnText = coinMultiplierAdBtn.GetComponentInChildren<Text>();
           // xpAdBtnText = xpMultiplierAdBtn.GetComponentInChildren<Text>();

           // coinAdBtnText.text = forCoinAdStr;
           // xpAdBtnText.text = forXPAdStr;
            stdsData = StoreSTDSGameData.instance.stdsData;
            founderAndSpectatorFill = FindObjectOfType<FounderAndSpectatorFill>();
           // ready = true;
            AllCheck();



           // AllCheck();
        }


        //void OnEnable()
        //{
        //    if(ready)
        //    AllCheck();
        //}


        void AllCheck()
        {
            //Advertisement.Initialize(
            bonusActivityText.text = stdsData.multiplierActivity.ToString();// xpAndCoinsMutipStat.XPAndCointMultiplierActiveFights.ToString();

            int coinMultiplier = stdsData.coinMultiplier;
            //coinMultiplierText.text = coinMultiplier.ToString();// xpAndCoinsMutipStat.GetCoinMultiplier.ToString();
            SetReadyOrDontReady(ref cashImage, coinMultiplier);

            int xpMultiplier = stdsData.xpMultiplier;
            //xpMultiplierText.text = xpMultiplier.ToString();// xpAndCoinsMutipStat.GetXPMultiplier.ToString();
            SetReadyOrDontReady(ref xpImage, xpMultiplier);
            //dupa
            //RewardeAdCheck();

            if (stdsData.coinMultiplier > 0)
            {
                coinMultiplierAdBtn.interactable = false;
                cashiInMainMenuImage.color = forActiveCashColor;
            }
            if (stdsData.xpMultiplier > 0)
            {
                xpMultiplierAdBtn.interactable = false;
                xpInMainMenuImage.color = forActiveXpColor;
            }

            if (stdsData.multiplierActivity == 2)
            {
                coinMultiplierAdBtn.interactable = false;
                xpMultiplierAdBtn.interactable = false;
            }
            //MaxAdCheckForCoin();
            //MaxAdCheckForXP();
        }

        void SetReadyOrDontReady(ref Image image, int value)
        {
            if (value > 0)
                image.sprite = readySprite;
            else
                image.sprite = unreadySprite;

        }


        //void RewardeAdCheck()
        //{
        //    if (Advertisement.IsReady("rewardedVideo"))
        //    {
        //        coinMultiplierAdBtn.interactable = true;
        //        xpMultiplierAdBtn.interactable = true;
        //       // Debug.Log("ad ready");
        //    }
        //    else
        //    {
        //        coinMultiplierAdBtn.interactable = false;
        //        xpMultiplierAdBtn.interactable = false;
        //       // Debug.Log("ad not ready");
        //    }
        //}


        //void MaxAdCheckForCoin()
        //{
        //    if (xpAndCoinsMutipStat.CanProgresCoinMultiplier())
        //    {
        //        coinMultiplierAdBtn.interactable = true;
        //    }
        //    else
        //    {
        //        coinMultiplierAdBtn.interactable = false;
        //        coinAdBtnText.text = "MAX";
        //    }
        //}


        //void MaxAdCheckForXP()
        //{
        //    if (xpAndCoinsMutipStat.CanProgresXPMultiplier())
        //    {
        //        xpMultiplierAdBtn.interactable = true;
        //    }
        //    else
        //    {
        //        xpMultiplierAdBtn.interactable = false;
        //        xpAdBtnText.text = "MAX";
        //    }
        //}


        void CoinMultiplierAd()
        {
            chosen = 1;
            //dupa
            //ShowRewardedAd();
            ChosenRecognize();
        }


        void XPMultiplierAd()
        {
            chosen = 2;
            //dupa
            //ShowRewardedAd();
            ChosenRecognize();
        }


        //void ShowRewardedAd()
        //{
        //    if (Advertisement.IsReady("rewardedVideo"))
        //    {
        //        var options = new ShowOptions { resultCallback = HandleShowResult };
        //        Advertisement.Show("rewardedVideo", options);
        //    }
        //}


        //private void HandleShowResult(ShowResult result)
        //{
        //    switch (result)
        //    {
        //        case ShowResult.Finished:
        //            ChosenRecognize();
        //            break;
        //        case ShowResult.Skipped:
        //            break;
        //        case ShowResult.Failed:
        //            break;
        //    }

        //}

        void SetValuToOneAndIncrease(ref int what)
        {
            what = 1;
            stdsData.multiplierActivity++;
            DecreaseIfToBig();
        }


        void ChosenRecognize()
        {
            //if 1 add to coin multiplier
            if (chosen == 1)
                SetValuToOneAndIncrease(ref stdsData.coinMultiplier);
            //if 2 add to XP multiplier
            if (chosen == 2)
                SetValuToOneAndIncrease(ref stdsData.xpMultiplier);
            AllCheck();
            stdsData.spectator = 1;
            StoreSTDSGameData.instance.Save();
            if (founderAndSpectatorFill != null)
                founderAndSpectatorFill.Show();
            chosen = 0;
        }

        void DecreaseIfToBig()
        {
            if (stdsData.multiplierActivity > 2)
                stdsData.multiplierActivity = 2;
        }
//#endif
    }
}