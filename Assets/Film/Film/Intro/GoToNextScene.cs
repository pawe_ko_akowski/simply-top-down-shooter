﻿using UnityEngine;
//using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace STDS
{
    public class GoToNextScene : MonoBehaviour
    {
        [SerializeField]
        float timeToReload = 40f;

        public string[] polishFab;
        public string[] englishFab;

        public Text[] text;

        public GameObject obiectToUnhide;
        GameObject thisParentOb;

        int nextScene;
        // Use this for initialization
        void Start()
        {
            Cursor.visible = true;
            thisParentOb = transform.parent.gameObject;
            //nextScene = SceneManager.GetActiveScene().buildIndex + 1;
            if (StoreSTDSGameData.instance.stdsData.language == 0)
            {
                for (int i = 0; i < text.Length; i++)
                    text[i].text = polishFab[i];
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                    text[i].text = englishFab[i];
            }

            if (StoreSTDSGameData.instance.stdsData.introOutroWatch == 0)
            {
                obiectToUnhide.SetActive(false);
                StoreSTDSGameData.instance.stdsData.introOutroWatch = 1;
                StoreSTDSGameData.instance.Save();
                Cursor.visible = false;
            }
            else
                thisParentOb.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            timeToReload -= Time.deltaTime;

            if (Input.anyKeyDown || timeToReload <= 0)
            {
                thisParentOb.SetActive(false);
                obiectToUnhide.SetActive(true);
                Cursor.visible = true;
                // ToNextScene();

            }

        }

        void ToNextScene()
        {
         //   SceneManager.LoadScene(nextScene);
        }

    }
}
